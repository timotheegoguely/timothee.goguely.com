document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/,'js');

// Toggle theme
var themeSwitch = document.querySelector('#theme-switch'),
    preferedColorScheme = getComputedStyle(document.documentElement).getPropertyValue('content').slice(1, -1),
    currentTheme = localStorage.getItem('theme') ? localStorage.getItem('theme') : preferedColorScheme;
if (currentTheme) {
  document.documentElement.setAttribute('data-theme', currentTheme);
}
function switchTheme() {
  var currentTheme = document.documentElement.getAttribute('data-theme');
  if (currentTheme !== 'dark') {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
  }
  else { 
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
  }    
}
themeSwitch.addEventListener('click', switchTheme, false);
themeSwitch.addEventListener('keypress', function(e) {
  if (e.which === 13) { // enter key
    switchTheme;
  }
});
