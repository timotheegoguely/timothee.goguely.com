# Timothée Goguely

Personal website build with [Hugo](https://gohugo.io/)

![Homepage dark mode](static/img/home-1280x720.png)
