---
title: Mentions légales
slug: mentions-legales
date: 2021-08-06
stats:
  - name: Poids de la page
    dots: ...
    value: 42Ko
  - name: Nombre de requêtes
    dots: .
    value: 7
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 114
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

## Édition, design & développement

**Timothée Goguely**
Entrepreneur-salarié associé au sein de la coopérative Oxalis depuis <time datetime="2020-08">août 2020</time>.
Scop SA à capital variable. RCS Chambéry
Siège social&#8239;: 603, bld Président Wilson – 73100 Aix les Bains
Tél.&#8239;: <a href="tel:+33450244455">+33 4 50 24 44 55</a>
Fax&#8239;: <a href="fax:+33479610928">+33 4 79 61 09 28</a>
Email&#8239;: <a href="mailto:info@oxalis-scop.org" aria-label="Courriel d’Oxalis">info@oxalis-scop.org</a>
Site web&#8239;: <a href="https://www.oxalis.coop" aria-label="Site web d’Oxalis">www.oxalis.coop</a>
SIREN&#8239;: 410 829 477
Code APE&#8239;: 8299Z

<div class="clear-both" aria-hidden="true"></div>

---

## Hébergement

**Infomaniak Network SA**
Siège social&#8239;: 26, Avenue de la Praille – 1227 Carouge / Genève – Suisse 
Tél.&#8239;: <a href="tel:+41228203544">+41 22 820 35 44</a>
Fax&#8239;: <a href="fax:+41223016769">+41 22 301 67 69</a>
Email&#8239;: <a href="mailto:contact@infomaniak.ch" aria-label="Courriel d’Infomaniak">contact@infomaniak.ch</a>
Site web&#8239;: <a href="https://www.infomaniak.com" aria-label="Site web d’Infomaniak">www.infomaniak.com</a>

<div class="clear-both" aria-hidden="true"></div>

---

## Contenus

L’ensemble des contenus de ce site (textes et media) sont mis à disposition selon les termes de la license <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><abbr title="Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International">CC BY-NC-SA 4.0</abbr></a>.

<div class="clear-both" aria-hidden="true"></div>

---

## Outils & crédits

- Ce site a été maquetté avec [Figma](https://www.figma.com/) puis codé en HTML, CSS et JavaScript à l’aide de l’éditeur de texte [Sublime Text](https://www.sublimetext.com/) et du générateur de site statique [Hugo](https://gohugo.io). Le code source est disponible sur mon [GitLab](https://gitlab.com/timotheegoguely/timothee.goguely.com).
- Les polices de caractères utilisées sont [NT Bau](https://nodotypefoundry.com/product/nt-bau/) et <span class="mono">[NT Bau Mono](https://nodotypefoundry.com/product/nt-bau-mono/)</span> dans leur version regular, dessinées par Ariel Di Lisio et Aldo Arillo et distribuées par [Nodo Type Foundry](https://nodotypefoundry.com/).
- La photo de la page d’accueil a été prise par [Gauthier Roussilhe](https://gauthierroussilhe.com/) et retouchée par mes soins.

<div class="clear-both" aria-hidden="true"></div>

---

## Confidentialité

Ce site utilise <a href="https://plausible.io" rel="noreferrer noopener" lang="en">Plausible Analytics</a>. Il s’agit d’une alternative européenne à Google Analytics respectueuse de la vie privée des internautes. Leur page <a href="https://plausible.io/data-policy" lang="en">Data Policy</a> liste toutes les données que le service collecte et stocke et celles qui ne le sont pas.

<div class="clear-both" aria-hidden="true"></div>
