---
title: Log
slug: log
alias: "2023"
date: 2023-01-01
description: Journal de notes mensuelles
stats:
  - name: Poids de la page
    dots: ...
    value: 65Ko
  - name: Nombre de requêtes
    dots: .
    value: 8
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 118
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%
---

Journal de notes mensuelles.
Archives [2022](/log/2022).

<!-- - [Janvier 2023](#janvier-2023) -->

---

<h2 id="janvier-2023">Janvier 2023</h2>

- Remplacement de [Simple Analytics](https://simpleanalytics.com/) par [Plausible Analytics](https://plausible.io/)
- Workshop de 4 jours avec Adrien Payet pour les M1 [Design « Environnements numériques » de la faculté des arts de l’Université de Strasbourg](https://arts.unistra.fr/formations/masters/master-design/environnements-numeriques) autour des extension navigateur