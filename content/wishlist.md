---
title: Idées de cadeaux
slug: wishlist
date: 2022-11-11
description: Idées de cadeaux
draft: true
stats:
  - name: Poids de la page
    dots: ...
    value: 42Ko
  - name: Nombre de requêtes
    dots: .
    value: 8
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 135
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

## bandes dessinées

- ~~[Saint-Elme t.2 : l'avenir de la famille](https://www.placedeslibraires.fr/livre/9782413043096-saint-elme-t-2-l-avenir-de-la-famille-serge-lehman-frederik-peeters/), Serge Lehman, Frederik Peeters, Delcourt, 16.95€~~
- ~~[Saint-Elme t.3 : le porteur de mauvaises nouvelles ](https://www.placedeslibraires.fr/livre/9782413044499-saint-elme-t-3-le-porteur-de-mauvaises-nouvelles-serge-lehman-frederik-peeters/), Serge Lehman, Frederik Peeters, Delcourt, 16.95€~~

## objets

- [Handover : Vegan Signwriting Kit with Brush Case](https://www.handover.co.uk/vegan-signwriting-kit-with-brush-case/)
- [Handover : Wooden Kit Box](https://www.handover.co.uk/handover-wooden-kit-box-45-x-35-x-20cm-quality-1/) 45 x 35 x 20cm : QUALITY 1
- [Pinceaux Handover 2112 Series](https://www.handover.co.uk/handover-sable-chisel-writer-brushes/) : tailles 5, 6, 7
- ~~[Mug Hasami en porcelaine](https://www.latresorerie.fr/fr/cafe-et-the/881-120375-mug-japonais-hasami.html#/704-couleurs_hasami-noir), couleur noir, 29.80€~~
- ~~[Cafetière italienne ou à piston Bodum CHAMBORD 1923-16](https://www.darty.com/nav/achat/encastrable/casserolerie/cafetiere_italienne/bodum_chambord_1923-16.html), 0.35L, 49.99€~~
- [Ginkgo Japanese Coffee Kettle](https://food52.com/shop/products/5104-japanese-coffee-kettle), 75$

## hi-tech

- ~~[Écouteurs Fairphone True Wireless](https://shop.fairphone.com/fr/accessories/true-wireless-stereo-earbuds), couleur gris, 99.55€~~
