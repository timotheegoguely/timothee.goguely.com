---
title: Timothée Goguely
slug: home
date: 2021-08-06
description: design & code
summary: 'Designer et développeur web spécialisé en écoconception et accessibilité numérique. <br>&emsp;J’enseigne également l’UI/UX design et les bases de l’HTML et du CSS en tant que maître de conférence associé à temps partiel au sein du département Arts visuels de l’Université de Strasbourg.'
image: /media/cover.png
hero:
  image:
    src: 'media/atelier-du-quai.png'
    alt: 'Vigne grimpante recouvrant presque tout le mur et encadrant les ouvertures de la façade. Des plantes, une table et 2 chaises d’extérieur sont disposées sur la petite terrasse surélevée devant la porte vitrée entre ouverte, où l’on peut lire « Atelier du Quai ».'
    caption: 'La façade de l’atelier où je travaille à Strasbourg, quand je ne suis pas sur le campus de l’Unistra.'
stats:
  - name: Poids de la page
    dots: ...
    value: 119Ko
  - name: Nombre de requêtes
    dots: .
    value: 8
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 278
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

---

## Compétences
### Design

* définition des besoins
* architecture de l’information
* identité visuelle, logo, charte graphique
* web design / interface utilisateur <small>(<abbr title="User Interface" lang="en">UI</abbr>)</small>
* expérience utilisateur <small>(<abbr title="User Experience" lang="en">UX</abbr>)</small>
* prototypage
* <span lang="en">design system</span>
* édition, mise en page

### Code

* développement front-end <small>(HTML, CSS, JS)</small>
* <abbr title="Content Management System" lang="en">CMS</abbr> <small>(Kirby, Strapi, Static CMS, Mattrbld)</small>
* <abbr title="Static Site Generators" lang="en">SSG</abbr> <small>(Hugo, <abbr title="Eleventy" lang="en">11ty</abbr>)</small>
* accessibilité <small>(a11y)</small>
* référencement <small>(<abbr title="Search Engine Optimization" lang="en">SEO</abbr>)</small>        
* optimisations & performances
* déploiement & maintenance
* <span lang="en">web2print</span> <small>(paged.js)</small>

---

## Objectif
### Construire un monde soutenable

Je souhaite mettre mes compétences au profit de personnes, d’initiatives et d’organisations qui prennent le soin de **protéger**, **restaurer** et **améliorer** la diversité de la vie sur Terre, ainsi que la **dignité**, l’**autonomie** et l’**épanouissement** de ses habitants.

---

## Approche
### Écoconception

Conscient des enjeux liés à l’[impact environ&shy;nemental croissant du numérique](https://theshiftproject.org/article/impact-environnemental-du-numerique-5g-nouvelle-etude-du-shift/), je participe à la sensibilisation en faveur de davantage de sobriété numérique et au développement de projets web **utiles**, **conviviaux**, **accessibles** et **durables**.

---

## Projets
### Sélection de projets
