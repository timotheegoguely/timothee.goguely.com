---
title: Le statut d’entrepreneur salarié au sein d’une coopérative d’activité et d’emploi (CAE)
slug: statut-entrepeneur-salarie-cae
date: 2023-12-31
lastmod: 2024-04-12
description: Retour d’expérience et bilan concernant le choix de mon statut professionnel. 
tags:
  - retour d’expérience
  - coop
  - vie professionnelle
image: /media/screenshot-louty-20231231.png
comments: https://mastodon.design/@timotheegoguely/111676124348796511
stats:
  - name: Poids de la page
    dots: ...
    value: 52Ko
  - name: Nombre de requêtes
    dots: .
    value: 7
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 446
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

En tant que designer / développeur indépendant, le choix du statut professionnel sous lequel exercer son activité en France n’est pas évident. Assez peu abordé en écoles d’art et de design – durant mon parcours entre 2009 et 2014, je n’ai eu qu’une seule intervention d’une personne de l’<abbr title="Alliance Française des Designers">AFD</abbr> un après-midi en 4<sup>e</sup> année à l’ésad d’Amiens – il est pourtant central pour toute personne travaillant à son compte.

Comme beaucoup de monde, j’ai appris sur le tas en demandant autour de moi, durant mes stages, auprès des gens avec qui j’ai travaillé, de mes anciens camarades de classe, et en fouillant bien sûr dans quelques livres, guides, sites web et forums spécialisés.

**Quel est le statut le plus adapté à sa pratique ?** Comment rédiger un devis, une facture, des conditions générales de vente ? Combien et comment vendre son travail ? Au forfait, au temps, à la tâche ? De combien a-t-on besoin pour vivre dignement ?

Chacune de ses questions mériterait d’y consacrer un article à part entière, mais j’aimerais me concentrer ici sur la première d’entre elles, en vous partageant mon expérience concernant le statut qui est aujourd’hui le mien, un statut peu courant, car souvent méconnu : celui **d’entrepreneur·e-salarié·e au sein d’une coopérative d’activité et d’emploi**.

---

Sommaire

<nav>

- [Mes débuts en tant qu’artiste-auteur à la Maison des Artistes](#artiste-auteur-mda)
- [Ma première expérience au sein d’une CAE](#ma-premiere-experience-au-sein-dune-cae)
- [Qu’est-ce qu’une coopérative d’activités et d’emploi (CAE) ?](#cooperative-activite-emploi-cea)
- [Concrètement, comment ça se passe ?](#concretement-comment-ca-se-passe)
- [Désillusions](#desillusions)
- [Nouveau départ](#nouveau-depart)
- [Bilan 2020–2023](#bilan-2020-2023)
- [Objectifs et envies pour 2024](#objectifs-envies-2024)
- [Liste de professionnel·les du design, du web ou du numérique membres d’une CAE](#liste-pro-design-web-numerique-membres-cae)
- [Pour aller plus loin](#pour-aller-plus-loin)

<nav>

---

<h2 id="artiste-auteur-mda">Mes débuts en tant qu’artiste-auteur à la Maison des Artistes</h2>

Durant ma dernière année d’études à l’ésad d’Amiens en 2014, je me suis inscrit à la Maison des Artistes (MDA) en tant qu’artiste-auteur afin de répondre à quelques premières petites commandes. À l’époque, ce choix de statut était plutôt adapté à ce que je faisais (création d’identités visuelles et design d’interface essentiellement), mais j’ai mis des années à savoir quoi indiquer sur mes devis et mes ~~factures~~ ~~notes d’auteur~~ factures.

Entre les différentes mentions obligatoires, le fait de préciser qu’ils s’agissait de « conception et réalisation d’œuvres graphiques originales », le précompte au début, le calcul des cessions de droits, la TVA non-applicable (art. 293 B-III du Code Général des Impôts), les différentes cotisations sociales dont les taux changeaient presque chaque année, l’histoire du 1,1% diffuseur, savoir quel montant déclarer aux impôts au sein du formulaire 2042 C PRO et sans se tromper de case… il y avait vraiment de quoi devenir dingue.

Au-delà de ces complexités administratives, du temps à y consacrer et du stress permanent lié au fait de ne jamais vraiment savoir si on fait les choses correctement et à la crainte de se réveiller un jour en découvrant que les huissiers ont saisi tout votre argent sur votre compte bancaire pour une erreur de déclaration à l’URSSAF (_true story_, c’est arrivé à un ami), l’une des particularités de ce statut est qu’il ne permet légalement de facturer que des activités étant considérées comme relevant de la « création d’œuvres originales ».

En effet, la liste des activités autorisées en tant qu’artiste-auteur est assez restrictive, même si celle-ci a été un peu élargie depuis l’entrée en application en janvier 2021 du [décret n° 2020-1095 du 28 août 2020 relatif à la nature des activités et des revenus des artistes-auteurs,](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042284065) aboutissement de [15 ans de travail et de plaidoyers de l’Alliance Française des Designers](http://www.alliance-francaise-des-designers.org/blog/2020/09/05/historique-statut-designer-un-statut-social-unique-pour-tous-les-auteurs-d-oeuvres-de-design.html) auprès des pouvoirs publics.

Impossible par exemple, à l’époque, de facturer des prestations de développement web, d’impression ou de conseil : il aurait fallu pour cela un second statut, et l’idée de cumuler plusieurs statuts me semblait bien trop lourde et source de confusions pour que je puisse même l’envisager.

Pour résumer voilà les différentes raisons qui m’ont poussé à expérimenter un autre statut que celui d’artiste-auteur :

- **un seul statut adapté pour l’ensemble de mon activité** : design et développement web, enseignement, sans devoir cumuler plusieurs statuts
- **alignement avec mes valeurs** : dimension coopérative et collective
- **conformité avec la loi** : être sûr à 100% d’être en règle auprès des impôts et des pouvoirs publics
- **protection sociale** : cotiser au chômage et à la retraite + droit aux indemnités journalières de la CPAM en cas d’arrêt de travail ou de congé paternité
- **stabilité financière** : être salarié en CDI et avoir des revenus fixes (ça aide pas mal quand vous cherchez à louer un appartement par exemple)
- **accompagnement professionnel** : pouvoir être conseillé dans son projet professionnel, avoir quelqu’un qui répond à vos questions sans vous juger
- **formation** : en tant que salarié (versus auto-entreprise), le fait de cotiser au fonds de formation des salariés permet de co-financer une partie de ses formations
- **émancipation** : expérimenter des formes de travail et de collaborations nouvelles

---

<h2 id="ma-premiere-experience-au-sein-dune-cae">Ma première expérience au sein d’une CAE</h2>

<time class="mono opacity-60">mai 2017 → décembre 2018</time>

Début 2017, j’ai donc commencé à me renseigner sur les autres statuts possibles. J’ai alors participé à différentes réunions d’informations à Paris organisées par des cabinets d’experts comptables spécialisés pour les indépendants et différentes coopératives d’activités et d’emploi (CAE) telles que [Coopaname](https://www.coopaname.coop/), [Smart](https://smartbe.be/fr/) et [Omnicité](https://omnicite.fr) (qui s’appelait encore Port Parallèle à l’époque).

L’idée était d’essayer de bien comprendre les spécificités des différentes options que j’envisageais (auto-entreprise, SA, SARL, SAS, portage salarial, entrepreneur-salarié), leurs avantages et leurs inconvénients respectifs. Sachant que j’avais *a priori* une préférence pour les coopératives du point de vu des valeurs et du modèle social, mais que je ne connaissais pas du tout comment cela fonctionnait concrètement et si c’était même tout simplement compatible avec mon activité.

**J’insiste sur l’importance de participer de ces réunions d’informations pour toute personne qui souhaiterait rejoindre une CAE. Car toutes les CAE ne sont pas identiques, loin de là.** Au delà de leur emplacement géographique (il y en existe plus de 150 dans toute la France) et de leur taille (cela peut aller de moins d’une dizaine de personnes à plusieurs centaines), il existe en effet des coopératives spécialisées dans certaines filières (culture, services à la personne, bâtiment, formation, agriculture…) et d’autres dites « généralistes » qui accueillent des activités relevant de secteurs très variées : art, artisanat, design, paysagisme, architecture, journalisme, traduction, coaching, soin, consulting, informatique et j’en passe.

Pour trouver une CAE près de chez vous et de votre secteur d’activité, vous pouvez consulter cet [annuaire des CAE en France](https://www.les-cae.coop/trouver-une-cae-0). 

---

<h2 id="cooperative-activite-emploi-cea">Qu’est-ce qu’une coopérative d’activités et d’emploi (CAE) ?</h2>

C’est donc pendant ces fameuses réunions d’informations que l’on m’a expliqué ce qu’est une CAE. En voici une définition qui me semble assez précise, trouvée sur le site [les-cae.coop](https://www.les-cae.coop/qu-est-ce-qu-une-cae) :

> Les CAE, Coopératives d’Activité et d’Emploi, sont des entreprises coopératives qui proposent [un cadre juridique](https://www.les-cae.coop/le-cadre-juridique), économique, social et humain à toutes celles et tous ceux qui souhaitent créer, développer puis de stabiliser leur activité entrepreneuriale.
> 
> La CAE permet à une personne (ou un collectif de personnes) d’exercer une **activité entrepreneuriale** pérenne, dans son ou ses champs d’expertise et au sein d’une structure collective et coopérative.
> 
> L’originalité de la CAE repose sur le fait qu’elle offre un statut d’entrepreneur salarié qui permet de percevoir un **salaire** généré depuis le chiffre d’affaires de l’activité, de bénéficier de la même **couverture sociale** qu’un·e salarié·e classique, tout en étant **autonome** sur l’exercice de son activité professionnelle.
> 
> L’autre particularité est que l’entrepreneur·e devient **associé·e** de sa coopérative et participe aux grandes décisions de celles-ci.

Constituée juridiquement sous la forme d’une [Scop (Société coopérative et participative)](https://www.les-scop.coop/les-scop), d’une [Scic (Société d’intérêt collectif)](https://www.les-scop.coop/les-scic) ou de coopérative Loi 47, une CAE propose donc **« un modèle d’entrepreneuriat salarié unique**, qui permet de créer et de développer **sa propre activité** dans un **cadre autonome, coopératif et sécurisé ».**

---

<h2 id="concretement-comment-ca-se-passe">Concrètement, comment ça se passe ?</h2>

**Le parcours d’intégration d’une CAE se fait en plusieurs étapes**, chaque nouvelle étape correspondant à une augmentation progressive de votre chiffre d’affaires et du niveau de maturité de votre activité.

### Réunion d’information et candidature

Tout parcours d’entrée commence systématiquement par une réunion d’accueil collective durant laquelle on vous présente ce qu’est une CAE, comment ça fonctionne, les avantages et inconvénients, à qui cela s’adresse, les éventuelles spécificités de cette CAE par rapport aux autres. C’est également le moment de poser toutes vos questions et de sentir un peu l’ADN de la CAE, son état d’esprit, l’ambiance qui y règne et le type de personnes qui en font partie. À la suite de cette réunion d’information, vous avez la possibilité de candidater afin de rejoindre votre CAE en remplissant un formulaire et en fournissant différents documents et justificatifs.

### <abbr title="Contrat d’Appui au Projet d’Entreprise">CAPE</abbr> : test de votre activité économique et accompagnement

Si votre candidature est retenue, il vous est proposé une sorte de période d’essai sous la forme d’un **Contrat d’Appui au Projet d’Entreprise (CAPE)**, parfois appelé « convention de partenariat », d’une durée maximale de 1 an en général, renouvelable 2 fois (donc 3 ans maximum de CAPE possible). C’est un contrat commercial signé entre la CAE et l’entrepreneur qui permet de continuer à être indemnisé par Pôle emploi (si vous l’êtes) tout en commençant à facturer vos prestations et à accumuler de la trésorerie qui vous permettra par la suite de vous salarier. L’objectif est de prendre vos marques, de vous familiariser avec le fonctionnement, le jargon et les outils de la CAE, et d’être accompagné au lancement et au développement de votre activité professionnelle dans de bonnes conditions.

### <abbr title="Contrat d’Entrepreneur·e Salarié·e Associé·e">CESA</abbr> : CDI entrepreneur·e salarié·e

Une fois votre contrat CAPE arrivé à son terme et si cela s’est avéré concluant pour vous comme pour la CAE (revenus et marge suffisants pour vous rémunérer), c’est le moment de signer votre CDI, sous la forme d’un **Contrat d’Entrepreneur·e Salarié·e Associé·e (CESA)**. L’objectif est maintenant d’atteindre un niveau de viabilité économique de votre activité dans la durée. Votre salaire dépend de votre activité et peut être revu régulièrement en fonction de l’évolution de vos besoins et de votre chiffre d’affaire. Vous payez les cotisations relatives à votre salaire (sociales + patronales) et la contribution coopérative, généralement comprise entre 8 et 15 % de votre chiffre d’affaires ou de votre marge brute selon les CAE. Celle-ci permet de financer les services mutualisés, tels que les outils informatiques de gestion comptable, l’accompagnement (2 rendez-vous minimum/an obligatoires) ainsi que les frais de fonctionnement de la CAE.

### CESA : CDI entrepreneur·e associé·e

Au bout de 3 ans au plus tard à compter de la signature de votre contrat CAPE, il vous est obligatoirement proposé de **devenir sociétaire de votre CAE**. Si votre candidature est retenue, vous participez désormais à la gouvernance partagée de votre CAE selon le principe démocratique d’1 personne = 1 voix.

### Sortie éventuelle

À tout moment de votre parcours au sein d’une CAE, il vous est possible de la quitter pour reprendre une activité salariée ou créer votre propre entreprise par exemple, dans le respect de certaines conditions propres à chaque CAE. L’équipe de la CAE vous accompagnera tout au long de votre sortie, vous bénéficiez des conditions de rupture de contrat salarié définies par le Code du travail et vous conservez tous vos droits sociaux acquis.

Pour en savoir davantage sur ces différentes étapes, je vous invite à consulter la plaquette [Entreprendre en CAE (PDF · 2,2 Mo)](https://www.les-cae.coop/system/files/inline-files/Entreprendre%20en%20CAE%20-%20Plaquette%202022%20Vdiff_0.pdf) publiée par la [Fédération des CAE](https://www.les-cae.coop/).

---

<h2 id="desillusions">Désillusions</h2>

**Après un an et demi au sein de Port Parallèle, j’ai dû me rendre à l’évidence : mes revenus étaient beaucoup trop faibles compte tenu de mes compétences et de mon expérience**. J’ai mis plusieurs mois à me l’avouer, car cela était pour moi synonyme d’échec professionnel et venait ébranler mes espoirs quant à la viabilité économique de formes d’organisation du travail alternatives. 

**Par convictions politiques et par profond dégoût et rejet des startups et de leur monde, je me suis toujours refusé de « faire carrière » comme UI/UX designer au sein de je ne sais quelle « boite » parisienne, malgré la forte demande et même si cela signifiait gagner deux voir trois fois moins que ce à quoi je pourrais prétendre comme salarié ou indépendant sur le « marché du travail »**. Je n’ai jamais regretté ce choix, mais je dois avouer qu’à cette période de ma vie, je me suis souvent poser la question du bien fondé de cette posture. Certaines personnes de mon entourage, lorsque je leur en parlais, ne comprenaient pas que je sacrifie un confort financier à portée de main pour de « simples » raisons idéologiques.

Au mieux de cette période, je me versais en effet un salaire tout juste équivalent au SMIC de l’époque, à savoir un peu moins de 1500€ brut. Une fois les différentes cotisations déduites, je touchais environ 1290€ net chaque mois, auxquels s’ajoutaient les remboursements de notes de frais (3340€ au total sur l’année 2018, soit environ 278€ / mois en moyenne). Et avec le coût exorbitant des loyers et de la vie à Paris, cela devenait de plus en plus difficile à assumer. Ma compagne et moi-même n’étions pas non plus dans une situation de pauvreté économique, loin de là (le seuil de pauvreté, au seuil de 60% du revenu médian, était de 970€ en 2020), mais je finissais régulièrement le mois dans le rouge, et disons que j’avais l’impression d’être responsable, par mes choix, de cette situation financière précaire.

Après analyse des causes et des leviers à ma disposition, le principal facteur selon moi était que **je m’étais habitué à facturer mes missions selon un <abbr title="Taux journalier moyen">TJM</abbr> trop bas**, ce qui est l’un des pièges dans lequel beaucoup de personnes s’enferment en début d’activité. À ce moment là, mon TJM oscillait entre 350€ et 400€ HT en fonction de mes clients. Pour le jeune designer affilié à la MDA que j’étais, c’était largement suffisant, notamment car j’avais un client principal pour qui je travaillais très souvent et qui m’assurait une relative stabilité financière. 

Certaines personnes seraient tentées de se dire que même à 350€ / jour, tout va bien ! Il suffit de travailler 5 jours par mois et hop, 1750€ dans la poche ! Sauf que non : ces 1750€, c’est votre chiffre d’affaire, et lorsque vous voulez sortir de l’argent de votre trésorerie sous forme de salaire, celui-ci est imposé et soumis à cotisations sociales et patronales (santé, retraite, CSG, assurance chômage, etc.). Sans compter la contribution à votre CAE (entre 8 et 15% pour rappel).

**Donc si vous voulez un salaire net après impôt de 1750€, cela correspond en réalité à un brut de 2323€ et à un coût total employeur de 2903€**, soit 10 jours facturés et non 5, si vous souhaitez en plus pouvoir vous rembourser 250€ de notes de frais chaque mois. Je vous invite à jouer avec [ce simulateur du coût d’un salarié](https://mon-entreprise.urssaf.fr/simulateurs/salaire-brut-net) si vous n’êtes pas familier avec tout cela. 

Et toute personne travaillant à son compte sait que sur 20 jours ouvrés par mois en moyenne, en facturer effectivement ne serait-ce que la moitié, n’est pas chose aisée : le temps de gestion comptable, de démarchage et de congés ne sont pas des temps facturés, il arrive d’avoir des creux en terme de missions certains mois, certains projets s’étalent dans le temps ou ont été mal estimés, certains clients payent parfois avec plusieurs mois de retard… tout ceci fait partie intégrante de toute activité indépendante et constitue souvent la partie la plus complexe et stressante, et pourtant la moins enseignée.

Vous l’aurez donc compris, **arriver à se dégager un salaire suffisant en tant qu’entrepreneur·e-salarié·e au sein d’une CAE n’est pas si simple, car cela nécessite d’avoir une activité stable et économiquement viable, ce qui peut parfois demander plusieurs années d’expériences** – le montant se cachant derrière « suffisant » pouvant par ailleurs fortement varier d’une personne à l’autre en fonction de ses besoins et de sa situation personnelle et géographique.

Face à cette situation, j’ai donc fait une demande fin 2018 de rupture conventionnelle auprès de ma CAE et décidé de déclarer de nouveau mon activité via mon statut d’artiste-auteur à la MDA – que j’avais eu la bonne idée de ne pas clôturer tout de suite, au cas où – à compter de janvier 2019, dans l’espoir de me refaire un peu financièrement.

---

<h2 id="nouveau-depart">Nouveau départ</h2>

<time class="mono opacity-60">depuis août 2020</time>

Comme je l’expliquais [ici](https://timothee.goguely.com/blog/avant-propos/), j’ai quitté le studio de design numérique Figs, pour lequel je travaillais depuis 5 ans, durant l’été 2019. Cela n’a pas été une décision facile, notamment du point de vue économique, car mon activité reposait à cette époque en grande partie sur les missions que me confiait ce studio. **Disons que j’avais fait l’erreur, par confort, de mettre tous mes œufs dans le même panier. Cesser de travailler pour eux signifiait donc que je devais me reconstituer rapidement mon propre réseau de collaborateur·rices et de clients, afin de ne pas me retrouver sans revenus.**

J’ai donc commencé à cette période à rencontrer et à tisser des liens avec différentes personnes. Quelques premières collaborations se sont petit à petit mises en places, c’était encourageant, mais c’est là qu’est arrivée la pandémie de Covid-19 au printemps 2020. Par chance, m’étant progressivement un peu éloigné de l’UI/UX design pour me concentrer sur le design et le développement web, cela n’a pas trop impacté mon activité car beaucoup de structures et de collectifs ont profité de cette période pour créer ou repenser leur site web.

Durant l’été 2020, fraîchement sortis du premier confinement, [Gauthier Roussilhe](https://gauthierroussilhe.com/) et moi même avons proposé à une douzaine de designers dont nous apprécions le travail et l’engagement de nous retrouver quelques jours à Massiac dans le Cantal. De cette [première résidence estivale](https://designcommun.fr/evenements/massiac), qui tombait à point nommé après ces longues semaines d’isolement, est née l’association [design↔commun](https://designcommun.fr/). 

C’est également à l’occasion de cette rencontre que j’ai fait la connaissance de Lysiane Lagadic, Mikhaël Pommier, Nicolas Loubet et Sylvia Fredriksson, tous les quatre membres à cette époque d’[Oxamyne](https://www.oxamyne.fr/), « une structure coopérative basée à Lyon, à vocation stratégique, prospective et pragmatique au service des transitions », co-portée par l’association [la MYNE](https://lamyne.org) et la coopérative [Oxalis](https://www.oxalis-scop.fr/). **Discuter avec ces personnes m’a donné envie de les rejoindre en m’offrant une deuxième chance au sein d’une CAE.** 

**C’est comme ça que je me suis retrouvé en août 2020 à intégrer Oxalis directement en tant qu’entrepreneur-salarié** (sans passer par l’étape CAPE), car j’avais déjà connaissance du fonctionnement d’une CAE, un projet professionnel bien défini et une activité suffisante pour me permettre de me dégager un salaire.

---

<h2 id="bilan-2020-2023">Bilan 2020–2023</h2>

### Résultats annuels

Dans un souci de **transparence**, de **pédagogie**, et parce qu’**il faut arrêter d’avoir peur de parler d’argent**, voici une synthèse de mes résultats annuels depuis mon entrée à Oxalis en août 2020 :

<table class="table-mono">
  <caption>Résultats annuels de 2020 à 2023 (€).</caption>
  <thead>
    <tr>
      <th style="text-align:left">Soldes, comptes et écritures</th>
      <th>2020</th>
      <th>2021</th>
      <th>2022</th>
      <th>2023</th>
      <th>Solde</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Chiffre d’affaires (CA)</th>
      <td>17 096</td>
      <td>35 593</td>
      <td>33 290</td>
      <td>48 110</td>
      <td>134 089</td>
    </tr>
    <tr>
        <th scope="row">Achats déduits contribution</th>
        <td></td>
        <td>−741</td>
        <td>−681</td>
        <td>−1 868</td>
        <td>−3 290</td>
    </tr>
    <tr>
        <th scope="row">= MARGE SOUMISE À CONTRIBUTION</th>
        <td>17 096</td>
        <td>34 852</td>
        <td>32 609</td>
        <td>46 242</td>
        <td>130 800</td>
    </tr>
    <tr>
        <th scope="row">− Charges de fonctionnement (NDF)</th>
        <td>−228</td>
        <td>−3 957</td>
        <td>−3 215</td>
        <td>−3 803</td>
        <td>−11 203</td>
    </tr>
    <tr>
        <th scope="row">+ Autres produits</th>
        <td>97</td>
        <td></td>
        <td>1 000</td>
        <td>1209</td>
        <td>2 307</td>
    </tr>
    <tr>
        <th scope="row">= VALEUR AJOUTÉE</th>
        <td>16 965</td>
        <td>30 896</td>
        <td>30 395</td>
        <td>43 648</td>
        <td>121 904</td>
    </tr>
    <tr>
        <th scope="row">− Contribution Coopérative</th>
        <td>−2 564</td>
        <td>−2 000</td>
        <td>−2 663</td>
        <td>−3 849</td>
        <td>−11 077</td>
    </tr>
    <tr>
        <th scope="row">+ Report à nouveau</th>
        <td>−2 624</td>
        <td>−25</td>
        <td>8 419</td>
        <td>−1 889</td>
        <td>−6 530</td>
    </tr>
    <tr>
        <th scope="row">= MARGE NETTE DISPONIBLE</th>
        <td>11 777</td>
        <td>28 870</td>
        <td>32 270</td>
        <td>31 380</td>
        <td>104 295</td>
    </tr>
    <tr>
        <th>− Rémunérations</th>
        <td>−10 775</td>
        <td>−28 870</td>
        <td>−32 270</td>
        <td>−31 380</td>
        <td>−103 334</td>
    </tr>
    <tr>
        <th>− Résultat mutualisé</th>
        <td>−1 002</td>
        <td></td>
        <td></td>
        <td></td>
        <td>−1 002</td>
    </tr>
  </tbody>
</table>

### Taux journalier moyen

**En 2020, mon <abbr title="Taux journalier moyen">TJM</abbr> variait entre 400€ et 500€ HT** selon les clients.
**Fin 2023, il est maintenant de 600€ HT**, soit 100–200€ d’augmentation en trois ans :

<table class="table-mono">
  <caption>Évolution de mon TJM de 2020 à 2023 (€ HT).</caption>
  <thead>
    <tr>
      <td></td>
      <th scope="col">2020</th>
      <th scope="col">2021</th>
      <th scope="col">2022</th>
      <th scope="col">2023</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">TJM</th>
      <td>400–500</td>
      <td>450–550</td>
      <td>500–600</td>
      <td>500–600</td>
    </tr>
  </tbody>
</table>

### Salaires

Côté salaires (brut HT), cela a fortement varié au cours de ces trois dernières années : 

- 1900€ d’août 2020 à octobre 2020*
- 1000€ de novembre 2020 à janvier 2021
- 1250€ en février et mars 2021
- 1550€ d’avril à juin 2021
- 2000€ de juillet 2021 à mai 2022
- 2000€ de juin 2022 à mai 2023
- 2100€ de juin 2023 à août 2023
- 2200€ depuis septembre 2023

\*J’avais volontairement gonflé mon salaire au delà de ce que permettait mon prévisionnel à mon entrée à Oxalis, afin d’augmenter nos chances de trouver un appartement en location avec ma compagne lorsque nous avons quitté Paris pour Strasbourg à l’automne 2020. D’où la douloureuse baisse en novembre 2020.

**Cela peut surprendre, mais c’est aussi l’un des avantages de ce statut : pouvoir moduler son salaire en fonction de ses besoins**. Évidemment, cela se fait en fonction de ce que vous permet votre trésorerie et avec l’accord de la personne qui vous accompagne de sein de votre CAE. J’ai en effet rendez-vous tous les trimestres environ avec mon accompagnatrice Éléonore Charles – que je remercie au passage pour la qualité de son écoute et de ses conseils, ainsi que pour sa relecture de cet article. Durant ces entretiens de suivi, nous faisons systématiquement un point sur ma trésorerie et mon activité, en passant en revue les projets terminés (à facturer), en cours, à venir (sûrs) et potentiels (peut-être), de façon à décider sur l’on maintient, baisse ou augmente mon salaire.  Je profite également de ces moments pour lui poser toutes mes questions administratives concernant par exemple mes notes de frais, comment fonctionne l’amortissement de l’achat de matériel supérieur à 500€, comment deviser pour un client ne résidant pas en France, ou encore comment ajouter ma compagne à mon contrat de mutuelle. 

**Cet accompagnement est très précieux pour moi car il me permet d’avoir une vision claire de là en j’en suis à intervalles réguliers**, et de ne pas me sentir seul à mener ma petite barque, ce qui est très appréciable.

---

<h2 id="objectifs-envies-2024">Objectifs et envies pour 2024</h2>

- **Augmenter mon TJM entre 600–700€ HT** en fonction des missions
- **Enseigner moins mais mieux** : selon les opportunités et les créations de postes à venir, il est fort possible que je réduise un peu mes heures à LISAA Strasbourg (je ne garderais qu’un cours consacré à l’écoconception numérique et quelques heures sur les fondamentaux de la typographie) pour me consacrer davantage à l’enseignement au sein du Master Design « Environnements numériques » de l’Université de Strasbourg, ce qui me permettrait de compléter également mes revenus et donc de ne pas forcément chercher à augmenter le chiffre d’affaire que je réalise au sein d’Oxalis
- **Continuer à me former** : notamment à l’accessibilité et aux performances web, ainsi qu’à l’utilisation des Web Components
- **Prendre le temps d'écrire plus d’articles sur ce blog** : ce ne sont pas les sujets qui manquent pourtant…
- **Refaire mon site web** : j’ai envie d’explorer pleins d’idées en ce moment !
- **Possible changement de coopérative** : je suis en train de me renseigner pour voir s’il ne serait pas plus intéressant pour moi que je rejoigne la CAE [Artenréel](https://artenreel.fr/) basée à Strasbourg. Affaire à suivre…

---

<h2 id="liste-pro-design-web-numerique-membres-cae">Liste de professionnel·les du design, du web ou du numérique membres d’une CAE</h2>

L’idée de cette petite liste et de vous permettre d’**identifier et contacter facilement des personnes travaillant dans le milieu du design, du web ou du numérique** (graphisme, design, développement, accessibilité web…), étant membre d’une CAE et habitant près de chez vous. Si vous souhaitez y apparaître, surtout si votre département ou votre CAE ne sont pas représentés, écrivez-moi simplement sur [Mastodon](https://mastodon.design/@timotheegoguely) ou par email.

|Contact|Activité(s)|Département|CAE|
|---|---|---|---|
|[@vincent@mamot.fr](https://mamot.fr/@vincent)|développeur|Loire|[Codeurs en Liberté](https://codeursenliberte.fr/)|
|[@clairezed@octodon.social](https://octodon.social/@clairezed)|développeuse web|Côtes-d’Armor|[Coopaname](https://www.coopaname.coop)|
|[@AugierLe42e@diaspodon.fr](https://diaspodon.fr/@AugierLe42e)|développeur|Haute-Garonne|[Coopaname](https://www.coopaname.coop)|
|[@greenman@diaspodon.fr](https://diaspodon.fr/@greenman)|SysAdmin, formateur|Val-de-Marne|[Coopaname](https://www.coopaname.coop)|
|[@supertanuki@toot.aquilenetfr](https://toot.aquilenet.fr/@supertanuki)|développeur, podcasteur|Île-de-France|[Coopaname](https://www.coopaname.coop)|
|[@turb@mamot.fr](https://mamot.fr/@turb)|développeur back|Rhône|[Graines de SOL](https://grainesdesol.fr/)|
|[Lysiane Lagadic](https://fr.linkedin.com/in/lysiane-lagadic-56254ba8)|designer|Finistère|[Oxalis](https://www.oxalis-scop.fr/)|
|[Mikhaël Pommier](https://fr.linkedin.com/in/mikhaelpommier)|designer, chercheur|Finistère|[CAE29](https://www.cae29.coop/)|

---

<h2 id="pour-aller-plus-loin">Pour aller plus loin</h2>

**Une sélection de quelques liens et ressources pour approfondir le sujet.** N’hésitez par à me contacter pour m’en suggérer d’autres, je complèterai l’article avec plaisir, le but étant que cela serve au plus grand nombre.

- [Fédération des CAE](https://www.les-cae.coop/), issu de la fusion de [Coopérer pour Entreprendre](https://cooperer.coop/) et de [Copéa](https://www.copea.fr/)
- [C’est quoi ta vie d’entrepreneur·euse en coopérative ?](https://vivreletravail.net/enquete-cest-quoi-ta-vie-dentrepreneur%c2%b7euses-en-cae/), enquête réalisée par [Ladyss](https://www.ladyss.com/), la [Manufacture Coopérative](http://manufacture.coop/) et [Viv(r)e le travail autrement](https://vivreletravail.net/), 25 mai 2022 (résultats à paraître)
- [Bigre!](https://www.bigre.coop/) : « projet intercoopératif initié par [Coopaname](https://www.coopaname.coop/), [Smart](https://smartbe.be/fr/), [Oxalis](https://www.oxalis-scop.fr/) et leurs écosystèmes respectifs », organisant notamment chaque été une semaine de rencontres rassemblant jusqu’à 300 personnes issues d’une trentaine d’organisations différentes.
- [Statut, TJM et clients, la base du freelance](https://mixitconf.org/2023/statut-tjm-et-clients-la-base-du-freelance), l’excellente conférence de Raphaël Yharrassarry présentée durant l’édition 2023 de MiXiT.
