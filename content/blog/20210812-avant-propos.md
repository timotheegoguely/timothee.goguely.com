---
title: Avant-propos
slug: avant-propos
date: 2021-12-08
publishdate: 2021-12-08
description: Aujourd’hui j’ai 30 ans, et ceci est mon premier billet de blog. J’ai décidé de profiter de cette occasion pour me présenter, revenir sur mon parcours et introduire les sujets et thématiques que je compte aborder ici, de façon à permettre celles et ceux qui ne me connaîtraient pas ou peu de mieux situer mes propos.
tags:
  - vie professionnelle
  - retour d’expérience
image: /media/atelier.png
draft: false
stats:
  - name: Poids de la page
    dots: ...
    value: 65Ko
  - name: Nombre de requêtes
    dots: .
    value: 9
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 118
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

Aujourd’hui j’ai 30 ans, et ceci est mon premier billet de blog.

J’ai décidé de profiter de cette occasion pour me présenter, revenir sur mon parcours et introduire les sujets et thématiques que je compte aborder ici, de façon à permettre celles et ceux qui ne me connaîtraient pas ou peu de mieux situer mes propos.

Et histoire que ce soit un peu plus fun à lire pour vous (et à écrire pour moi) j’ai décidé de rédiger ça sous la forme d’une auto-interview.
Bonne lecture&#8239;!

---

## Bonjour Timothée, peux-tu te présenter en quelques mots&#8239;?

Timothée Goguely, tout juste la trentaine, fils d’un père graphiste en imprimerie avant de se reconvertir dans la formation professionnelle, et d’une mère professeure des écoles dans le public et directrice d’école maternelle sur la fin de sa carrière. J’ai eu la chance, notamment grâce à leur soutien, de pouvoir choisir et poursuivre des études supérieures qui me permettent aujourd’hui d’exercer le métier de designer et développeur web. 

Depuis quelques années, je m’intéresse notamment aux problématiques liées à l’accessibilité et aux impacts environnementaux du numérique, et plus généralement aux communs, aux coopératives, ainsi qu’aux façons de vivre et de collaborer ensemble qui soient respectueuses des personnes et du vivant.

## Où habites-tu&#8239;?

À Strasbourg, depuis août 2021, où je loue un appartement avec <a href="https://margotcannizzolazaro.fr/" rel="noopener" target="_blank">Margot</a> (ma compagne), ainsi qu’un petit atelier pour travailler.

<figure>
  <picture class="filter-text-current">
    <img src="/media/atelier.png" alt="" width="769" height="522" loading="lazy" class="object-none pixelated rounded">
  </picture>
  <figcaption class="text-sm"><span aria-hidden="true">↑</span> Facade de l’atelier que je loue à Strasbourg, à quelques minutes de chez moi.</figcaption>
</figure>

Je me rends aussi assez souvent à Paris en train, soit pour le boulot soit pour mes cours auprès des étudiants en première année de master web design à l’ECV Digital, où j’enseigne le design d’interface et d’interaction et les bases de la typographie, de l’HTML et du CSS.

## Tu es né en Alsace&#8239;?

Non pas du tout, je suis né le 8 décembre 1991 à Bourgoin-Jallieu, en Isère (les amateur·rice·s de rugby connaîtront). Un jour, en préparant mes cours, je me suis rendu compte que j’étais né la même année où [Tim Berners-Lee présenta publiquement son projet WWW](https://www.w3.org/People/Berners-Lee/1991/08/art-6484.txt)&#8239;! J’aime bien me dire que je suis presque aussi vieux que le web. 

1991, c’est aussi l’année de sortie au Japon des jeux vidéo *Legend of Zelda: A Link to the Past* et de *Street Fighter II: The World Warrior,* qui sont deux de mes jeux préférés avec *Tetris*, *Pokémon Version Jaune*, la série des *Age of Empires* et des *Tony Hawk’s Pro Skater*, *Minecraft*, *Cyberpunk 2077* et *Zelda: Breath of the Wild* évidemment.

## Où as-tu passé ton enfance&#8239;?

Lorsque j’avais 3 mois, mes parents se sont installés dans un chalet sur les hauteurs de Die, dans la Drôme (les amateur·rice·s de [clairette](https://fr.wikipedia.org/wiki/Clairette_de_Die) connaîtront). Mon père était alors journaliste pour le journal local, puis il s’est retrouvé à travailler comme maquettiste dans l’imprimerie où ce journal était tiré.

<figure>
	<picture class="filter-text-current">
		<img src="/media/chalet-die.png" alt="" width="769" height="522" loading="lazy" class="object-none pixelated rounded">
	</picture>
	<figcaption class="text-sm"><span aria-hidden="true">↑</span> Notre chalet à Die et sa vue imprenable sur le massif karstique du Vercors.</figcaption>
</figure>

Après mes années de maternelle, il a trouvé du boulot comme graphiste dans une autre imprimerie à Brioude, en Haute-Loire, et c’est comme ça qu’on s’est retrouvés à déménager à Issoire, dans le Puy-de-Dôme. J’y ai fait ma rentrée en école primaire, et en 1999, mes parents ont acheté une ancienne ferme à Usson, un tout petit village de 300 habitants, perché sur un volcan éteint, célèbre pour ses orgues basaltiques.

C’est ici que j’ai passé mes plus belles années étant enfant, mes frères et moi en gardons vraiment de très beaux souvenirs&#8239;: les ruines du château de la Reine Margot (pas ma copine, celle d’Henri IV) et les bois alentours nous offraient un terrain de jeu infini. Le panorama depuis le sommet de la colline sur la chaîne des puys était à couper le souffle.

## Tu as combien de frères&#8239;?

Deux, un grand et un petit, avec 4 ans d’écart entre chacun. Mon grand frère habite à Paris et travaille comme designer industriel chez Patrick Jouin, et le plus jeune est chef cuisinier au Garde Fou ici à Strasbourg.

La parcours de mon frère aîné a été déterminant au regard du mien, dans le sens où sans lui, je n’aurais peut-être jamais su que les arts appliqués existaient. À la base, il voulait être prof de sport. Notre père l’a emmené un jour à la biennale du design de Saint-Étienne, et là, ça a été le déclic pour lui, il s’est dit qu’il voulait faire du design. Personne dans ma famille ne savait trop ce que c’était ni quelles études il fallait faire pour devenir designer, mais il a fini par trouver et a commencé son cursus en internat au lycée Saint Géraud à Aurillac, dans le Cantal. Il rentrait tous les week-end pendant lesquels je découvrais ce qu’il y faisait et je trouvais ça cool. N’ayant pas spécialement envie de faire un bac général et fort de son expérience, je me suis naturellement dirigé moi aussi vers un bac STI arts appliqués, mais à Cournon d’Auvergne.

Mon petit frère a lui aussi commencé des études en arts appliqués mais ça ne lui convenait pas. Il s’est rapidement reconverti dans la cuisine où il s’épanouit à fond maintenant.

## Quelles études as-tu fait après ton bac arts appliqués&#8239;?

Avec mon meilleur ami, on a été acceptés à l’École des arts décoratifs de Strasbourg — qui a été renommée la <abbr title="Haute École des Arts du Rhin">HEAR</abbr> depuis. On s’est mis en colloc’ et on y est resté trois ans. C’est à cette époque que j’ai rencontré Margot avec qui je vis toujours depuis. Une fois notre <abbr title="diplôme national d’arts plastiques">DNAP</abbr> en poche, lui est parti poursuivre ses études en media design à la <abbr title="Haute École d’Art et de Design">HEAD</abbr> de Genève, elle, est restée en communication graphique à Strasbourg, tandis que moi, je suis rentrée en 4<sup>e</sup> année à l’ésad d’Amiens, en design graphique.

J’y ai rencontré des gens très bien, l’enseignement y était vraiment intéressant, et l’année du diplôme, je suis parti habiter à Paris avec mon petit frère. Je faisais les allers-retours Paris–Amiens une fois par semaine pour mes rendez-vous de suivi, si bien que j’ai fini par faire mon projet de diplôme sur la Gare du Nord&#8239;!

Parmi ces rencontres, il y a eu notamment celles avec mes professeurs de design numérique, Laurent Herbet et Olivier Cornet, qui m’ont enseigné les bases de l’UI et de l’UX design et m’ont pris en stage à la fin de ma 4<sup>e</sup> année au sein de leur studio [Figs](https://www.figs-lab.com). Le boulot me plaisait bien, alors une fois diplômé, j’ai finalement bossé pour eux pendant cinq ans. C’est là que j’ai appris le plus sur le plan professionnel&#8239;: j’ai pas mal travaillé sur des interfaces pour des jeux vidéo (dont notamment [Endless Space II](https://www.endless-space.com/)), mais aussi pour des app mobiles, des bornes d’information voyageurs, des plateformes pour des chercheurs… c’était plutôt varié.

## Pourquoi t’être lancé dans le design numérique&#8239;? Tu ne voulais pas faire du dessin de caractères à un moment&#8239;?!

Initialement, je voulais faire du cinéma d’animation quand j’étais au lycée. Mais quand je me suis rendu compte du niveau en dessin qu’il fallait aux portes ouvertes des Gobelins, j’ai tout de suite compris que ce n’était pas pour moi.

Sinon oui, c’est vrai, à la fin de la 5<sup>e</sup> année à Amiens, j’ai vraiment hésité à poursuivre sur le [post-diplôme en type design](http://postdiplome.esad-amiens.fr/). D’autant plus que deux potes eux se motivaient pour le faire. C’est peut-être idiot, mais à l’époque j’ai eu peur de trop m’enfermer dans une discipline de niche je crois. J’ai énormément d’admiration pour les type designers, et je continue aujourd’hui à m’intéresser de près à ce qui se passe dans ce domaine. Je crois que ma curiosité pour d’autres sujets était trop forte pour en faire ma seule spécialité à ce moment là. Cela dit, j’aurais pu faire le post-diplôme et ne pas être *que* dessinateur de caractères tu me diras… comme beaucoup de gens qui font du graphisme, de l’édition, de la direction artistique ou du web design par la suite. 

En fait, en 4<sup>e</sup> année, j’ai aussi commencé à apprendre à coder en me formant par moi-même en plus des quelques cours de Processing qu’on avait, et j’aimais énormément ça. J’ai eu quelques premières commandes pour des petits sites web vitrine à ce moment là, et le fait de pouvoir penser de façon globale une identité visuelle, de la décliner sur plusieurs supports (papier et numérique) et de coder soi-même le site après, j’ai commencé vraiment à y prendre du plaisir et à visualiser les ponts entre ses deux mondes que sont le <span lang="en">print</span> et le web. La typo était toujours là de toute façon, et à défaut d’en dessiner, je composais avec, ce que j’ai toujours adoré faire.

Mais j’ai récemment décidé que j’allais me (re)mettre à la typo, juste pour le plaisir. On a convenu avec [Anton Moglia](http://maous.fr/) qu’il allait me donner quelques cours de dessin de caractères en échange de cours de code&#8239;! Comme quoi, il n’est jamais trop tard :)

## Tu as participé à un projet de recherche aussi il me semble lorsque tu étais à Amiens, tu peux nous en dire deux mots&#8239;?

J’ai effectivement participé pendant trois ans entre 2013 et 2016, à raison d’une matinée par semaine, à un projet de recherche au sein du De-sign-e Lab porté par l’ésad d’Amiens. Les membres de ce projet travaillent entre autres à l’élaboration d’un système d’écriture pour les langues des signes appelé Typannot. 

Ma contribution au sein de ce projet s’est surtout concentrée sur l’élaboration du système (typo)graphique de glyphes pour représenter les différentes positions des mains (ce qui n’est qu’une composante du système complet qui comprend l’expression du visage et le mouvement) ainsi que sur quelques interfaces graphiques d’outils de captation et d’annotation que nous développions.

Ça a été ma seule et unique expérience (pour le moment) de recherche et c’était super enrichissant&#8239;: le fait de travailler en équipe avec des linguistes, des chercheur·se·s et d’autres designers sur un sujet totalement nouveau pour moi, ça m’a vraiment passionné. 

L’équipe a publié un article intitulé « [Systèmes graphématiques et écritures des langues signées](https://journals.openedition.org/signata/1684) » dans la revue Signata fin 2018 pour celles et ceux que ça intéresserait.

## Tu as pensé à rependre la recherche un jour, a faire un doctorat&#8239;?

Oui j’y pense parfois, mais pas pour le moment. Disons que je n’ai pas fermé la porte, mais ayant côtoyé (et côtoyant toujours) plusieurs personnes qui sont passées par là ou qui sont en plein dedans, je sais à quel point cela peut être difficile, notamment la recherche de financements mais aussi d’un point de vue personnel, donc je me dis « on verra, une chose à la fois… ». Pour l’instant je me concentre sur mon activité et sur l’enseignement.

## Et à propos d’enseignement, comment t’es-tu retrouvé à donner des cours&#8239;?

Je me suis retrouvé à l’ECV Digital en 2016 en remplacement de Laurent (de chez Figs) sur un cours d’UX design&#8239;: certain·e·s étudiant·e·s avaient le même âge que moi, voire un peu plus, ce qui était un peu déstabilisant au début. La première fois que tu te retrouves devant un classe de 30 tout seul, c’est assez impressionnant. Mais l’expérience m’a plu et les retours des étudiant·e·s étaient plutôt bons, ce qui m’a rassuré quant à ma légitimité. Donc j’ai continué, jusqu’à aujourd’hui. 

Je ne sais pas si ça a un lien, mais je suis issu d’une famille d’enseignants&#8239;: mon grand-père paternel était professeur de physique chimie et proviseur, ma grand-mère maternelle professeure d’arts ménagers dans un lycée agricole pour filles, ma mère a été instit’ et mon père est encore formateur au GRETA. 

Ce qui m’intéresse dans l’exercice de transmission qu’est l’enseignement, au delà du contact avec les étudiant·e·s, c’est que ça te force à prendre du recul sur ta propre pratique, à mettre des mots dessus, à la replacer dans un contexte, dans une évolution historique et à questionner sa finalité. Et il faut dire que j’ai la chance d’intervenir en master, donc *a priori* face à des personnes qui savent pourquoi elles sont là. Mais ça m’intéresserait aussi d’intervenir auprès d’étudiant·e·s plus jeunes je crois.

## Les allers-retours entre Paris et Strasbourg, ce n’est pas trop galère&#8239;?

Non car je n’interviens pas toutes les semaines. Cette année, je n’ai que 78h réparties sur plusieurs jeudis entre janvier et avril sur trois cours différents. Les élèves à l’ECV Digital sont en alternance, donc ils n’ont cours qu’un jour par semaine, le reste du temps, ils sont en entreprise. Et puis Strasbourg–Paris, c’est moins de 2h en TGV&#8239;! Ça faisait d’ailleurs partie des arguments pour retourner y habiter.

## Quelles ont été les autres raisons de ce choix de quitter Paris&#8239;?

En fait, j’ai d’abord décidé de quitter Figs en juin 2019. Après 5 ans, j’ai voulu changer un peu de cadre et voir si je pouvais mener mon activité de mon côté, avec mon propre réseau. J’avais aussi envie de me rapprocher davantage de l’écosystème des coopératives et de rencontrer d’autres gens avec qui collaborer sur des projets plus en lien avec certaines de mes préoccupations sociales, écologiques et politiques.

Je me suis alors pris un petit bureau partagé à Bagnolet, et c’est d’ailleurs là-bas que j’ai rencontré les gens du [Collectif Bam](https://collectifbam.fr/), pour qui je travaille en ce moment à mi-temps. On était voisins de bureau et ils étaient très cool, donc le courant est tout de suite passé.

Pendant cette période, Margot, qui venait aussi de quitter son boulot, après trois ans chez agnès b. en tant que graphiste / scénographe, a exploré de nouvelles perspectives professionnelles, notamment autour du textile de seconde main et de ses enjeux écologiques et sociaux.

À l’été 2019, là on s’est dit que c’était le moment&#8239;: on sortait du premier confinement et on avait envie de retrouver une meilleure qualité de vie dans une ville à taille humaine. Margot voulait aussi se rapprocher de sa famille qui habite à Mulhouse, et Strasbourg est une ville qu’on connaissait déjà de nos années étudiantes. Donc on savait où on mettait les pieds, la ville nous était familière. Encore aujourd’hui, on se dit souvent qu’on a vraiment bien fait de revenir vivre ici.

## Et en ce moment, à quoi ressemblent tes semaines&#8239;?

Depuis septembre, comme je l’évoquais plus haut, je travaille trois jours par semaine pour le Collectif Bam à Bagnolet&#8239;: une semaine sur deux là-bas, l’autre à distance à Strasbourg. On est parti sur un CDD de six mois pour tester ce format un peu hybride, mais après trois mois, je me rends compte que cela ne me convient pas. Non pas que les projets soient inintéressants ou que je ne m’entende pas avec l’équipe, bien au contraire, mais c’est d’un point de vue logistique et la fatigue que cela entraine que ça ne fonctionne pas. Le fait de n’avoir plus que deux jours par semaine pour travailler sur mes autres projets, c’est trop peu. À vouloir tout faire, j’ai l’impression de ne rien faire vraiment correctement. De plus, j’ai enfin un super atelier à Strasbourg depuis deux mois mais au final je n’en profite que les 3/4 du temps. 

Mais je ne regrette pas du tout&#8239;! C’est une belle expérience et ce sont vraiment des gens super, très à l’écoute et qui prennent le temps de réfléchir à ce qu’ils font et aux conditions dans lesquelles ils le font. Ce qui est assez rare et remarquable je trouve.

## Tu as un statut professionnel un peu particulier, peux-tu nous en parler&#8239;?

Je suis effectivement entrepreneur-salarié associé au sein d’Oxamyne, une petite structure coopérative basée à Lyon, co-portée par l’association [la MYNE](https://lamyne.org/) et la coopérative [Oxalis](https://www.oxalis-scop.fr/).

Mais je prépare un billet spécialement sur le sujet, encore un peu de patience&#8239;!

## Ok, c’est vrai que ce billet commence à être un peu long, donc gardons-nous quelques sujets pour 2022. Pour finir, peux-tu nous partager un peu tes motivations et tes envies concernant l’ouverture de ce blog&#8239;?

J’y parlerai de design et de code bien sûr, mais l’idée derrière ce blog, c’est aussi de partager mon expérience et les idées, les sujets, les questionnements qui m’animent. J’ai également pensé à différents formats d’articles&#8239;: 

- des **tutoriels** <span class="opacity-60">(HTML, CSS, SVG, accessibilité, Hugo, Kirby, Paged.js…)</span>
- des **retours d’expériences** <span class="opacity-60">(outils, applications, formations, randonnées…)</span>
- des **études de cas** sur certains projets <span class="opacity-60">(sur lesquels j’ai travaillé, mais pas seulement)</span>
- des **notes** et des **réflexions plus personnelles** <span class="opacity-60">(lectures, musique, vie professionnelle…)</span>
- des **entretiens écrits et/ou audio** avec des gens qui me semblent pertinents en lien avec les sujets que j’aborderai
- et pourquoi pas quelques **traductions** d’articles anglophones aussi

Je vais tâcher de viser une publication au moins tous les deux mois. En tout cas, ça fait longtemps que j’y pense, plusieurs années pour être honnête, et c’est quelque chose qui me tient vraiment à cœur, donc je suis très content que ça prenne enfin forme et j’espère que ça vous intéressera.

## On attend la suite maintenant, je compte sur toi&#8239;!

Merci, moi aussi.