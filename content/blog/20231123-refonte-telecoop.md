---
title: Refonte de TeleCoop.fr
slug: refonte-telecoop
date: 2023-11-23
publishdate: 2023-11-23
author: Timothée Goguely, Gauthier Roussilhe
description: Retour d’expérience co-écrit avec Gauthier Roussilhe sur le travail d’écoconception mené durant la refonte du site web de TeleCoop, le premier opérateur télécom coopératif en France.
tags:
  - retour d’expérience
  - écoconception
  - coop
image: https://telecoop.fr/media/pages/home/23517b9a1b-1696934431/telecoop.png
comments: https://mastodon.design/@timotheegoguely/112100037548608627
stats:
  - name: Poids de la page
    dots: ...
    value: 65Ko
  - name: Nombre de requêtes
    dots: .
    value: 9
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 118
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

Cet article a été co-écrit avec [Gauthier Roussilhe](https://gauthierroussilhe.com/) et est également disponible sur le [blog](https://telecoop.fr/blog/ecoconception-de-notre-site-web) de TeleCoop.

Si vous souhaitez consulter la **déclaration d’écoconception** que nous avons rédigé pour TeleCoop, elle est disponible à cette adresse&#8239;: [telecoop.fr/ecoconception](https://telecoop.fr/ecoconception). Elle rentre un peu plus dans le détails sur certains points, notamment du point de vue du code.

---

## Introduction

L’**écoconception numérique** continue à se développer en France, notamment portée par le nouveau [réglement général d’écoconception de services numériques (RGESN)](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/) développé au sein de l’État. **Cette pratique consiste à se demander quoi faire pour réduire la charge environnementale (émissions de gaz à effet de serre, consommation d’énergie, d’eau, etc.) d’un site web et de son usage.** Nous avons maintenant une certaine expérience du sujet puisque nous explorons à la fois en théorie et en pratique depuis 5 ans toutes ces questions. 

De plus, nous avons vu de nombreuses déclarations d’écoconception apparaître avec de nombreuses variations (référentiels, indicateurs et mesures, méthodologie, etc.) :
* [ille-et-vilaine.fr/declaration-ecoconception](https://www.ille-et-vilaine.fr/declaration-ecoconception)
* [exposition-celtique.bzh/ecoconception](https://www.exposition-celtique.bzh/ecoconception/)
* [grenoblealpesmetropole.fr/302-eco-conception.htm](https://www.grenoblealpesmetropole.fr/302-eco-conception.htm)
* [lafibre.info/forum/ecoconception/](https://lafibre.info/forum/ecoconception/)
* [lesentreprises-sengagent.gouv.fr/eco-conception](https://lesentreprises-sengagent.gouv.fr/eco-conception)
* [ademe.fr/une-logique-d-ecoconception/](https://www.ademe.fr/une-logique-d-ecoconception/)

L’idée était de trouver un format de déclaration d’écoconception numérique qui réponde de façon détaillée au RGESN tout en restant synthétique et pertinent.

La refonte de Telecoop s’inscrit d’ailleurs dans la continuité de l’écoconception du site de [Commown](commown.coop/) développée en collaboration avec [Derek Salmon](https://www.pikselkraft.com/). Dans cette étude de cas, nous avons appliqué le [Référentiel général d’écoconception de services numériques (RGESN)](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/) constitué de **8 grands axes** :

1. [Stratégie](#Stratégie)
2. [Spécifications](#Spécifications)
3. [Architecture](#Architecture)
4. [UX/UI](#UXUI-Contenus)
5. [Contenus](#UXUI-Contenus)
6. [Front-end](#Front-Back-end)
7. [Back-end](#Front-Back-end)
8. [Hébergement](#Hébergement)

<h2 id="Stratégie">Stratégie</h2>

Commençons donc par la stratégie. Contrairement à d’autres projets portés précédemment, **le contenu et l’arborescence n’ont pas subi de modifications majeures**. Certaines pages ont été enlevées ou déplacées mais la structure est restée à peu près la même. Cela est d’autant plus normal que l’activité et l’offre de Telecoop n’ont pas été modifiées entre temps. La majorité du travail d’écoconception s’est donc portée sur le **design et le développement du nouveau site web**.

Nous avons commencé notre démarche en nous fixant certains objectifs sur nos indicateurs techniques habituels&#8239;: **poids total de la page**, **nombre de requêtes**, **indicateurs de performances** (<abbr title="First Contentful Paint">FCP</abbr>, <abbr title="Largest Contentful Paint">LCP</abbr>, <span lang="en">Speed Index</span>), **temps de chargement en 3G** (1,6Mbps). À cela se rajoute aussi des objectifs de **rétrocomptabilité** basés sur le RGESN, visant à produire un site web qui fonctionne de manière optimale pour des terminaux âgés de 5 ans ou plus.

Nos objectifs pour ce site étaient de limiter le **poids total moyen des pages à 750 Ko** (500 Ko idéalement), le **nombre maximum de requêtes à 30**, un **temps de chargement FCP inférieur à 1,5 seconde** et un **temps de chargement en 3G de bonne qualité inférieur à 3 secondes**.

Nous avons décidé de **ne pas prendre en compte les outils grand public d’estimation du poids environnemental de pages web** (Ecoindex ou Website Carbon par exemple) car nous ne trouvons pas leur méthodologie satisfaisante. Les facteurs d’impacts environnementaux d’Ecoindex sont issus d’une ACV qui n’est pas ouverte, et le modèle de Website Carbon est basé sur une vision trop macroscopique pour être fournir des estimations pertinentes à notre sens. Nous préférons directement travailler avec les indicateurs techniques eux-mêmes utilisés et agrégés par ces outils.

<figure class="w-full">
  <picture>
    <img src="/media/ecoindex-website-carbon.jpg" alt="" width="1280" height="720" loading="lazy">
  </picture>
  <figcaption class="text-sm"><span aria-hidden="true">↑</span> Schéma expliquant les méthodologies de calculs des outils Ecoindex et Website Carbon · Crédit: Gauthier Roussilhe</figcaption>
</figure>

Notre stratégie se résume finalement à répondre au cœur du RGESN&#8239;: contribuer à l’allongement de la durée de vie des équipements et fournir une expérience optimale même dans les conditions d’usage les plus difficiles. Nous estimons qu'en visant ces objectifs nous permettons de réduire l’empreinte environnementale attenante à l’usage du site web sans que cela ait besoin d’une estimation chiffrée en eq-CO2 ou autres facteurs.

Au-delà du respect du RGESN, il s’agissait aussi pour nous de renforcer le cercle vertueux des bonnes pratiques du web&#8239;: attention très forte à l’accessbilité, respect du RGPD (Matomo), une meilleure expérience pour les utilisateurs et l’augmentation générale de la qualité du site.

<h2 id="Spécifications">Spécifications</h2>

Cette partie du RGESN est moins pertinente pour un projet «&#8239;simple&#8239;» comme celui de TeleCoop, mais nous avons dû faire avec des **services tiers** particulièrement récalcitrant à une démarche d’écoconception&#8239;: le service de gestion des cookies (Axeptio), Avis Vérifiés et Matomo (hébergé par Ethibox) représentent à eux seuls plus de la moitié du poids de la page d’accueil (uniquement en JS). Il n’était malheureusement pas possible d’enlever ses services tiers ou même d’influer concrètement sur les widgets et les options d’intégration qu'ils proposent.

Seule l’intégration du **widget RocketChat** a pu faire l’objet d’une d’intégration sur mesure consistant à ne charger et n’afficher le widget (et tous les assets qui vont avec) qu'au clic sur un bouton composé d’une simple icône SVG. 
_Mise à jour : le widget Rocket a finalement été désactivé._

<h2 id="Architecture">Architecture</h2>

Le site de TeleCoop est géré via une seule solution, le **CMS [Kirby](https://getkirby.com/)** basé sur PHP. À ce titre nous ne faisons pas appel à des frameworks externes. Les différents protocoles techniques ont bien été intégré en fonction de leur évolution et de leur pertinence par rapport au contenu présent sur le site. 

Nous avons fait en sorte que le blog soit rapatrié sous le même nom de domaine (avant&#8239;: https://www.blog.telecoop.fr/ / après&#8239;: https://telecoop.fr/blog). Le blog est ainsi géré par la même solution (Kirby) au lieu d’avoir deux solutions différentes.

<h2 id="UXUI-Contenus">UX/UI &amp; Contenus</h2>

Au vu de nos indicateurs et de nos objectifs, le travail d’UX et d’UI mené a pris en compte les critères du RGESN là où ils sont applicables&#8239;: le site est fait pour **fonctionner à bas débit**, les **parcours ont été optimisés**, les **contenus lourds (images, vidéo) suivent une logique de sélection et d’optimisation assez poussée** et décrite dans la [déclaration d’écoconception](https://telecoop.fr/ecoconception). 

Concrètement, toutes les images sont converties en WebP, avec des fallbacks prévus en JPG ou PNG pour les équipements plus anciens, et un chargement dit «&#8239;paresseux&#8239;» (<em lang="en">lazy loading</em>). La seule vidéo présente dans le site a été compressée à 480p et sans option de pré-chargement avant d’éviter tout chargement exédentaire de données.

<h2 id="Front-Back-end">Front &amp; Back-end</h2>

Comme dit précédemment, [Kirby](https://getkirby.com/) est utlisé pour le site de TeleCoop. Ce CMS est basé sur PHP et ne gère pas de base de données. Au-delà des caractéristiques techniques, Kirby propose selon nous d’une des meilleures expériences de gestion de contenu pour ses utilisateurs.

Le site a été pensé en <em lang="en">mobile-first</em> et est fait pour s’adapter à toutes les tailles d’écran standards. Nous avons mis en place de nombreux mécanismes pour s’assurer d’un **chargement optimisé** des ressources et des contenus transférés comme expliqué dans la [déclaration d’écoconception](https://telecoop.fr/ecoconception). De même, une **mise en cache** est activée pour éviter tout transfert superflu.

<h2 id="Hébergement">Hébergement</h2>

Le site est hébergé chez **Scaleway**. Il existe quelques hébergeurs français qui répondent aux critères du RGESN. Nous apprécions particulièrement et valorisons [la transparence de Scaleway](https://www.scaleway.com/fr/leadership-environnemental/) sur la consommation d’énergie et d’eau de ses installations, c'est pour cela que nous les avons choisi.

## Résultats

Voici à titre d’exemple l’**évolution des indicateurs avant et après refonte**, sur une sélection de quelques pages. Les données de la colonnes «&#8239;après&#8239;» sont une moyenne des résultats sur mobile et desktop.

<table class="table-mono">
  <caption>Page d’Accueil</caption>
  <thead>
    <tr>
      <td></td>
      <th scope="col">Avant</th>
      <th scope="col">Après</th>
      <th scope="col">Évolution</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">FCP</th>
      <td>1.57s</td>
      <td>0.75s</td>
      <td>÷2.09 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">LCP</th>
      <td>3.22s</td>
      <td>1.7s</td>
      <td>÷1.89 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">Speed Index</th>
      <td>2.79s</td>
      <td>0.9s</td>
      <td>÷3.1 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">Performances</th>
      <td>62/100</td>
      <td>97/100</td>
      <td>+35 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Accessibilité</th>
      <td>81/100</td>
      <td>96/100</td>
      <td>+15 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Bonnes pratiques</th>
      <td>100/100</td>
      <td>100/100</td>
      <td>–</td>
    </tr>
    <tr>
      <th scope="row">SEO</th>
      <td>89/100</td>
      <td>100/100</td>
      <td>+11 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Poids total</th>
      <td>881 Ko</td>
      <td>721 Ko</td>
      <td>÷1.22 ↘︎</td>
    </tr>
  </tbody>
</table>

<table class="table-mono">
  <caption>À propos</caption>
  <thead>
    <tr>
      <td></td>
      <th scope="col">Avant</th>
      <th scope="col">Après</th>
      <th scope="col">Évolution</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">FCP</th>
      <td>1.73s</td>
      <td>0.75s</td>
      <td>÷2.3 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">LCP</th>
      <td>2.11s</td>
      <td>1.7s</td>
      <td>÷1.24 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">Speed Index</th>
      <td>3.01s</td>
      <td>0.9s</td>
      <td>÷3.34 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">Performances</th>
      <td>62/100</td>
      <td>97/100</td>
      <td>+35 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Accessibilité</th>
      <td>72/100</td>
      <td>96/100</td>
      <td>+24 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Bonnes pratiques</th>
      <td>100/100</td>
      <td>100/100</td>
      <td>–</td>
    </tr>
    <tr>
      <th scope="row">SEO</th>
      <td>89/100</td>
      <td>100/100</td>
      <td>÷11 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Poids total</th>
      <td>873 Ko</td>
      <td>650 Ko</td>
      <td>÷1.34 ↘︎</td>
    </tr>
  </tbody>
</table>

<table class="table-mono">
  <caption>Blog</caption>
  <thead>
    <tr>
      <td></td>
      <th scope="col">Avant</th>
      <th scope="col">Après</th>
      <th scope="col">Évolution</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">FCP</th>
      <td>1.73s</td>
      <td>0.75s</td>
      <td>÷2.3 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">LCP</th>
      <td>2.11s</td>
      <td>1.45s</td>
      <td>÷3 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">Speed Index</th>
      <td>3.01s</td>
      <td>0.75s</td>
      <td>÷4 ↘</td>
    </tr>
    <tr>
      <th scope="row">Performances</th>
      <td>89/100</td>
      <td>99/100</td>
      <td>+10 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Accessibilité</th>
      <td>96/100</td>
      <td>100/100</td>
      <td>+4 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Bonnes pratiques</th>
      <td>100/100</td>
      <td>100/100</td>
      <td>–</td>
    </tr>
    <tr>
      <th scope="row">SEO</th>
      <td>93/100</td>
      <td>100/100</td>
      <td>+7 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Poids total</th>
      <td>1310 Ko</td>
      <td>905 Ko</td>
      <td>÷1.44 ↘︎</td>
    </tr>
  </tbody>
</table>

<table class="table-mono">
  <caption>Article</caption>
  <thead>
  <tr>
    <td></td>
    <th scope="row">Avant</th>
    <th scope="row">Après</th>
    <th scope="row">Évolution</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <th scope="row">FCP</th>
    <td>1.7s</td>
    <td>0.7s</td>
    <td>÷2.4 ↘︎</td>
  </tr>
  <tr>
    <th scope="row">LCP</th>
    <td>1.85s</td>
    <td>1.3s</td>
    <td>÷1.4 ↘︎</td>
  </tr>
  <tr>
    <th scope="row">Speed Index</th>
    <td>1.7s</td>
    <td>0.7s</td>
    <td>÷2.4 ↘︎</td>
  </tr>
  <tr>
    <th scope="row">Performances</th>
    <td>99/100</td>
    <td>100/100</td>
    <td>+1 ↗︎</td>
  </tr>
  <tr>
    <th scope="row">Accessibilité</th>
    <td>96/100</td>
    <td>100/100</td>
    <td>+4 ↗︎</td>
  </tr>
  <tr>
    <th scope="row">Bonnes pratiques</th>
    <td>100/100</td>
    <td>100/100</td>
    <td>–</td>
  </tr>
  <tr>
    <th scope="row">SEO</th>
    <td>100/100</td>
    <td>100/100</td>
    <td>–</td>
  </tr>
  <tr>
    <th scope="row">Poids total</th>
    <td>229 Ko</td>
    <td>569 Ko</td>
    <td>×2.5↗︎</td>
  </tr>
  </tbody>
</table>

<table class="table-mono">
  <caption>Moyenne</caption>
  <thead>
    <tr>
      <td></td>
      <th scope="col">Avant</th>
      <th scope="col">Après</th>
      <th scope="col">Évolution</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">FCP</th>
      <td>1.68s</td>
      <td>0.74s</td>
      <td>÷2.3 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">LCP</th>
      <td>2.32s</td>
      <td>1.54s</td>
      <td>÷1.5 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">Speed Index</th>
      <td>2.63s</td>
      <td>0.81s</td>
      <td>÷3.2 ↘︎</td>
    </tr>
    <tr>
      <th scope="row">Performances</th>
      <td>78/100</td>
      <td>98/100</td>
      <td>+20 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Accessibilité</th>
      <td>86/100</td>
      <td>98/100</td>
      <td>+12 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">Bonnes pratiques</th>
      <td>93/100</td>
      <td>100/100</td>
      <td>+7 ↗︎</td>
    </tr>
    <tr>
      <th scope="row">SEO</th>
      <td>100/100</td>
      <td>100/100</td>
      <td>–</td>
    </tr>
    <tr>
      <th scope="row">Poids total</th>
      <td>823 Ko</td>
      <td>711 Ko</td>
      <td>÷1.2 ↘︎</td>
    </tr>
  </tbody>
</table>

En se basant sur la **moyenne des résultats obtenus pour chaque indicateur** à partir de ce panel de 4 pages, on peut constater que **nos différents objectifs ont tous été atteints**.

---

## Parrainage TeleCoop

Si vous n’êtes pas encore client, que la mission de TeleCoop vous parle et que vous souhaitez changer d’opérateur mobile, voilà mon code de parrainage, à renseigner au moment de votre inscription pour obtenir une réduction de 5€ sur votre première facture (et moi aussi par la même occasion)&#8239;: <code>[ZUZZ3RSX6](https://souscription.telecoop.fr/?cr=ZUZZ3RSX6)</code>