---
title: Blog
slug: blog
date: 2021-08-12
description: Blog de Timothée Goguely.
stats:
  - name: Poids de la page
    dots: ...
    value: 74Ko
  - name: Nombre de requêtes
    dots: .
    value: 10
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 75
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---
