---
title: PPPermapublishing
slug: pppermapublishing
date: 2024-10-29
lastmod: 2024-10-30
description: Retour sur l’événement PrePostPrint à la HEAR de Strasbourg les 28 et 29 septembre 2024.
tags:
  - retour d’expérience
  - PrePostPrint
  - permacomputing
image: /media/PPPermapublishing-A4-jgs.png
comments: https://mastodon.design/@timotheegoguely/113393410522756725
stats:
  - name: Poids de la page
    dots: ...
    value: 67Ko
  - name: Nombre de requêtes
    dots: .
    value: 7
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 266
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

Il y a tout juste un mois s’est tenu une rencontre [PrePostPrint](https://prepostprint.org/) intitulée <strong lang="en">PPPermapublishing</strong> à Strasbourg. Avec la précieuse aide de Victor Hoffner (avec qui nous avons co-organisé l’événement) et de Jérôme Saint-Loubert Bié (qui nous a ouvert les portes de la <abbr title="Haute École des Arts du Rhin">HEAR</abbr>), nous avons réuni sur deux jours une cinquantaine de personnes venues de France, de Belgique et des Pays-Bas, autour de l’invitation suivante :

<blockquote lang="en">
  <p>“The PrePostPrint community of practices shares a lot of <a href="https://permacomputing.net/Principles/">principles</a> and political values with the <a href="https://permacomputing.net/">Permacomputing</a>’s one. So we’d be happy to bring designers, developers, artists, hackers, researchers, computer scientists, teachers and publishers closer together, to meet and help each other and share ideas, knowledge, and approaches around informal discussions and short projects’ demos and presentations.”</p>
</blockquote>

<figure class="pixelated">
  <picture class="filter-text-current" style="max-width: 595px">
    <img class="pixelated" src="/media/PPPermapublishing-A4-jgs.png" alt="" width="595" height="842" loading="lazy">
  </picture>
  <figcaption class="text-sm" style="max-width: 80ch">Affiche réalisée par mes soins en <a href="/code/PPPermapublishing-A4-jgs.html">HTML et CSS (5 Ko)</a> (pour un rendu fidèle à l’affiche, installez la police de caractères <a href="https://velvetyne.fr/fonts/jgs-font/">jgs</a> sur votre ordinateur).</figcaption>
</figure>

---

Sommaire

<nav>

- [PrePostPrint](#prepostprint)
- [Permacomputing](#permacomputing)
- [Table ronde](#table-ronde)
- [Notes](#notes)

</nav>

---

<h2 id="prepostprint">PrePostPrint</h2>

Si c’est la première fois que vous entendez parler de **PrePostPrint** (ou PPP pour les intimes), voici une traduction en français de la présentation qui en est faite sur [prepostprint.org](https://prepostprint.org/about/) :

<blockquote cite="https://prepostprint.org/about/">
  <p>PrePostPrint souhaite rassembler ceux qui travaillent avec des techniques de publication expérimentales et les aider à rendre leurs projets et leurs outils plus accessibles.</p>
  <p>PrePostPrint est un label et un site web visant à promouvoir des objets et des ressources graphiques répondant aux caractéristiques suivantes :</p>
  <p><strong>Expérimental</strong> <br>L’objet graphique est réalisé dans le cadre d’une approche expérimentale ou inhabituelle avec des outils alternatifs, bricolés ou <abbr lang="en" title="Do It Yourself">DIY</abbr>, dans un contexte contraint par son économie, son urgence ou sa nature collaborative.</p>
  <p><strong>Logiciel libre</strong> <br>Les outils utilisés pour réaliser l’objet et, si possible, le contenu de l’objet lui-même, sont publiés sous une licence (logiciel) libre.</p>
  <p><strong>Documenté</strong> <br>L’objet est documenté selon les critères de PrePostPrint, puis publié sur le site prepostprint.org</p>
</blockquote>

En plus de la maintenance du site web, PrePostPrint organise également depuis 2017 des [événements](https://wiki.prepostprint.org/index.php?title=Main_Page#Events), essentiellement en France, mais aussi aux Pays-Bas à Rotterdam et bientôt à Bruxelles normalement. Personnellement, c’est en venant en curieux à l’un de ces événements organisé à Paris à l’[EnsadLab en 2018](https://www.ensadlab.fr/fr/francais-conference-ensadlab-invite-prepostprint/) que j’ai rencontré des membres de cette communauté et que j’ai commencé à m’intéresser à ces pratiques, de loin dans un premier temps puis de plus en plus activement. 

C’est d’ailleurs à la suite du dernier événement organisé à [Césure en janvier 2024](https://wiki.prepostprint.org/index.php?title=PPPrototypes_workshop_@_C%C3%A9sure), que nous nous sommes proposés avec Victor d’organiser le prochain à Strasbourg, et pendant les [LGM à Rennes en mai](https://wiki.prepostprint.org/index.php?title=PrePostPrint_@_Libre_Graphics_Meeting_(Rennes):_9%E2%80%9312_May_2024) que l’idée du thème est née.

<h2 id="permacomputing" lang="en">Permacomputing</h2>

Vous l’aurez compris, notre envie était de réflechir collectivement aux pratiques, savoirs et idées développées au sein de <span lang="en">PrePostPrint</span> au regard des [10 principes](https://permacomputing.net/Principles/) définis par la communauté <span lang="en">Permacomputing</span>.

Dans le but de les rendre accessibles au plus grand nombres, en voici une traduction personnelle en français (n’hésitez pas à me suggérer des corrections si vous trouvez certaines formulations approximatives).

---

**Principes du permacomputing**

Ces principes de design ont été modelés d’après ceux de la permaculture.

Il s’agit avant tout de principes de design/pratique, et non de principes philosophiques. Sentez-vous libres d’être en désaccord avec eux, de les remanier et les (ré)interpréter librement. Le  <span lang="en">Permacomputing</span> n’est pas prescriptif, mais favorise plutôt la prise de conscience située de la diversité du contexte. En d’autres termes, ses principes de design peuvent être aussi bien utiles pour guider la pratique dans une situation spécifique, que pour aider à mettre en évidence des problèmes systémiques dans la relation entre la technologie informatique et l’écologie.

Par ailleurs, il s’agit d’un gros travail en cours :)

<h3 id="prendre-soin-de-la-vie">Prendre soin de la vie</h3>

Il s’agit de la base éthique sur laquelle repose le <span lang="en">Permacomputing</span>. Elle fait référence aux principes permacoles de « protection de la terre » (<cite lang="en">care for the earth</cite>) et de « protection des personnes » (<cite lang="en">care for the people</cite>), mais peut être considérée comme l’axiome de base pour tous les choix.

Créer des systèmes à faible consommation d’énergie qui renforcent la biosphère et utiliser le réseau étendu avec parcimonie. Réduire au minimum l’utilisation d’énergie artificielle, de combustibles fossiles et de ressources minérales. Ne créez pas de systèmes qui [dissimulent les déchets](https://permacomputing.net/Jevons_paradox/).

<h3 id="prendre-soin-des-puces">Prendre soin des puces</h3>

La production de nouveau matériel informatique consomme beaucoup d’énergie et de ressources. Par conséquent, nous devons **[maximiser la durée de vie](https://permacomputing.net/lifespan_maximization/)** des composants matériels, en particulier des [microprocesseurs](https://permacomputing.net/IC/), en raison de leur faible recyclabilité.

* Respecter les bizarreries et les particularités de ce qui existe déjà et réparer ce qui peut l’être.
* Créez de nouveaux appareils à partir de [composants récupérés](https://permacomputing.net/salvage_computing/).
* Soutenez le [partage du temps](https://permacomputing.net/time-sharing/) au sein de votre communauté locale afin d’éviter d’acheter des objets redondants.
* Pousser l’industrie vers la **[longévité planifiée](https://permacomputing.net/planned_longevity/)**.
* [Designer pour le démontage](https://permacomputing.net/Design_for_disassembly/).

<h3 id="rester-petit">Rester petit</h3>

Les petits systèmes sont plus susceptibles d’avoir de faibles besoins en matériel et en énergie, ainsi qu’une grande compréhensibilité. Ils sont plus faciles à comprendre, à gérer, à remanier et à réutiliser.

* Les [dépendances](https://permacomputing.net/dependency/) (y compris les exigences matérielles et les logiciels/bibliothèques externes dont le programme a besoin) doivent également être réduites.
* Éviter la [pseudo-simplicité](https://permacomputing.net/pseudosimplicity/), comme les interfaces utilisateur qui cachent leur fonctionnement à l’utilisateur.
* **Accumuler la sagesse et l’expérience plutôt que la base de code**.
* **La faible complexité est belle**. Ce principe s’applique également aux médias visuels, par exemple, où l’on pense souvent que la « haute qualité » découle de résolutions élevées et d’importants débits.
* [Échelle humaine](https://permacomputing.net/human-scale/) : un niveau raisonnable de complexité pour un système informatique est celui qui peut être entièrement compris par une seule personne (des détails matériels de bas niveau aux bizarreries au niveau de l’application).
* La [scalabilité](https://permacomputing.net/scalability/) (vers le haut) n’est essentielle que s’il existe un besoin réel et justifiable d’extensibilité ; la réduction d’échelle peut souvent être plus pertinente.
* **Penser en termes d’abondance**. Si la capacité de calcul vous semble trop limitée, vous pouvez la repenser du point de vue de l’abondance (par exemple en vous reportant cinquante ans en arrière) : des dizaines de kilo-octets de mémoire, des milliers d’opérations par seconde - pensez à toutes les possibilités !

<h3 id="esperer-le-meilleur-se-preparer-pour-le-pire">Espérer le meilleur, se préparer au pire</h3>

C’est une bonne pratique que de garder tout aussi résilient et tolérant à l’effondrement que possible, même si vous ne croyez pas à ces scénarios.

* Tout en étant résilient et en construisant sur une base solide, soyez ouvert aux possibilités positives et utopiques. Expérimentez de nouvelles idées et ayez de grandes visions.
* [Concevez pour la descente](https://permacomputing.net/design_for_descent/).

<h3 id="rester-flexible">Rester flexible</h3>

La flexibilité signifie qu’un système peut être utilisé à des fins très diverses, y compris celles pour lesquelles il n’a pas été conçu à l’origine. La flexibilité est le complément de la petite taille et de la simplicité. Dans un système idéal et élégant, les trois facteurs (taille réduite, simplicité et flexibilité) se soutiennent mutuellement.

S’il est possible d’imaginer tous les cas d’utilisation possibles lors de la conception d’un système, la conception peut très bien être trop simple et/ou trop rigide. La petitesse, la simplicité et la flexibilité font également partie de l’idéal de la ligne de commande [Unix](https://permacomputing.net/Unix/) : « des outils petits et tranchants » (<cite lang="en">small, sharp tools</cite>). Ici, la clé de la flexibilité est la capacité à combiner de manière créative de petits outils qui font de petites choses individuelles.

* La technologie informatique en général est très flexible en raison de sa programmabilité. La programmation et la programmabilité doivent être soutenues et encouragées partout, et les blocages artificiels qui empêchent la (re)programmation doivent être supprimés.
* Concevez des systèmes que vous pouvez progressivement modifier et améliorer tout en les faisant fonctionner.

<h3 id="construire-sur-une-base-solide">Construire sur une base solide</h3>

Il est bon d’expérimenter de nouvelles idées, de nouveaux concepts et de nouveaux langages, mais en dépendre est généralement une mauvaise idée. Appréciez les technologies matures, les idées claires et les théories bien comprises lorsque vous construisez quelque chose qui est destiné à durer.

* Évitez les [dépendances](https://permacomputing.net/dependency/) peu fiables, en particulier les dépendances dures (non optionnelles). Si vous ne pouvez pas les éviter (dans le cas d’un logiciel), mettez-les à disposition au même endroit que votre programme.
* Il est possible de prendre en charge plusieurs plates-formes cibles. Dans le cas de programmes durables, l’une d’entre elles devrait être une [plate-forme de base](https://permacomputing.net/bedrock_platform/) qui ne change pas et ne provoque donc pas de [pourrissement du logiciel](https://permacomputing.net/software_rot/).
* **Ne prenez rien pour acquis**. En particulier, ne vous attendez pas à ce que les infrastructures telles que le réseau électrique et le réseau mondial continuent à fonctionner indéfiniment.
* Vous pouvez aussi lire cela comme « faites pousser vos racines sur un sol solide ». Apprenez des choses qui durent, enrichissez votre tradition locale, connaissez l’histoire de chaque chose.

<h3 id="amplifier-la-conscience">Amplifier la conscience</h3>

Les ordinateurs ont été inventés pour aider les gens dans leurs processus cognitifs. L’« amplification de l’intelligence » était un bon objectif, mais l’intelligence peut aussi être utilisée de manière étroite et aveugle. Il serait donc plus judicieux d’amplifier la conscience.

* Par conscience, on entend la conscience de ce qui se passe concrètement dans le monde/l’environnement, mais aussi la conscience de la manière dont les choses fonctionnent et dont elles se situent dans leur contexte (culturel, historique, biologique, etc.).
* Il n’est pas nécessaire de tout manipuler pour comprendre. Le [Yin hacking](https://permacomputing.net/balance_of_opposites/) met l’accent sur l’observation.
* Il peut également s’avérer judicieux d’amplifier la conscience qu’a l’ordinateur de son environnement physique à l’aide de capteurs, par exemple.

<h3 id="exposer-tout">Exposer tout</h3>

Extension de l’expression « amplifier la conscience » : ne cachez pas l’information !

* Gardez tout ouvert, modifiable et flexible.
* Partagez votre [code source](https://permacomputing.net/FLOSS/) et votre philosophie de conception.
* **Visualisation de l’état** : faites en sorte que l’ordinateur visualise/auralise son état interne ainsi que tout ce qu’il sait sur l’état de son environnement physique. Considérez cette visualisation/sonorisation comme un paysage d’arrière-plan : facilitez l’observation mais ne volez pas l’attention. N’utilisez pas non plus trop de ressources informatiques pour cela (mettre à jour un paysage d’arrière-plan en plein écran des dizaines de fois par seconde est totalement exagéré).

<h3 id="reagir-aux-changements">Réagir aux changements</h3>

Les systèmes informatiques doivent s’adapter aux changements de leur environnement d’exploitation (notamment en ce qui concerne l’énergie et la chaleur). Il n’est pas nécessaire que toutes les parties du système soient disponibles 24 heures sur 24 et 7 jours sur 7, ni que les performances de fonctionnement soient constantes (par exemple, la vitesse du réseau).

* À long terme, les systèmes logiciels et matériels ne doivent pas devenir obsolètes en raison de l’évolution des besoins et des conditions. De nouveaux logiciels peuvent être écrits même pour de vieux ordinateurs, d’anciens logiciels peuvent être modifiés pour répondre à de nouveaux besoins, et de nouveaux appareils peuvent être construits à partir d’anciens composants. Il faut éviter à la fois le [pourrissement des logiciels](https://permacomputing.net/software_rot/) et la [rétro](https://permacomputing.net/retro/)-informatique.

<h3 id="chaque-chose-a-sa-place">Chaque chose à sa place</h3>

Participez à la circulation de l’énergie et de la matière, aux écosystèmes et aux cultures de votre région. Chérissez la localité, [évitez la centralisation](https://permacomputing.net/decentralization/). Renforcez les racines locales de la technologie que vous utilisez et créez.

Tout en opérant localement et actuellement, soyez conscient de l’ensemble du contexte mondial dans lequel s’inscrit votre travail. Cela inclut le contexte historique de plusieurs décennies passées et futures. Comprendre le(s) passé(s) est la clé pour envisager les futurs possibles.

* Rien n’est « universel ». Même les ordinateurs, « calculateurs universelles » qui peuvent être réadaptées à n’importe quelle tâche, sont pleins de bizarreries qui proviennent des cultures qui les ont créées. Ne les considérez pas comme la seule façon dont les choses peuvent être, ou comme la façon la plus « rationnelle » ou la plus « avancée ».
* Chaque système, aussi omniprésent ou « universel » soit-il, n’est qu’un petit grain de sable dans un immense océan de possibilités. Essayez de comprendre l’ensemble de l’espace des possibilités en plus des petites taches individuelles dont vous avez une expérience concrète.
* **[Appréciez la diversité](https://permacomputing.net/technological_diversity/)**, évitez la [monoculture](https://permacomputing.net/monoculture/). Mais n’oubliez pas que les normes ont également une place importante.
* L’utilitarisme strict appauvrit. L’inutilité a également une place importante, alors appréciez-la.
* Vous pouvez également lire ce principe comme suit : **Il y a une place pour chaque chose**. Rien n’est obsolète ou sans intérêt. Même s’ils perdent leur sens premier, les systèmes programmables peuvent être réadaptés à de nouveaux usages pour lesquels ils n’avaient pas été conçus à l’origine. Pensez la technologie comme un rhizome plutôt qu’une « autoroute du progrès et de l’obsolescence constante ».
* Il y a une place pour les processus à la fois lents et rapides, graduels et ponctuels. Ne regardez pas toutes les choses avec les mêmes lunettes.

---

<h2 id="table-ronde">Table ronde</h2>

Le week-end à commencé par une table ronde animée par [Julien Bidoret](https://accentgrave.net/) et [Lucille Haute](https://www.lucilehaute.fr/accueil.html) – que je remercie chaleureusement au passage – avec comme invités [Michael Murtaugh](https://automatist.org), [Marie Verdeil](https://verdeil.net) et [Aymeric Mansoux](https://bleu255.com/~aymeric/), qui nous ont fait l’honneur de leur présence respective, physiquement pour Michael et Marie et en ligne pour Aymeric.

Pour celles et ceux que ça intéressent et qui n’ont pas pu venir, voici le lien vers [les slides](https://prepostprint.org/pppermapublishing/) ainsi que l’enregistrement audio de la discussion (en anglais) :

<figure>
  <audio controls src="/media/PPPermapublishing-table-ronde-240928.mp3" preload="metadata" style="width:100%; max-width:640px"></audio>
  <figcaption class="text-sm">Table ronde, <cite>PPPermapublishing</cite>, 28 octobre 2024 (1'16"). <br><a href="/media/PPPermapublishing-table-ronde-240928-.mp3">Télécharger le fichier MP3 (7.4 Mo)</a></figcaption>
</figure>

---

<h2 id="notes">Notes</h2>

Si vous voulez retrouver les **notes prises sur le pad collectif** (un immense merci à toutes les personnes qui y ont contribué, et tout particulièrement Marie Verdeil) lors les différentes présentations et démos qui ont eu lieu tout au long du week-end, elles ont été archivées sur [la page de l’événement](https://wiki.prepostprint.org/index.php?title=PPPermapublishing_@_HEAR_(Strasbourg):_28%E2%80%9329_September_2024#PPPAD_notes) sur le wiki de PPP.

[Kiara](https://mastodon.design/@kajou/113226913523913250) a également partagé sur [sa page au sein du wiki de XPUB](https://pzwiki.wdka.nl/mediadesign/User:Kiara/Journal#PPPermapublishing) ses notes sur son week-end et quelques photos de l’événement !

---

Personnellement, même si cela nous a demandé pas mal d’énergie et que j’y ai laissé quelques plumes, j’ai vraiment été très heureux de comment s’est déroulé ce week-end, et j’ai appris plein de choses sur la logistique liée à l’organisation d’événement de ce type, car c’était une première pour moi. 

Je retiendrai surtout les échanges avec les participant·es, le plaisir de recroiser quelques visages familiers et de recontrer de nouvelles personnes, dont certaines dont j’admire et suis le travail depuis de nombreuses années.
En tout cas, tout le monde avait globalement l’air heureux d’être là, et je suis fier d’avoir participer à faire se croiser des gens, des pratiques et des idées le temps d’un week-end, ici à Strasbourg. Vivement le prochain !

🥨

À ce propos, si vous voulez vous tenir informé·e des futurs événéments, je vous invite à suivre [PrePostPrint sur Mastodon](https://post.lurk.org/@prepostprint) et à vous inscrire à la [liste de diffusion mail](https://lists.domainepublic.net/cgi-bin/mailman/listinfo/prepostprint).
