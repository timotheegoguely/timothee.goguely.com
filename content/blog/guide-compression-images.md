---
title: Guide de compression d’images
slug: guide-compression-images
date: 2023-11-30
publishdate: 2023-11-30
description: Guide à destination de toute personne souhaitant apprendre à compresser des images numériques à l’aide d’outils simples et gratuits.
tags:
  - guide
  - écoconception
image: https://telecoop.fr/media/pages/home/23517b9a1b-1696934431/telecoop.png
draft: true
stats:
  - name: Poids de la page
    dots: ...
    value: 65Ko
  - name: Nombre de requêtes
    dots: .
    value: 9
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 118
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

Cet article est un **guide à destination de toute personne souhaitant apprendre à compresser des images numériques** à l’aide d’outils simples et gratuits, [en ligne](#en-ligne) ou [hors ligne](#hors-ligne). Le but de ces techniques est de **réduire au maximum le poids des images** que l’on décide de publier en ligne, afin de réduire le volume des données des pages y faisant appel (et donc leur temps de chargement), ainsi que l'espace disque nécessaire à leur stockage.

---

<h2 id="en-ligne">En ligne</h2>

### Compression

- [Squoosh](https://squoosh.app/)
  - multi formats : JPG, PNG, WebP, AVIF
  - outils tout en un
  - 1 image à la fois
  - preview avant / après
  - réglage de la qualité
  - option de dithering
- [TinyJPG](https://tinyjpg.com/) / [TinyPNG](https://tinypng.com/)
  - 20 images **JPG/PNG** à la fois
  - pas d'options de réglages
  - différences quasi-imperceptibles à l’œil nu (sauf cas de dégradés de couleurs par exemple)
- [SVGOMG](https://jakearchibald.github.io/svgomg/)
  - inputs : ficher SVG `Open SVG ` ou code SVG `Paste Markup`
  - nombreuses options de réglages

### Dithering

- [Dither it!](https://ditherit.com/)
  - différents presets de palettes (par exemple Black & White)
  - options
    - Image size : pas de possibilité de définir de taille personalisée pour le moment, juste des presets (320px, 640px, 1080px, 1280px)
    - Algorythm : différents algorythmes de compression (FloydSteinberg par défaut)

 
<h2 id="hors-ligne">Hors-ligne</h2>

### ImageOptim

[ImageOptim](https://imageoptim.com/fr) est un petit logiciel libre disponible sur tous les principaux systèmes d'exploitation permettant de compresser très simplement plusieurs images PNG, JPG, GIF ou SVG en même temps. Des réglages de qualité sont disponibles pour les formats JPG, PNG et GIF.


### GIMP 2.10

Les réglages qui suivent permettent d’obtenir une image tramée en noir et blanc avec pixels visibles (*dithered*), comme celle figurant en page d’accueil de mon site. Pour obtenir d’autres effets, libre à vous de modifier les réglages de **Palettes** et de **Tramage** de l’étape 4.

1. **Télécharger et installer** la dernière version de [GIMP](https://www.gimp.org/downloads/) (compatible GNU/Linux, OS X, Microsoft Windows)

2. **Ouvrir** votre image JPG ou PNG dans Gimp **`Fichier > Ouvrir…`**
Si la fenêtre suivante s’ouvre, laissez Mode de rendu > `Colorimétrie relative` puis cliquer sur **`Convertir`**
![Convertir vers l’espace de travail RVB ?](https://i.imgur.com/MVPwIO3.png)

3. **Réduire la taille de l’image** jusqu'à la dimension souhaitée **`Image > Échelle et taille de l’image…`** 🔗 *Laisser les liens chaînés pour ne pas déformer l’image*
  - **Taille de l’image** > Largeur : `1024px` (par exemple). La hauteur s’adapte automatiquement
  - **Résolution** : `72dpi`. La Résolution en X et en Y doit rester identique
  - **Qualité** > Interpolation : `Cubique` (par défault)
  - **`Mise à l’échelle`**

![Échelle et taille de l’image](https://i.imgur.com/uzrZBAz.png)

  - **Recadrer** si besoin **`Image > Taille du canvevas…`**
    - nécessaire si l'on souhaite par exemple que son image respecte un certain ratio (16:9, 4:3, 3:2, 2:1, etc.). Dans l'image ci-dessous, le ratio est égale à 4:3 (1024:768 = 4:3 = 1.333)
    - peut aussi se faire après l’étape 4

![Régler la taille du canevas](https://i.imgur.com/xSc1Lug.png)


4. **Modifier le mode le l’image** **`Image > Mode > Couleurs indexées…`**
  - **Palette** : `Utiliser une palette noir et blanc (1-bit)`
  - **Tramage** > Tramage des couleurs : `Floyd-Steinberg (normal)`
  - **`Convertir`**

![Conversion en couleurs indexées](https://i.imgur.com/rh45N9w.png)

5. **Exporter `Fichier > Exporter sous…`**
  - **Nommer votre fichier** comme bon vous semble, par exemple `image-dithered-b&w-1024w`
  - **Choisir ou créer le dossier** de destination
  - Cliquer sur **`Sélectionner le type de fichier`** (en bas de la fenêtre) pour afficher tous les formats d’exports possibles
  - Sélectionner **`Image PNG`** 
  - **`Exporter`**
    ![Exporter l’image](https://i.imgur.com/MscmxSX.png)
  - un nouvelle fenêtre s’ouvre : laissez les paramètres par défaut (voir image ci-dessous)
  - **`Exporter`**
    
![Exporter l’image en PNG](https://i.imgur.com/vVyV68q.png)

6. **Compresser** votre image à l’aide de [TinyPNG](https://tinypng.com/)
  - **Sélectionner ou glisser-déposer** votre fichier (20 fichiers à la fois maximum)
  - **Attendre** que la compression s’effectue
  - **Télécharger** votre fichier compressé, 1 par 1 ou tous en-même temps en cliquant sur `Download All`
  - **Remplacer** votre précédant fichier (non compressé) par celui que vous venez de télécharger (compressé, ou *tinyfied*)

---

## Glossaire

### Image vectorielle

Une [image vectorielle](https://fr.wikipedia.org/wiki/Image_vectorielle) est une image constituée d’objets géométriques individuels (segments de droite, arcs de cercle, courbes de Bézier, polygones, etc.), définis chacun par différents attributs (forme, position, couleur, remplissage, visibilité, etc.) et auxquels on peut appliquer différentes transformations.

- **Formats** (les plus communs) : 
  - SVG : format vectoriel universel, affichable directement dans le navigateur
  - AI : format vectoriel du logiciel Adobe Illustrator

Les notions de *pixels*, de *définition* et de *résolution* ne s’appliquent pas à une image vectorielle.

### Image matricielle

Une [image matricielle](https://fr.wikipedia.org/wiki/Image_matricielle) (ou *bitmap*) est un image constituée d’une matrice de points colorés, c’est-à-dire d’un tableau, d’une grille, où chaque case possède une couleur qui lui est propre et est considérée comme un point (les fameux pixels).

- **Formats** (les plus communs, tous affichables dans le navigateur) : 
  - <abbr title="Joint Photographic Experts Group">JPEG</abbr>/JPG : adapté pour les photographies et les images complexes, ne permet pas la gestion de la transparence, compression avec perte (*lossy compression*)
  - <abbr title="Portable Network Graphics">PNG</abbr> : adapté pour tout le reste, dont les images en couleurs indéxées, permet la gestion transparence
  - <abbr title="Graphics Interchange Format">GIF</abbr> : adapté pour les images animées et/ou en couleurs indéxées, limité à 256 couleurs, gère également la transparence, compression sans perte (*lossy compression*)
- **Définition** : nombre de pixels composants une image matricielle, soit le nombre de pixels qui la composent en hauteur et en largeur.
*exemple* : 200px par 450px, abrégé en « 200 × 450 »
- **Résolution** : nombre de pixels par unité de longueur (point par pouce = ppp, ou *dot per inch* = *dpi* en anglais)
*exemples* : 72dpi (pour le web), 144dpi (écrans Retina), 300dpi (pour l’impression)
- **Mode** : modèle mathématique destiné à représenter la couleur par des grandeurs auxquelles on peut associer des nombres
  - <abbr title="Rouge Vert Bleu">**RVB**</abbr> (<abbr title="Red Green Blue">*RGB*</abbr> en anglais) : synthèse additive, adapté pour l’écran et le web. Valeurs entre 0 et 255. 
*exemple* : rgb(255, 0, 0) = rouge
  - <abbr title="Cyan Magenta Jaune Noir">**CMJN**</abbr> (<abbr title="Cyan Magenta Yellow BbacK">*CMYK*</abbr> en anglais) : synthèse soustractive, adapté pour l’impression. Valeurs entre 0 et 100. 
*exemple* : cmyk(0, 100, 100, 0) = orange
  - **Nivaux de gris** (*Greyscale* en anglais) : image en noir et blanc (avec toutes les valeurs de gris entre)
  - **Couleurs indexées** (*Indexed colors* en anglais) : seulement certaines couleurs définies seront utilisées (peut importe leur nombre). Moins il y a de couleurs, moins le fichier pesera lourd à la fin.
*exemple* : image en bichromie jaune et noir

---

## Pour aller plus loin…

### Image numérique

- Wikipédia, [Image numérique](https://fr.wikipedia.org/wiki/Image_num%C3%A9rique)

### Compression d’images

- [La compression des images numériques](http://x.heurtebise.free.fr/Enseignements/ATER/S3/ImInfo/PDF/ImInfo_CM_chap5.pdf) (support de cours PDF)
- Wikipédia, [Diffusion d’erreur](https://fr.wikipedia.org/wiki/Diffusion_d%27erreur) (Dither)
- Olivier Losson, [Traitement d’images – Compression d’images](http://master-ivi.univ-lille1.fr/fichiers/Cours/ti-semaine-12-compression.pdf) (cours théorique, principes mathématiques avancés)

### Low-tech numérique
- Low-tech Magazine, [How to build a low-tech website](https://solar.lowtechmagazine.com/2018/09/how-to-build-a-lowtech-website.html)
- Gauthier Roussilhe, [Éco-conception, le brouillard à venir [3/4]](https://gauthierroussilhe.com/articles/eco-conception-le-brouillard-a-venir#est-ce-quoptimiser-un-service-cest-eco-concevoir)
- GreenIT, [Pour une low-tech numérique](https://www.greenit.fr/2019/09/24/%EF%BB%BFpour-une-low-tech-numerique/)