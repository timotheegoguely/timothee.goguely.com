---
title: Formations et ressources en accessibilité web et numérique
slug: formations-ressources-a11y-web-numerique
date: 2024-08-05
lastmod: 2024-08-08
description: Formations, livres, blogs et sites pour se former à l’accessibilité web et numérique. 
tags:
  - a11y
  - formation
  - ressources 
image: /media/a11y.png
comments: https://mastodon.design/@timotheegoguely/112910138588616066
stats:
  - name: Poids de la page
    dots: ...
    value: 45Ko
  - name: Nombre de requêtes
    dots: .
    value: 7
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 293
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

Les enjeux, les normes et les bonnes pratiques de l’accessibilité web (et numérique plus généralement) sont malheureusement encore trop méconnues par nombre d’enseignant·es et de professionnel·les du design, du développement et de la rédaction web. 

N’ayant pas du tout été sensibilisé à ce sujet durant mes études, j’ai voulu progressivement monter en compétences ces dernières années, en suivant certaines [formations](#formations) et en parcourant [livres](#livres), [blogs](#blogs), [sites](#sites) et [autres](#autres) ressources spécialisées.
Afin d’encourager et de faciliter cet apprentissage par tout un chacun, je vous partage ici une sélection des meilleures d’entre elles.

---

<h2 id="definitions">Qu’est-ce que l’accessibilité web et numérique&#8239;?</h2>

Mais avant tout, commençons par définir ce que l’on entend par «&#8239;accessibilité web&#8239;» ou «&#8239;accessibilité numérique&#8239;». Voici ce que l’on peut lire sur quelques-uns des principaux sites de référence&#8239;:

<blockquote>
  <p><strong>L’accessibilité numérique est la mise à la disposition de tous les individus</strong>&#8239;–&#8239;quels que soient leur matériel ou logiciel, leur infrastructure réseau, leur langue maternelle, leur culture, leur localisation géographique, ou leurs aptitudes physiques ou mentales&#8239;–&#8239;des ressources numériques.</p>
  <footer><a href="https://www.w3.org/standards/webdesign/accessibility" hreflang="en" lang="en"><cite>W3C Accessibility</cite></a></footer>
</blockquote>

<blockquote>
  <p><strong>L’accessibilité du web signifie que les personnes handicapées peuvent l’utiliser</strong>. Plus spécifiquement, elle signifie que ces gens peuvent <strong>percevoir</strong>, <strong>comprendre</strong>, <strong>naviguer</strong>, <strong>interagir</strong> avec le web, et y <strong>contribuer</strong>. L’accessibilité du web bénéficie également à d’autres, notamment les personnes âgées ayant des capacités diminuées dues au vieillissement.</p>
  <footer><a href="https://www.w3.org/WAI/fundamentals/accessibility-intro/" hreflang="en" lang="en"><cite>W3C Web Accessibility Initiative (WAI)</cite></a></footer>
</blockquote>

<blockquote>
  <p>L’<strong>accessibilité numérique</strong> consiste à rendre les contenus et services numériques <strong>compréhensibles</strong> et <strong>utilisables</strong> par les personnes en situation de handicap.</p>
  <footer><a href="https://accessibilite.numerique.gouv.fr/"><cite>Référentiel Général d’Amélioration de l’Accessibilité (RGAA)</cite></a></footer>
</blockquote>

<h2 id="formations">Formations</h2>

- **[Développer des sites web accessibles et conformes au RGAA](https://formations.access42.net/formations/formation-developpement-accessible/)** <span class="opacity-60">(1500€ HT, en ligne / présentiel, français)</span> ★ <br>J’ai eu la chance de suivre cette formation certifiante de 3 jours proposée par Access42 en juin 2020 avec le regretté [Jean-Pierre Villain](https://access42.net/hommage-jean-pierre-villain/)&#8239;: sans doute l’une des meilleures formations en France à ce sujet. À noter&#8239;: de nombreuses solutions existent pour [financer votre formation](https://formations.access42.net/foire-aux-questions/#financement).
- **<a href="https://practical-accessibility.today/" hreflang="en" lang="en">Practical Accessibility</a>** <span class="opacity-60">(399$, en ligne, anglais)</span> ★ <br>Une formation en ligne extrêmement riche et documentée (38 leçons, 15h de vidéos), à destination des développeur·euses front-end et des web designers, par [Sara Soueidan](https://www.sarasoueidan.com/), l’une des grandes spécialistes indépendantes de l’accessibilité numérique à l’échelle internationnale. À noter&#8239;: une [réduction pour les étudiant·es](https://practical-accessibility.today/#faq) est possible.
- **<a href="https://www.w3.org/WAI/courses/foundations-course/" hreflang="en" lang="en">Digital Accessibility Foundations</a>** <span class="opacity-60">(gratuit, en ligne, anglais)</span> <br>La formation gratuite proposée par la <a href="https://www.w3.org/WAI/courses/foundations-course/" hreflang="en" lang="en">Web Accessibility Initiative</a> (WAI).
- **[Introduction to Content Design](https://www.futurelearn.com/courses/introduction-to-content-design)** <span class="opacity-60">(gratuit, en ligne, anglais)</span> <br>Une formation pour découvrir les principes fondamentaux de la conception de contenu et apprendre à concevoir des contenus accessibles qui répondent aux besoins des utilisateurs.
- **[Concevez un contenu web accessible](https://openclassrooms.com/fr/courses/6691346-concevez-un-contenu-web-accessible)** <span class="opacity-60">(gratuit, en ligne, français)</span> <br>Une bonne introduction de 6h par le célèbre site OpenclassRooms.
- **[Codez un site web accessible avec HTML & CSS](https://openclassrooms.com/fr/courses/6691451-codez-un-site-web-accessible-avec-html-css)** <span class="opacity-60">(gratuit, en ligne, français)</span> <br>Une autre formation de 6h d’OpenclassRooms, plus poussée et axée développement front-end.
- **[Guide Accessibilité](https://developer.mozilla.org/fr/docs/Web/Accessibility)** <span class="opacity-60">(gratuit, en ligne, français / anglais)</span> <br>Le guide de <abbr title="Mozilla Developer Network">MDN</abbr> pour vous aiguiller parmis tous les contenus et tutoriels consacrés à l’accessibilité qu’ils proposent.
- <del>**<a href="https://web.dev/learn/accessibility" hreflang="en" lang="en">Learn Accessibility</a>** <span class="opacity-60">(gratuit, en ligne, français / anglais)</span> <br>Le cours très complet sur l’accessibilité de [web.dev](https://web.dev/) pour améliorer vos compétences en développement web.</del> <ins cite="https://mastodon.zaclys.com/@pikselkraft/112913814240689981" datetime="2024-08-06">Adrian Roselli explique en détails dans <a href="https://adrianroselli.com/2024/07/dont-use-webdev-for-accessibility-info.html" hreflang="en" lang="en">cet article</a> pourquoi ce site est problématique (merci [@pikselkraft](https://mastodon.zaclys.com/@pikselkraft/112913814240689981) pour le lien).</ins>
- **<a href="https://stephaniewalter.design/accessibility-for-designers-workshop/" hreflang="en" lang="en">Accessibility for Designers Workshop</a>** <span class="opacity-60">(payant, en ligne, français / anglais)</span> <br>Un workshop à destination des designers en entreprise proposé par <a href="https://stephaniewalter.design/" hreflang="en" lang="en">Stéphanie Walter</a> (merci à elle de me l’avoir signalé sur [Mastodon](https://front-end.social/@stephaniewalter/112927051620846365)).

<h2 id="livres">Livres</h2>

- **<a href="https://www.oreilly.com/library/view/web-accessibility-cookbook/9781098145590/" hreflang="en" lang="en">Web Accessibility Cookbook</a>** <span class="opacity-60">(55€, anglais)</span> ★ <br>Le tout dernier livre que je me suis commandé, écrit par l’expert autrichien en HTML, accessibilité, architecture et layout CSS [Manuel Matuzovic](https://www.matuzo.at/), aka [@matuzo](https://front-end.social/@matuzo). À noter&#8239;: les versions [EPUB + PDF](https://www.ebooks.com/en-fr/book/211381474/web-accessibility-cookbook/manuel-matuzovic/) sont à 40€.
- **<a href="https://accessibilityforeveryone.site/" hreflang="en" lang="en">Accessibility For Everyone</a>** <span class="opacity-60">(24$, anglais)</span> ★ <br>Le livre de référence consacré à l’accessibilité de la célèbre collection <span lang="en">A Book Apart</span>, écrit par [Laura Kalbag](https://laurakalbag.com/), la co-fondatrice de <a href="https://small-tech.org/" hreflang="en" lang="en">Small Technology Foundation</a>.
- **<a href="https://book.inclusive-components.design/" hreflang="en" lang="en">Inclusive Components</a>** <span class="opacity-60">(18€, anglais)</span> ★ <br>Le livre d’[Heydon Pickering](https://heydonworks.com/), aka [@heydon](https://mastodon.social/@heydon), au sein duquel il examine les patterns d’interface utilisateur web courants sous l’angle de l’inclusion, en essayant de trouver les solutions les plus accessibles et robustes possibles. À noter&#8239;: le livre est accompagné d’un [site web](https://inclusive-components.design/) avec pleins d’exemples richement documentés.

<h2 id="blogs">Blogs</h2>

- **<a href="https://adrianroselli.com/posts" hreflang="en" lang="en">Adrian Roselli</a>** <span class="opacity-60">(anglais)</span> ★ <br>Le blog de l’une des personnes les plus reconnues mondialement en matière d’accessibilité numérique. Attention, c’est assez technique&#8239;!
- **[Access42](https://access42.net/blog/)** <span class="opacity-60">(français)</span> <br>L’incontournable blog des expert·es français·es en la matière.
- **<a href="https://www.a11y-collective.com/blog/" hreflang="en" lang="en">The A11Y Collective</a>** <span class="opacity-60">(anglais)</span> <br>Le blog de <a href="https://www.a11y-collective.com/" hreflang="en" lang="en">The A11Y Collective</a>, une plateforme de formation à l’accessibilité web payante. Je n’ai jamais testé leurs cours, mais leurs articles sont très biens.
- **<a href="https://www.scottohara.me/writing/" hreflang="en" lang="en">Scott O’Hara</a>** <span class="opacity-60">(anglais)</span> <br>De très bons articles écrit par un développeur & designer UX / ingénieur en accessibilité (si si, ça existe).
- **[Accessibilité – Stéphanie Walter](https://stephaniewalter.design/blog/category/accessibility/)** <span class="opacity-60">(français / anglais)</span> <br>Tous les articles dédiés à l’accessibilité d’une experte en <span lang="en">UX research & product design</span>, donc plutôt à destination des designers.

<h2 id="sites">Sites</h2>

- **<a href="https://www.accessibility-developer-guide.com/" hreflang="en" lang="en">Accessibility Developer Guide</a>** <span class="opacity-60">(anglais)</span> ★ <br>Une initiative de la fondation suisse <span href="en">Access for all</span> rassemblant plein d’exemples et de ressources classées par thématiques, co-écrites par de grandes agences web.
- **<a href="https://www.a11yproject.com/" hreflang="en" lang="en">The A11Y Project</a>** <span class="opacity-60">(anglais)</span> ★ <br>Un site regroupant des articles, des portraits d’expert·es en accessibilité et de nombreuses ressources classées par sujets.
- **[Accessibility Myths](https://a11ymyths.com/fr/)** <span class="opacity-60">(français / anglais)</span> <br>22 mythes de l’accessibilité débunkés.
- **<a href="https://www.smashingmagazine.com/category/accessibility" hreflang="en" lang="en">Accessibility – Smashing Magazine</a>** <span class="opacity-60">(anglais)</span> <br>Tous les articles étiquetés <q lang="en">Accessibility</q> sur le site de référence en développement front-end et UX design <span lang="en">Smashing Magazine</span>.

<h2 id="autres">Autres</h2>

- **[RGAA](https://accessibilite.numerique.gouv.fr/)** <span class="opacity-60">(référentiel, français)</span> ★ <br>La 4<sup>e</sup> version du Référentiel Général d’Amélioration de l’Accessibilité de la <abbr title="Direction Interministérielle du NUMérique ">DINUM</abbr>, structuré en 2 parties&#8239;: [Les obligations légales](https://accessibilite.numerique.gouv.fr/obligations/) et [La méthode technique](https://accessibilite.numerique.gouv.fr/methode/).
- **[BingO Bakery: Headings, Landmarks, and Tabs](https://www.youtube-nocookie.com/embed/HE2R86EZPMA)** <span class="opacity-60">(vidéo, 5'30", anglais)</span> ★ <br>Une super vidéo didactique de Microsoft pour sensibiliser aux problématiques liées aux niveaux de titres, aux landmarks et à la navigation au clavier au sein d’une page web.
- **[WAVE](https://wave.webaim.org/extension/)** <span class="opacity-60">(extension navigateur, anglais)</span> ★ <br>L’extension navigateur la plus complète et utile pour tester et détecter bon nombre des problèmes d’accessibilité les plus courants sur n’importe quelle page web.
- **[Accessibilité – Le wiki de la Lutine du Web](https://wiki.lalutineduweb.fr/accessibilite/)** <span class="opacity-60">(veille, français)</span> ★ <br>Toute la veille commentée de l’intégratrice web et consultante en accessibilité web Julie Moynat, aka [La Lutine du Web](https://www.lalutineduweb.fr/).
- **[are.na/timothee-goguely/a11y-fufrtbbet50](https://www.are.na/timothee-goguely/a11y-fufrtbbet50)** <span class="opacity-60">(veille)</span> <br>Et pour finir, mon channel Are.na ou je stocke toute ma veille en vrac sur l’accessibilité.

---

Et voilà&#8239;! En espérant que vous avez découvert des ressources que vous ne connaissiez pas déjà et que cela vous a donné envie d’en apprendre plus concernant ce domaine passionnant. Si vous pensez que j’en ai oublié certaines qui vous semblent incontournables, n’hésitez pas à me les suggérer en commentaires, je les ajouterai avec plaisir.

Car oui, il est de notre responsabilité à toutes et tous, nous professionnel·les du design et du web, de nous former à ces sujets et d’y sensibiliser nos étudiant·es, nos confrères, nos consœurs et nos commanditaires, afin de respecter et faire perdurer l’un de ses principes fondateurs&#8239;: l’**universalité**.

<blockquote>
  <p><strong>Le pouvoir du Web est dans son universalité. <br>L’accès pour tous, quel que soit le handicap, est un aspect essentiel.</strong></p>
  <footer><span lang="en">Tim Berners-Lee</span>, directeur du <abbr title="World Wide Web Consortium" lang="en">W3C</abbr> et inventeur du <span lang="en">World Wide Web</span></footer>
</blockquote>

    

