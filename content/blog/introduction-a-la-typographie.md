---
title: Introduction à la typographie
slug: introduction-a-la-typographie
date: 2022-02-10
publishdate: 2022-02-10
description: Enseigner la typographie à des étudiant·e·s en première année de master web design à l’ECV Digital.
tags:
  - cours
  - typographie
  - retour d’expérience
image: /media/atelier.png
stats:
  - name: Poids de la page
    dots: ...
    value: 65Ko
  - name: Nombre de requêtes
    dots: .
    value: 9
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 118
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%
draft: true

---

Lorsque j’ai commencé à enseigner à l’[ECV Digital](https://www.ecv.fr/digital/) en 2016, aucun cours ou workshops au sujet de la typographie n’étaient proposés aux étudiant·e·s durant leur master. Si certain·e·s avaient déjà eu quelques heures dans leur cursus, c’était loin d’être le cas de tout le monde. 
J’ai alors proposé en 2019 au directeur pédagogique de mettre en place un petit module d’une dizaine d’heures afin qu’ils repartent toutes et tous avec une culture typographique commune – ce qu’il accepta.

Me voilà alors à me replonger dans [mes livres de typographie préférés](/slides/introduction-a-la-typographie/#/6/4) et quelques sites spécialisés pour tâcher de mettre sur pieds un support de cours digne de ce nom. Après de longues soirées de lecture et de recherches, j’ai fini par aboutir à un premier plan que j’ai progressivement amélioré et enrichi chaque année. Le voici dans sa version actuelle :

---

## Introduction à la typographie

- [Définitions](/slides/introduction-a-la-typographie/#/1) : de quoi parle-t-on
  - [Qu’est-ce que la typographie](/slides/introduction-a-la-typographie/#/1/1)
  - [La typographie est-elle une science, de l’ingénieurie, ou un art ?](/slides/introduction-a-la-typographie/#/1/5)
  - [À qui s’adresse la typographie ?](/slides/introduction-a-la-typographie/#/1/10)
  - [Pourquoi la typographie est-elle importante ?](/slides/introduction-a-la-typographie/#/1/13)
  - [Qu’est-ce qu’une bonne typographie ?](/slides/introduction-a-la-typographie/#/1/16)
  - [Vocabulaire](/slides/introduction-a-la-typographie/#/1/23)
- [Brève et partielle histoire de la typographie occidentale](/slides/introduction-a-la-typographie/#/2)
  - [Origines de l’imprimerie](/slides/introduction-a-la-typographie/#/2/1)
  - [Les débuts de l’imprimerie en Europe](/slides/introduction-a-la-typographie/#/2/6)
  - [Garaldes](/slides/introduction-a-la-typographie/#/2/13) du XVI<sup>e</sup> au XVII<sup>e</sup> siècle
  - [Réales : old style et transitional](/slides/introduction-a-la-typographie/#/2/27) fin XVII<sup>e</sup> et XVIII<sup>e</sup> siècle
  - [Didones](/slides/introduction-a-la-typographie/#/2/35) fin XVIII<sup>e</sup> début XIX<sup>e</sup>
  - [Essor industriel](/slides/introduction-a-la-typographie/#/2/42) XIX<sup>e</sup>
  - [XX<sup>e</sup> siècle](/slides/introduction-a-la-typographie/#/2/47) première moitié
  - [XX<sup>e</sup> siècle](/slides/introduction-a-la-typographie/#/2/55) sonconde moitié
  - [XXI<sup>e</sup> siècle](/slides/introduction-a-la-typographie/#/2/63) des années 2000 à aujourd’hui
- [La lettre](/slides/introduction-a-la-typographie/#/3) : observer et décrire
  - [Anatomie](/slides/introduction-a-la-typographie/#/3/1)
  - [Ligne de base et hauteur d’x](/slides/introduction-a-la-typographie/#/3/2/0)
  - [Propriétés formelles](/slides/introduction-a-la-typographie/#/3/5) (objectives)
  - [Classifications](/slides/introduction-a-la-typographie/#/3/19) (subjectives)
  - [Connotations](/slides/introduction-a-la-typographie/#/3/27) (collectives)
  - [Exercice](/slides/introduction-a-la-typographie/#/3/43) : que connotent ces fontes ?
- [Le texte](/slides/introduction-a-la-typographie/#/4) : fonts pairing & typesetting
  - [Fonts pairing](/slides/introduction-a-la-typographie/#/4/1) : comment associer des polices de caractères ?
  - [Typesetting](/slides/introduction-a-la-typographie/#/4/15) : comment rythmer, composer et structurer un texte ?
  - [Études de cas](/slides/introduction-a-la-typographie/#/4/36)
- [Typographie & web](/slides/introduction-a-la-typographie/#/5) : code, écosystème et économie
  - [Unicode & UTF-8](/slides/introduction-a-la-typographie/#/5/1)
  - [Web fonts](/slides/introduction-a-la-typographie/#/5/2) : formats
  - [OpenType features](/slides/introduction-a-la-typographie/#/5/4)
  - [CSS Fonts](/slides/introduction-a-la-typographie/#/5/8)
  - [CSS Text](/slides/introduction-a-la-typographie/#/5/9)
  - [Distributeurs](/slides/introduction-a-la-typographie/#/5/11)
  - [Fonderies](/slides/introduction-a-la-typographie/#/5/12)
  - [Pricing & licensing](/slides/introduction-a-la-typographie/#/5/18)
- [Ressources](/slides/introduction-a-la-typographie/#/6) : pour aller plus loin
  - [Dessin de caractère](/slides/introduction-a-la-typographie/#/6/1)
  - [Gestion de polices de caractères](/slides/introduction-a-la-typographie/#/6/2)
  - [Identification de police](/slides/introduction-a-la-typographie/#/6/3)
  - [Bibliographie](/slides/introduction-a-la-typographie/#/6/4)
  - [Newsletters et sites divers](/slides/introduction-a-la-typographie/#/6/5)

---

## Safari typographique

Entre les deux séances, c’est safari typo : ils doivent photographier une curiosité typographique par jour pendant 2 semaines et de la publier sur un [channel are.na](https://are.na/ecv-digital/) commun en l’accompagnant d’un titre et d’une petite description expliquant brièvement ce qui les a interpellé.

## Exercice pratique

À la suite du cours, je leur donne un [exercice pratique](https://hackmd.io/@timotheegoguely/fonts-pairing-typesetting) où je leur demande de travailler sur le thème de blog d’un organisation fictive qu’il choisissent parmis une liste donnée. Ils doivent proposer deux options de couple typographique et définir les réglages typographiques (corps, graisse, style, casse, interlettrage, interlignage, justification, marges et couleur) pour chaque niveau hiérarchiques (titre, sous-titres, paragraphe d’introduction, texte courant, liens, citations, mises en exergues, listes et légendes). 

J’en profite pour les encourager à éviter d’utiliser des Google Fonts et à priviligier des polices provenant de type designers et de [fonderies indépendantes](https://mattymatt.co/type-foundry-directory/). L’ensemble doit être accompagné d’une note d’intention argumentée et utilisant avec précision le vocabulaire vu en cours afin de justifier leurs différents choix (typo)graphiques.

## Mardown + Hugo + Reveal.js

Pour la session 2022, j’en ai profité pour développer un petit thème [reveal.js](https://revealjs.com/) afin de publier mes slides directement sur mon site et d’avoir entièrement la main sur la mise en forme. J’aime beacoup la double navigation horizontale pour passer d’un chapitre à une autre et verticale pour « plonger » dans un chapitre. 

Tout le contenu est rédigé directement en markdown puis est injecté au sein d’un layout Hugo en utilisant la fonction `{{.RawContent}}`, de façon a être compris par reveal.js (la [documentation](https://revealjs.com/markdown/) à ce sujet sur leur site est très complète). Pour des besoins de mise en forme spécifiques (positionnements absolus ou marges particulières par exemple), j’écris directement en HTML avec du CSS via l’attribut `style` au sein du markdown, et ça fonctionne très bien !

Alors oui, cela m’a demandé de nombreuses heures de travail, mais écrire du markdown, du HTML et du CSS est toujours un plaisir pour moi. Et je suis assez fier aujourd’hui de pouvoir le partager publiquement, même s’il y a évidemment encore plein de choses qui pourraient être améliorées.

## Retours

Je suis donc très preneur de vos retours si vous identifiez des erreurs, des imprécisions ou des manques – en gardant tout de même en tête qu’il s’agit d’un support pour un cours de moins de dix heures à destination de gens pour qui cela est potentiellement leur premier cours de typo et que n’y figurent pas tout ce que je leur raconte à l’oral pendant le cours.

Et si cela vous intéresse, j’ai également un channel are.na [Typographie](https://are.na/timothee-goguely/typographie-1523470833) où je mets de côté toutes les ressources intéressantes autour de ce sujet sur lesquelles je tombe au grès de mes navigations sur le web. 
Bonne exploration !