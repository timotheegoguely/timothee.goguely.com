---
title: En ce moment
slug: now
layout: now
lastmod: 2024-10-21
description: Ce sur quoi je travaille en ce moment, ce que j’enseigne, lis et écoute.
stats:
  - name: Poids de la page
    dots: ...
    value: 41Ko
  - name: Nombre de requêtes
    dots: .
    value: 7
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 104
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---

## je travaille sur

- le développement web du site de [Noranim](https://www.noranim.fr), l’association professionnelle du cinéma d’animation en région Hauts de France
- la refonte du site de [Hubblo](https://hubblo.org), une société qui accompagne la transformation systémique des organisations du numérique
- le développement web du site de l’enseignant-chercheur [Vivien Philizot](https://accra-recherche.unistra.fr/laccra/membres/chercheurs/vivien-philizot/) de l’Univseristé de Strasbourg, avec la complicité d’Arthur Pons
- le site web d’[Amélie Chiba](https://ameliechiba.com), créatrice de bijoux franco-japonaise

---

## j’enseigne

- l’**UI/UX design** et les bases d’**HTML/CSS** en 3<sup>e</sup> année à l’[Université de Strasbourg](https://arts.unistra.fr/faculte/departements/arts-visuels/design) (2h par semaine)
- l’**UI/UX design** au sein du [Master Design : Environnements numériques](https://arts.unistra.fr/formations/masters/master-design/environnements-numeriques) de l’Université de Strasbourg (2h par semaine d’atelier de projet numérique avec les M1, 3h par semaine d’atelier de projet collectif avec les M2)

---

## je lis

- Becky Chambers, <cite>[Les voyageurs, tome 1 : L’espace d'un an](https://www.placedeslibraires.fr/livre/9782253260592-les-voyageurs-tome-1-l-espace-d-un-an-becky-chambers/)</cite> ★
- bell hooks, <cite>[apprendre à transgresser](https://www.placedeslibraires.fr/livre/9782849507506-apprendre-a-transgresser-l-education-comme-pratique-de-la-liberte-bell-hooks/)</cite> ★
- Silvio Lorusso, <cite lang="en">[What Design Can’t Do](https://www.setmargins.press/books/what-design-cant-do/)</cite> ★
- Kevin Yuen Kit Lo, <cite lang="en">[Design Against Design](https://www.setmargins.press/books/design-against-design/)</cite>
- David Reinfurt, <cite>[Un nouveau programme pour le design graphique](https://www.editions205.fr/products/un-nouveau-programme-pour-le-design-graphique)</cite> ★

---

## j’écoute

- Oklou, <cite lang="en">[harvest sky / obvious](https://oklou.bandcamp.com/album/harvest-sky-obvious)</cite> ★
- ML Buch, <cite lang="en">[Suntub](https://mlbuch.bandcamp.com/album/suntub)</cite> ★
- Time Capsule, <cite lang="en">[Nippon Acid Folk 1970–t1980](https://timecapsulespace.bandcamp.com/album/nippon-acid-folk-1970-1980)</cite>
- NTS, <cite lang="en">[European Primitive Guitar (1974-1987)](https://n-t-s.bandcamp.com/album/european-primitive-guitar-1974-1987)</cite>