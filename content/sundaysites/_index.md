---
title: Sunday Sites
slug: sundaysites
date: 2023-01-22
description: Welcome to my user page for Sunday Sites.
stats:
  - name: Poids de la page
    dots: ...
    value: 65Ko
  - name: Nombre de requêtes
    dots: .
    value: 8
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 118
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%
---

Welcome to my user page for [Sunday Sites](https://sundaysites.cafe/).

---

<h2 lang="en">My Sunday Sites</h2>

<ul lang="en">
  <li>
    Thirty-second Session – <time datetime="2023-08-27">August 27th, 2023</time> <br>
    🚄 <a href="./make-a-site-with-a-video/index.html"><cite>Make a site with a &lt;video&gt;</cite></a>
  </li>
  <li>
    Thirty-first Session – <time datetime="2023-06-23">July 23th, 2023</time> <br>
    ▶︎ <a href="./make-a-site-using-the-details-summary-element/index.html"><cite>Make a site using the details/summary element</cite></a>
  </li>
  <li>
    Twenty-eighth Session – <time datetime="2023-04-16">April 15th, 2023</time> <br>
    🏕️ <a href="./make-a-camp-site/index.html"><cite>Make a camp site</cite></a>
  </li>
  <li>
    Twenty-seventh Session – <time datetime="2023-03-19">March 19th, 2023</time> <br>
    🌞 <a href="./make-a-site-for-keeping-notes/index.html"><cite>Make a site for keeping notes</cite></a>
  </li>
  <li>
    Twenty-fifth Session – <time datetime="2023-01-22">January 22th, 2023</time> <br>
    🏴 <a href="./make-a-site-for-your-future-self/index.html"><cite>Make a site for your future self</cite></a>
  </li>
</ul>
