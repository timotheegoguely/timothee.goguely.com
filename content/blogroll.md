---
title: Blogroll
slug: blogroll
layout: blogroll
lastmod: 2024-01-24
description: Blogs, journaux, carnets et notes de celles et ceux qui pensent et construisent le web et le numérique d’aujourd’hui et de demain.
blogs:
  - name: Gauthier Roussilhe
    link: https://gauthierroussilhe.com/articles/
    lang: fr
    description: Chercheur spécialisé sur les enjeux environnementaux de la numérisation, en s'appuyant sur un travail de terrain, la compréhension du fonctionnement des infrastructures et des services numériques.
    tags:
      - recherche
      - numérique
      - éducation
      - écoconception
  - name: Julien Bidoret
    link: https://accentgrave.net/log/
    lang: fr
    description: Graphiste, développeur web, bricoleur numérique, enseignant. Intéressé par le partage de connaissances, d'outils et de doutes. Adepte de web2print, amoureux du CSS, utilisateur de logiciels libres et web designer radical.
    tags:
      - blogging
      - publication numérique
      - design
  - name: Julie Blanc
    link: https://julie-blanc.fr/blog/
    lang: fr
    description: Designer graphique, développeuse CSS et docteure en ergonomie et design graphique. Elle utilise les technologies du web pour concevoir toutes sortes de publications, numériques et imprimées.
    tags:
      - publication numérique
      - design
      - css
  - name: Antoine Fauchié
    link: https://quaternum.net/carnet/
    lang: fr
    description: Chercheur publiant des articles sur les enjeux de la publication web et numérique, des technologiques et du design.
    tags: 
      - publication numérique
      - technologies
      - design
      - numérique
  - name: Katzele
    link: http://katzele.netlib.re/notes/
    lang: fr
    description: Collectif d’informatique conviviale strasbourgeois.
    tags: 
      - numérique
      - FLOSS
      - éducation
  - name: Andy Bell
    link: https://andy-bell.co.uk/blog/
    lang: en
    description: Designer, développeur front-end anglais, expert CSS et co-fondateur de l'agence Set Studio.
    tags: 
      - design
      - css
  - name: Bastian Allgeier
    link: https://bastianallgeier.com/
    lang: en
    description: Notes du créateur du célèbre CMS Kirby.
    tags: 
      - blogging
  - name: Pikselkraft
    link: https://www.pikselkraft.com/blog/
    lang: fr
    description: Développeur web spécialisé en ergonomie et écoconception web.
    tags:
      - design
      - css
      - écoconception
  - name: Matthias Ott
    link: https://matthiasott.com/notes/
    lang: en
    description: UX design indépendant et design engineer allemand, membre de la communauté IndieWeb et curateur de la newsletter Own Your Web.
    tags: 
      - blogging
      - design
      - css
      - numérique
  - name: Stephanie Eckles
    link: https://thinkdobecreate.com/articles/
    lang: en
    description: Développeuse front-end, créatrice de ModernCSS.dev, SmolCSS and 11ty.Rocks
    tags: 
      - a11y
      - css
      - design
  - name: Thomas Parisot
    link: https://thom4.net/categories/journal/
    lang: fr
    description: Développeur et designer web publiant de courts billets sous forme de journal pour documenter ses apprentissages, ses réflexions et ses explorations à long terme.
    tags: 
      - blogging
  - name: Louis Derrac
    link: https://louisderrac.com/blog/
    lang: fr
    description: Acteur indépendant et militant de l’éducation au numérique et d’un alternumérisme radical.
    tags: 
      - blogging
      - numérique
      - éducation
  - name: Access42
    link: https://access42.net/blog/
    lang: fr
    description: Articles de fond sur l’accessibilité numérique, écrits par des expert·es en activité.
    tags: 
      - a11y
      - design
      - numérique
  - name: Adrian Roselli
    link: https://adrianroselli.com/posts/
    lang: en
    description: Expert en accessibilité numérique reconnu mondialement, auteur de nombreux ouvrages et articles à ce sujet.
    tags: 
      - a11y
      - numérique
  - name: Manuel Matuzović
    link: https://www.matuzo.at/
    lang: en
    description: Développeur front-end autrichien, spécialisé en HTML, accessibilité et en architecture et layout CSS.
    tags:
      - a11y
      - css
      - design
  - name: Sara Soueidan
    link: https://www.sarasoueidan.com/blog/
    lang: en
    description: Experte en accessibilité spécialisée dans l’implémentation de design inclusif.
    tags: 
      - a11y
      - css
      - design
stats:
  - name: Poids de la page
    dots: ...
    value: 41Ko
  - name: Nombre de requêtes
    dots: .
    value: 8
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 254
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---
