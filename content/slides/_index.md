---
title: Slides
slug: slides
description: Supports de présentations et de cours.
stats:
  - name: Poids de la page
    dots: ...
    value: 35Ko
  - name: Nombre de requêtes
    dots: .
    value: 6
  - name: Taille du <abbr title="Document Object Model">DOM</abbr>
    dots: ......
    value: 77
  - name: Score EcoIndex
    dots: .....
    value: A
  - name: Score Lighthouse
    dots: ...
    value: 100
  - name: Conformité <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
    dots: ....
    value: 100%

---
