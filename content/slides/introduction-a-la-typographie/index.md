---
title: Introduction à la typographie
slug: introduction-a-la-typographie
description: ECV Digital – Mastère DA Digital
date: 2022-10-14
cover: https://timothee.goguely.com/slides/introduction-a-la-typographie/cover.png
layout: slides
tags:
  - cours
  - typographie
transition: slide
backgroundTransition: slide

---

# Introduction à la typographie

<hr>

Timothée Goguely
<a href="https://timothee.goguely.com">timothee.goguely.com</a>
<small>Dernière mise à jour&#8239;: <time class="mono" datetime="2024-01">janvier 2024</time></small>

---

<!-- .slide: data-visibility="hidden" -->

<span class="heading-color">Timothée Goguely</span>
Designer et développeur web

<small>
  <a href="https://timothee.goguely.com">timothee.goguely.com</a>
</small>

---

<p class="subtitle">De quoi parle-t-on</p>

# Définitions

--

## Qu’est-ce que <br>la typographie&#8239;?

--

> La typographie désigne les **différents procédés de _composition_ et d’_impression_** utilisant des caractères et des formes en relief, ainsi que **l’_art_ d’utiliser les différents types de caractères dans un but _esthétique_ et _pratique_**.

<p class="text-sm">Source&#8239;: <em><a href="http://fr.wikipedia.org/wiki/Typographie">Typographie</a></em>, Wikipédia</p>

--

<!-- .slide: data-background-image="Print_works_typesetting-Beamish_Museum.jpg" -->

--

Le terme _typographie_ est employé dans 2 acceptations. 
Il désigne donc&#8239;:

- pour une page, **tout ce qui se rapporte à sa composition** (choix des polices de caractères, interlignage…) **et à sa mise en page** (proportions de la page, marges…)
- le **dispositif technique permettant d’imprimer des formes en relief** (caractères mobiles, gravure, clichés)

<p class="text-sm">Source&#8239;: <em><a href="https://www.editions205.fr/products/observer-comprendre-et-utiliser-la-typographie">Observer, comprendre et utiliser la typographie</a></em> Damien Gautier et Florence Roller</p>

--

## La typographie est-elle <br>une science, de l’ingénieurie, <br>ou un art&#8239;?

--

<div style="display: flex; justify-content: space-between; align-items: center">

<img src="JanTschichold2.jpg" alt="" width="200" height="200" style="width:200px; height:200px;border-radius: 50%;">

<blockquote>
  <p lang="en">Perfect typography <br>is <strong>more science</strong> than an art.</p>
  <footer class="text-sm mono">— Jan Tschichold</footer>
</blockquote>

</div>

--

<div style="display: flex; justify-content: space-between; align-items: center">

<blockquote>
  <p lang="en">Typography is <strong>more art</strong> <br>than engineering<br><em>—though engineering is<br> certainly a part of it.</em></em></p>
  <footer class="text-sm mono">— Robert Bringhurst</footer>
</blockquote>

<img src="RobertBringhurst2.jpg" alt="" width="240" height="240" style="width:200px; height:200px;border-radius: 50%">

--

> _Typography_ is the **visual component <br>of the written word**.

<p class="text-sm">Source&#8239;: <a lang="en" hreflang="en" href="https://typographyforlawyers.com/what-is-typography.html">What is typography?</a>, Typography for Lawyers</p>

--

> Typography is **the practice of making informed decisions about the _setting_ <br>of _type_**. Not to be confused with type design.

<p><small>Source&#8239;: <a lang="en" hreflang="en" href="https://fonts.google.com/knowledge/glossary/typography">Typography</a>, Fonts Knowledge, Google Fonts</small></p>

--

## À qui s’adresse <br>la typographie&#8239;?

--

> La typographie est **au service du _lecteur_**, pas de l’auteur.

<p class="text-sm">Source&#8239;: <a lang="en" hreflang="en" href="https://typographyforlawyers.com/who-is-typography-for.html">What is typography for?</a>, Typography for Lawyers</p>

--

<table>
  <thead>
    <tr>
      <th></th>
      <th scope="col">Auteur</th>
      <th scope="col">Lecteur</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td scope="row">Capacité d’attention</td><td>Longue</td><td><del class="opacity-60">Aussi longue que nécessaire</del> Courte</td>
    </tr>
    <tr>
      <td scope="row">Intérêt pour le sujet</td><td>Élevé</td><td><del class="opacity-60">Sans limite</del> Faible</td>
    </tr>
    <tr>
      <td scope="row">Persuadable par d’autres opinions</td><td>Non</td><td><del class="opacity-60">À peine</del> Oui</td>
    </tr>
    <tr>
      <td scope="row">Se préoccupe de votre bonheur</td><td>Oui</td><td><del class="opacity-60">Évidemment</del> Non</td>
    </tr>
  </tbody>
</table>

--

## Pourquoi la typographie <br>est-elle importante&#8239;?

--

> La typographie est importante car elle contribue à préserver la ressource la plus précieuse dont vous disposez en tant qu’auteur—**l’_attention_ du lecteur**.

<p class="text-sm">Source&#8239;: <a lang="en" hreflang="en" href="https://typographyforlawyers.com/why-does-typography-matter.html">Why does typography matter?</a>, Typography for Lawyers</p>

--

<figure class="text-center">
  <img src="resume-before-violet.png" alt="" width="475" class="bg-white">
  <img src="resume-after.png" alt="" width="475" class="bg-white">
</figure>

--

## Qu’est-ce qu’une <br>bonne typographie&#8239;?

--

<div style="display: flex; justify-content: space-between; align-items: center">

<img src="JanTschichold.jpg" alt="" width="200" height="200" style="width:200px; height:200px;border-radius: 50%;">

<blockquote style="max-width: 640px">
  <p lang="en">The essence of new typography is <strong>clarity</strong>.</p>
  <footer class="text-sm mono">— Jan Tschichold</footer>
</blockquote>

</div>

--

<div style="display: flex; justify-content: space-between; align-items: center">

<blockquote style="max-width: 680px">
  <p lang="en">Typography must <strong>invite</strong> the reader into text, <strong>reveal</strong> the tenor and meaning of the text, <strong>clarify</strong> its structure and order and <strong>link</strong> the text with other existing elements.</strong></p>
  <footer class="text-sm mono">— Robert Bringhurst</footer>
</blockquote>

<img src="RobertBringhurst2.jpg" alt="" width="200" height="200" style="width:200px; height:200px;border-radius: 50%">

</div>

--

> Une bonne typographie <br>**renforce les objectifs du texte**.

<p class="text-sm">Source&#8239;: <a lang="en" hreflang="en" href="https://typographyforlawyers.com/what-is-good-typography.html">What is good typography?</a>, Typography for Lawyers</p>

--

<ul>
  <li><strong>Une bonne typographie se mesure à sa capacité à renforcer les objectifs du texte, et non selon une échelle de qualité abstraite.</strong> <small>Ce qui fonctionne dans un cas ne fonctionnera pas nécessairement dans un autre.</small></li>
  <li class="fragment"><strong>Pour un texte donné, de nombreuses solutions typographiques peuvent fonctionner aussi bien.</strong> <br><small>Il n’y jamais qu’une seule bonne réponse.</small></li>
  <li class="fragment"><strong>Votre capacité à produire une bonne typographie dépend de votre compréhension des objectifs de votre texte, et non de vos goûts ou de votre formation visuelle.</strong> <br><small>Si vous comprenez mal les objectifs de votre texte, une bonne typographie ne serait qu’un hasard.</small></li>
</ul>

--

<figure class="text-center">
  <img src="roadsign-standard.jpg" alt="" width="720">
  <img src="roadsign-script.jpg" alt="" width="720">
</figure>

--

<figure class="text-center">
  <img src="speed-limit-75.svg" alt="" width="720">
  <figcaption>La même police est utilisée dans les 3 panneaux ci-dessus. <br>Lequel délivre le mieux le message «the speed limit is 75»&#8239;?</figcaption>
</figure>

--

## Vocabulaire

--

### Caractère

**Élément signifiant du langage** <small>(notion abstraite)</small>
Unité d’information standardisée par Unicode.

<small>`U+0061: LATIN SMALL LETTER A`</small>

<div class="fragment">
  <hr>

  ### Glyphe
  
  **Représentation graphique d’un caractère**
  <small>(parmi une infinité possible)</small>

  <span style="font-family: Arial">a</span> <span style="font-family: 'Comic Sans MS'">a</span> <span style="font-family: 'Courier'">a</span> <span style="font-family: 'Impact'">a</span> <span style="font-family: 'Georgia'">a</span> <span style="font-family: 'Verdana'">a</span> <span style="font-family: 'Menlo'">a</span> <span style="font-family: 'Optima'">a</span> <span style="font-family: fantasy">a</span> …

</div>

--

### Police de caractères / fonte

**Ensemble de glyphes** correspondant aux mêmes caractéristiques de _graisse_ et de _style_ <small> (« <span lang="en">typeface / font</span> » en anglais)</small>

<span style="font-family: Garamond">Garamond Regular</span>, <span style="font-family: Garamond; font-style: italic">Garamond Italic</span>
<span style="font-family: Garamond; font-weight: bold">Garamond Bold</span>, <span style="font-family: Garamond; font-weight: bold; font-style: italic">Garamond Bold Italic</span>

<div class="fragment">
  <hr>

  ### Famille de polices / de fontes
  
  **Ensemble de fontes** formant une même famille <small lang="en">(font-family)</small><br>

  <span style="font-family: Garamond">Garamond</span>

</div>

---

<p class="subtitle">Brève et partielle</p>

# Histoire de la typographie occidentale

--

<p class="subtitle">Occidentale&#8239;?!</p>

## Origines de l’imprimerie

<!--<p class="fixed bottom-0"><small>Source&#8239;: <a href="http://classes.bnf.fr/ecritures/arret/signe/typo/02.htm">Danièle Memet, Origines de l’imprimerie – L’aventure des écritures, BNF</a></small></p>-->

--

### 🇨🇳 Chine, XI<sup>e</sup> siècle

**Les premiers essais d’impression au moyen de caractères mobiles sont l’œuvre de Bi-Cheng** <span lang="zh">(毕升/畢昇)</span>, forgeron alchimiste, qui réalise des cubes en terre sculptés, cuits et collés dans un cadre de fer au moyen de cire et de résine.

--

<!-- .slide: data-background-image="Beijing.Musee_imprimerie.caracteres_mobiles.Bisheng.jpg" data-background-transition="slide" -->

<p class="caption" hidden>
  Reconstitution de l’outil de Bi Sheng, premiers caractères d’imprimerie mobiles, <br>Musée de l’imprimerie de Chine. Source&#8239;: <a href="https://commons.wikimedia.org/wiki/File:Beijing.Musee_imprimerie.caracteres_mobiles.Bisheng.jpg">Wikimedia Commons</a>
</p>

--

### 🇰🇷 Corée, XIV<sup>e</sup> siècle

Les Coréens exploitent leur savoir-faire en matière de gravure métallique pour fabriquer des monnaies et l'adaptent pour créer des « types » en bronze. 

Un des **premiers ouvrages imprimés à partir de caractères métalliques** est le **Traité bouddhique** du moine <span lang="ko">Kyonghan</span>, imprimé en **1377**. C’est le plus ancien connu actuellement.

<p class="text-sm">Le premier emploi des caractères mobiles métalliques en Corée remonte à <strong>1234</strong>. Le premier ouvrage reproduit serait le <span lang="ko">Sang-jong ye mun</span>, malheureusement il ne semble en subsister aucun exemplaire.</p>

--

<figure class="text-center">
  <img src="traite-bouddhique-kyonghan-coree-1377.jpg" alt="" width="360">
  <figcaption>Traité bouddhique, Jik ji sim kyông, 1377, 24,6 x 17 cm. <br>BnF, Manuscrits, coréen 109</figcaption>
</figure>

--

<p class="subtitle">78 ans plus tard en occident…</p>

## Les débuts de l’imprimerie en Europe

--

## De l’écriture gothique <br>aux caractères romains humanistes

<p class="subtitle">Seconde moitié du XV<sup>e</sup> siècle</p>

--

<!-- .slide: class="col-2" -->

<div>

### 1455 🇩🇪

**Johannes Gutenberg** achève l’impression de sa célèbre **«&#8239;Bible latine à 42 lignes&#8239;»** à Mayence, premier livre imprimé en Europe à l’aide de caractères mobiles, composé en **écriture gothique** dite textura.

</div>

<div>
  <figure>
    <img src="Bible-a-42-lignes-Bible-de-Gutenberg-02.jpg" alt="" width="320">
    <img src="189px-Gutenberg.jpg" alt="Johannes Gutenberg" width="120">
  </figure>
</div>

--

<!-- .slide: data-background-image="gutenberg-003v-004.jpg" data-background-transition="slide" -->

--

<!-- .slide: class="col-2" -->

<div>

### 1465 🇮🇹

Les imprimeurs allemands **Conrad Sweynheym et Arnold Pannartz** introduisent l’imprimerie en **Italie**.

</div>

<div>
  <figure>
    <img src="sweynheym_pannartz_subiaco_1465.jpg" alt="" width="200">
    <img src="Roman_typeface_Sweynheym_and_Pannartz_1465.png" alt="">
    <figcaption>↑ Textura Lactantius et specimen de caractère romain, Conrad Sweynheym et Arnold Pannartz, 1465.</figcaption>
  </figure>
</div>

--

<!-- .slide: class="col-2" -->

<div>

### 1470 🇮🇹

Premier **caractère romain** de **Nicolas Jenson**, à Venise.

</div>

<div>
  <figure>
    <img src="1280px-Jenson_1475_venice_laertius.png" alt="">
    <img src="Portrait_of_Nicholas_Jenson.gif" alt="Nicolas Jenson" width="120">
  </figure>
</div>

--

<!-- .slide: class="col-2" -->

<div>

### 1476 🏴󠁧󠁢󠁥󠁮󠁧󠁿

**William Caxton** imprime le premier livre en **Angleterre** dans les locaux de l’aumônerie de l'abbaye de Westminster, composé en **lettre bâtarde**.

</div>

<div>
  <figure>
    <img src="1479-Cordiale-William-Caxton.jpg" alt="" width="320">
    <img src="William_caxton.jpg" alt="William Caxton" width="120">
    <figcaption>↑ <a href="https://www.gla.ac.uk/myglasgow/library/files/special/exhibns/printing/caxton2.html">Cordiale, or Four last thinges</a>, <br>William Caxton, Westminster, 1479.</figcaption>
  </figure>
</div>

--

## Garaldes

<p class="subtitle">du XVI<sup>e</sup> au XVII<sup>e</sup> siècle</p>

--

<!-- .slide: class="col-2" -->

<div>

### 1495 🇮🇹

**Aldus Manutius** imprime 
_De Aetna_ à Venise.

<p class="text-sm">Son romain servira plus tard de modèle <br>à <strong>Garamond</strong>.</p>

</div>

<div>
  <figure>
    <img src="1495-De_Aetna-Aldus_Manutius.jpg" alt="">
    <img src="Vita_di_Aldo_Pio_Manuzio_-_portrait.png" alt="Aldus Manutius" width="120">
  </figure>
</div>

--

<!-- .slide: class="col-2" -->

<div>

## Italiques

### 1500 🇮🇹

**Premier caractère italique** 
gravé par **Francesco Griffo** 
pour Alde Manuce. 

<p class="text-sm">Son dessin se base sur celui de l’<strong>écriture cursive humaniste</strong> développée dans les années 1420 par <strong>Niccolò de’ Niccoli</strong>.</p>

</div>

<div>
  <figure>
    <img src="1501-griffo_aldine_italic.jpg" alt="" width="320">
    <img src="1420-Niccolo_de_Niccoli_italic_handwriting.jpg" alt="" width="320">
  </figure>
</div>

--

<!-- .slide: class="col-2" -->

<div>

### 1527 🇮🇹

Caractère italique dessiné 
par **Ludovico Arrighi**. 

<p class="text-sm">Elle se démarque de celle de <strong>Griffo</strong> de par sa structure plus modulaire, ses quelques <strong>ligatures</strong>, ses capitales romaines légèrement plus hautes, son angle plus doux, des ascendentes plus hautes et un interlignage plus fort, créant une sensation d’<strong>élégance</strong> et de raffinement supérieure.</p>

</div>

<div>
  <figure>
    <img src="1523-Arrighi_italic.png" alt="">
    <figcaption>↑ À origine, l’italique n’existe qu’en bas-de-casse, et est associé à des capitales romaines étroites.
  </figure>
</div>

--

<!-- .slide: class="col-2" -->

<div>

## Garamont 🇫🇷

### \~1510–1561

**Claude Garamont** grave ses fameux **Grecs du roi** pour 
**Robert Estienne** vers 1538, imprimeur de François 1<sup>er</sup>.

</div>

<div>
  <figure>
    <img src="1550-Gospel_Estienne.jpg" alt="" width="320">
    <img src="ClaudeGaramont.jpeg" alt="Claude Garamont" width="120">
    <figcaption>↑ <a href="https://en.wikipedia.org/wiki/Garamond#/media/File:Gospel_Estienne_1550.jpg">L’édition de 1550 d’Estienne du <em>Nouveau Testament</em></a>, composée avec les Grecs du roi de Garamont.</figcaption>
  </figure>
</div>

--

<figure>
  <img src="1592-garamond-specimen-Egenolff-Berner-detail2.jpg" alt="" width="526">
  <img src="1545-Claude_Garamont-italiques-Dexippus_In_defensionem_Praedicamentorum_Aristotelis.png" alt="" width="425">
  <figcaption>Romain (Spécimen Egenolff-Berner, 1592) et italique de Claude Garamond.</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

## Granjon 🇫🇷

### 1513–1590

**Robert Granjon** commence à graver ses propres caractères romains et italiques inspirés des types d’**Estienne** à partir de 1543.

<p class="text-sm">De 1563 à 1570, il grave de nombreux types pour <strong>Christophe Plantin</strong>. Ses italiques eurent une grande influence et seront commercialisées dans toute l’Europe jusqu’au XIX<sup>e</sup> siècle.</p>

</div>

<figure>
  <img src="Granjon-gros-romain-A.jpg" alt="" width="367">
  <figcaption>↑ Gros-romain italique ‘A’ (1547–), imprimé à Lyons par J. de Tournes, 1560</figcaption>
</figure>

--

<!-- .slide: class="text-center" -->

<figure>
  <img src="Granjon-Civilite_type.jpg" alt="">
  <figcaption>La célèbre «lettre francaise d’art de main» de Granjon, <br>appelée par la suite «<a href="https://en.wikipedia.org/wiki/Garamond#/media/File:Gospel_Estienne_1550.jpg">caractère de civilité</a>», qui reproduit <br>la petite cursive gothique du milieu du XVI<sup>e</sup> siècle.
</figure>

--

<!-- .slide: class="col-2" -->

<div>

## Sabon 🇫🇷

### 1535–\~1580

**Jacques Sabon** travaille pour la fonderie **Christian Egenolff** à Francfort, à partir de 1555. 

<p class="text-sm">Appelé à Anvers par <strong>Christophe Plantin</strong> en 1565, Sabon finalise la gravure d’un alphabet inachevé de <strong>Garamont</strong> de « grosses capitales extraordinaires ».</p>

</div>

<figure>
  <img src="Sabon-Specimen_Egenolff-Berner.png" alt="">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

## Jannon 🇫🇷

### 1580-1658

**Jean Jannon**, ancien de l’atelier de **Robert III Estienne**, à Paris, expert en gravure et en fonderie, maître imprimeur en 1606, s’installe à Sedan, à partir de 1610.

<p class="text-sm">La qualité et la finesse de ses types sont largement reconnues. Il grave notamment une police en très petit corps, devenue célèbre sous le nom de « Petite Sedanoise » ou « Sedanaise », qui constitue un tour de force en son temps.</p>

</div>

<figure>
  <img src="1621-Jannon.jpg" alt="" width="275">
  <figcaption>↑ Le specimen de Jean Jannon de 1621, Paris & Sedan, où l’on retrouve le «caractères de l’Université» qui deviendra le standard de la Manufacture royale d’imprimerie.</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1640

Création au Louvre de l’**Imprimerie royale** par 
le **cardinal de Richelieu**, 
sous le règne de Louis XIII.

<img src="1642-Triple_Portrait_of_Cardinal_de_Richelieu.jpg" alt="cardinal de Richelieu" width="200">

</div>

<figure>
  <img src="1640-imprimerie-royale-louvre.jpg" alt="">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

## Les Elzevier 🇳🇱

### XVII<sup>e</sup>

Aux Pays-Bas, les **Elzevier**, grande famille d’imprimeurs, dominent le XVII<sup>e</sup> siècle par la qualité et la diversité de leurs productions, notamment leur <em>in-12</em>.


<p class="text-sm">Ils emploient du <strong>Garamond</strong>, dans la lignée de <strong>Plantin</strong>, mais font aussi appel à une production locale riche et singulière, dont les « types hollandais » du célèbre <strong>Christoffel Van Dijck</strong> (1605–1669).</p>

</div>

<figure>
  <img src="1639-Geraldi_Vassiti-Velleius_Paterculis-Elzevier-Leyde.png" alt="">
  <figcaption>↑ Geraldi Vassiti, <em>Velleius Paterculis</em>, Elzevier, Leyde, Pays-Bas, 1639</figcaption>
</figure>

--

<!-- .slide: data-background-image="1639-Elzevier-Leyde.png" data-background-size="cover" data-background-transition="slide" -->

<p class="caption" hidden>Le romain de Van Dijck comparé au Garamond : plus fin, déliés plus souples, contrastes plus prononcés, empattements plus triangulaires et légers. L’axe de la lettre se redresse. Le caractère, plus géométrique, s’éloigne des sources calligraphiques.
</p>

--

<!-- .slide: data-background-color="#fff" class="text-center" data-background-transition="slide" --> 

<figure style="padding-top: 2rem">
  <img src="VanDijck.gif" alt="">
  <figcaption style="margin-top: 0rem">Van Dijck, Jan van Krimpen, Monotype, 1998</figcaption>
</figure>

--

## Réales : old style et transitional

<p class="subtitle">Fin XVII<sup>e</sup> et XVIII<sup>e</sup> siècle</p>

--

<!-- .slide: class="col-2" -->

<div>

## Grandjean 🇫🇷

### 1692 à 1745

Le **Romain du roi** est une police d’écriture commissionnée par le roi **Louis XIV** en 1692, pour être utilisée par l’Imprimerie royale.

<p class="text-sm">Ces premiers poinçons ont été gravés par <br><strong>Phillippe Grandjean</strong> (1666–1714), dont le nom est aussi utilisé pour désigner la police. Elle a été utilisée pour la première fois en 1702.</p>

</div>

<figure>
  <img src="1702-Philippe_Grandjean-Poincon_des_Romains_du_Roi-corps_120.png
" alt="" width="366">
  <figcaption>↑ Poinçon du Romains du Roi gravé par Philippe Grandjean, corps 120, 1702
</figcaption>
</figure>


--

<!-- .slide: class="col-2" -->

<div>

L’idée n’est pas de reproduire le Garamond ou les autres grands chefs-d’œuvres du passé, ou même de les interpréter, mais de les parfaire « scientifiquement ». 

<p class="text-sm">Proposer un autre style de lettre, aussi harmonieux, mais plus rigoureux : <strong>axe vertical, construction nettement géométrique, contrastes entre pleins et déliés très accentués</strong>.</p>

</div>

<figure>
  <img src="1692-Grandjean_romains_Simonneau.png" alt="" width="400">
  <figcaption>↑ Caractères Grandjean romains imprimés à partir des planches gravées par Simonneau, 1692
</figcaption>
</figure>

--

<!-- .slide: data-background-image="1699-Grandjean-epreuve_neuvieme_alphabet_droit_et_penche.png" -->

--

<!-- .slide: class="col-2" -->

<div>

## Caslon 🏴󠁧󠁢󠁥󠁮󠁧󠁿

### 1692–1766

De 1716 à 1728, **William Caslon** I dessine ses caractères **old style**, modèle du genre encore aujourd’hui.

<p class="text-sm">Il s’inspire notamment du style néerlandais.</p>

</div>

<figure>
  <img src="1728-William_Caslon.jpg" alt="">
  <figcaption>↑ William Caslon I, <em>Cyclopaedia</em>, 1728</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="text-center" data-background-transition="slide" --> 

<figure style="padding-top: 2rem">
  <img src="AdobeCaslon.gif" alt="">
  <figcaption style="margin-top: 0rem">Adobe Caslon, Carol Twombly, Adobe, 1990</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

## Baskerville 🏴󠁧󠁢󠁥󠁮󠁧󠁿

### 1706–1775

**John Baskerville**, imprimeur britannique de Birmingham. 
Vers 1750, il crée le premier caractère **transitionnal**. 

<p class="text-sm">Il dirige le graveur de poinçons John Handy, dans la création de nombreuses polices de caractères à l’apparence très similaire.</p>

</div>

<figure>
  <img src="1758-Baskerville.jpg" alt="" width="340">
  <figcaption>↑ John Milton, <em>Paradise Lost</em>, imprimé par John Baskerville, 1758</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="text-center" data-background-transition="slide" --> 

<figure style="padding-top: 2rem">
  <img src="Baskerville.gif" alt="">
  <figcaption style="margin-top: 0rem">Baskerville, Monotype, 1923</figcaption>
</figure>

--

## Didones

<p class="subtitle">Fin XVIII<sup>e</sup> début XIX<sup>e</sup></p>

--

<!-- .slide: class="col-2" -->

<div>

### Bodoni 🇮🇹

### 1740–1813

**Giambattista Bodoni** à Parme, Italie. En 1788, il réalise et publie le premier _Manuel typographique_ qui comprend les formes typographiques de son invention.

</div>

<figure>
  <img src="1788-bodoni-manuele-typographique.jpg" alt="" width="280">
  <img src="Bodoni.jpg" alt="Bodoni" width="120">
  <figcaption>↑ Première édition du <em>Manuale tipografico di Giambattista Bodoni</em>.
</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="text-center" data-background-transition="slide" --> 

<figure style="padding-top: 2rem">
  <img src="Bodoni.gif" alt="">
  <figcaption style="margin-top: 0rem">Bodoni, Morris Fuller Benton, American Type Founders, 1908-1915</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Les Didots 🇫🇷

### 1780–1850

**François Ambroise Didot** (1730-1801), fondateur d’une dynastie de fondeurs, imprimeurs et éditeurs dominant la typographie française au XIXe siècle.

<p class="text-sm">Il perfectionne le système de mesures basé sur le « point typographique » et livre de nouveaux caractères dans un registre néo-classique, dès les années 1780. Ses fils Pierre et Firmin Didot instaurent un style encore plus austère et dépouillé, s’appuyant sur de forts contrastes dans les registres de la lettre et de la mise en pages.</p>

</div>

<figure>
  <img src="1786-Didots.jpg" alt="" width="246">
  <figcaption>↑ <em>Essais de fables nouvelles dédiées au roi</em>, imprimé par François Ambroise Didot avec les caractères de son second fils Firmin, en 1786.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="text-center" data-background-transition="slide" --> 

<figure style="padding-top: 2rem">
  <img src="Didot.gif" alt="">
  <figcaption style="margin-top: 0rem">Didot, Apple, 2003</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1803 🏴󠁧󠁢󠁥󠁮󠁧󠁿

**Robert Thorne** (1754–1820), repreneur de la fonderie 
**Fann Street Foundry** à Londres, dessine le premier caractère 
dit «&#8239;Fat&nbsp;Face&#8239;».

</div>

<figure>
  <img src="1821-Robert_Thorne-fat-face-types.png" alt="">
  <figcaption>↑ <em lang="en">New Specimen of Printing Types, Late R. Thorne’s</em>, William Thorowgood, 1821</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1816 🏴󠁧󠁢󠁥󠁮󠁧󠁿

**William Caslon IV** dessine le **premier caractère sans-serif**. 

<p class="text-sm">Intitulé « Two Lines English Egyptian », il est composé uniquement de capitales.</p>

</div>

<figure>
  <img src="1816-Caslon_Egyptian.jpg" alt="">
  <img src="caslon-sans-mini.jpg" alt="">
</figure>

--

## Essor industriel

<p class="subtitle">XIX<sup>e</sup></p>

--

<!-- .slide: class="col-2" -->

<div>

## Mécanes

### 1845 🏴󠁧󠁢󠁥󠁮󠁧󠁿

L’Anglais **Robert Besley** (1794–1876) dessine le **Clarendon** pour **Fann Street Foundry**. 

<p class="text-sm">C’est un caractère <strong>slab gras et étroitisé</strong> destiné à complété pour le titrage les caractères romains traditionnels. Son dessin simplifié et solide le prédestinait à l’impression de journaux. C’est aussi le premier caractère à bénéficier d’un copyright.</p>

</div>

<figure>
  <img src="1845-Clarendon-FannStreetBesley.jpg" alt="">
  <img src="1874-Fann_Street_Foundry_Clarendon_image_with_text_for_emphasis.jpg" alt="">
  <figcaption>↑ Le Clarendon dans des specimen de Fann Street Foundry de 1845 et 1874.</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1867 🇺🇸

Mise au point aux USA d’un prototype artisanal considéré comme l’**ancêtre de la machine à écrire moderne** par l’imprimeur **Christopher Latham Sholes**.

<p class="text-sm">Il est également l’inventeur du clavier QWERTY.</p>

</div>

<figure>
  <img src="1867-The_Sholes_and_Glidden.jpg" alt="">
  <figcaption>↑ <a href="https://blog.nms.ac.uk/2018/08/11/re-typing-history-the-sholes-glidden-typewriter-and-the-qwerty-keyboard/">The Sholes-Glidden Typewritter</a>, aussi connue sous le nom de «Remington No.1», 1867</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1885 🇺🇸

L’Allemand **Ottmar Mergenthaler** (1854–1899) révolutionne le domaine de l’impression en inventant la **Linotype**.

<p class="text-sm">La Linotype révolutionna l’édition en permettant à de petits ateliers de saisir des textes importants dans des délais raccourcis, et rendit possible l’énorme développement, autour de 1900, de la presse quotidienne en lui offrant une réactivité impossible auparavant.</p>

</div>

<figure>
  <img src="1891-Linotype-affiche.jpg" alt="" width="400">
  <figcaption>↑ <a href="https://fr.wikipedia.org/wiki/1885_en_science#/media/Fichier:Linotype_advert_in_the_British_Printer.tiff">Affiche pour la Linotype</a>, 1891</figcaption>
</figure>

--

<!-- .slide: data-background="#000" data-background-transition="slide" -->

<iframe src="https://player.vimeo.com/video/15032988?h=bb3f4a969e&portrait=0" width="640" height="540" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="background: #000"></iframe>

--

## XX<sup>e</sup> siècle

<p class="subtitle">Première moitié</p>

--

<!-- .slide: class="col-2" -->

<div>

### 1921 🇫🇷

Francis Thibaudeau publie sa **classification Thibaudeau**. 

<p class="text-sm">Elle classe les polices de caractères en 4 grandes familles, rassemblées selon la forme des empattements : <strong>elzévirs</strong> (triangulaire), <strong>didots</strong> (trait fin horizontal), <strong>égyptiennes</strong> (rectangulaire) et <strong>antiques</strong> (sans empattement).</p>

</div>

<figure>
  <img src="1921-Thibaudeau.jpg" alt="">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1928 🏴󠁧󠁢󠁥󠁮󠁧󠁿

**Eric Gill** (1882–1940) 
dessine le **Gill Sans**.

<p class="text-sm">Il s’inspire du caractère dessiné par <strong>Edward Johnston</strong> en 1916 pour le métro de Londres. <br>Comme le Caslon et le Baskerville, les majuscules prennent modèle sur les capitales romaines, qu’on observe par exemple sur la colonne Trajane. </p>

</div>

<figure>
  <img src="1928-gill-sans.png" alt="">
  <img src="West_Brompton.jpg" alt="" width="320">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1927 🇩🇪

**Paul Renner** (1878-1956) 
publie le **Futura** pour 
Bauer Type Foundry.

</div>

<figure>
  <img src="1927-Paul-renner-futura-specimen.jpg" alt="">
</figure>

--

<!-- .slide: data-background-color="#fff" class="text-center" data-background-transition="slide" --> 

<figure style="padding-top: 2rem">
  <img src="Futura.gif" alt="">
  <figcaption style="margin-top: 0rem">Futura, Paul Renner, Bauer Type Foundry, 1927</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1928 🇩🇪

**Jan Tschichold** publie 
***La Nouvelle Typographie*** 
_(Die Neue Typographie)_. 

<p class="text-sm">Son sous-titre « Un manuel pour des créateurs de leur temps » annonce l’ambition de ce manifeste : servir de guide pratique à l’industrie du livre alors en pleine industrialisation. Ce livre emblématique marquera l’histoire du graphisme et de l’édition du XX<sup>e</sup> siècle.</p>

</div>

<figure>
  <img src="1928-tschichold-publicity-leaflet-for-die-neue-typographie.jpg" alt="" width="360">
  <figcaption>↑ Dépliant publicitaire pour <br><em>Die Neue Typographie</em>, 1928</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1944 🇫🇷

Invention de la **photo-composition** : un procédé de composition de lignes de texte par un principe photographique.

<p class="text-sm">La première machine opérationnelle, la <strong>Lumitype</strong>, <br>fut mise au point par les ingénieurs Louis Moyroud, René Higonnet et René Gréa à partir de 1944. <br>Le procédé se généralisa dans les années 1960 <br>et fut remplacé par l’informatique à la fin des <br>années 1970.</p>

</div>

<figure>
  <img src="Lumitype_550.jpg" alt="">
  <figcaption>↑ Lumitype 550-PR076, années 1960</figcaption>
</figure>

--

<!-- .slide: data-background="#000" data-background-transition="slide" -->

<iframe width="720" height="480" src="https://www.youtube-nocookie.com/embed/vCuLxQl-HrY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="background: #000"></iframe>

--

<!-- .slide: data-background="#fff" data-background-transition="slide"  -->

## XX<sup>e</sup> siècle

<p class="subtitle">Seconde moitié</p>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide"  -->

<div>

### 1954 🇫🇷

**Maximilien Vox** publie sa classification en s’appuyant sur celle établie par Thibaudeau.

<p class="text-sm">Mais il trouve que celle-ci, basée sur la forme des empattements, est quelque peu limitée et il s’emploie à créer une nouvelle classification <strong>davantage basée sur l’histoire et les grandes familles de caractères</strong>. Cette classification tend à regrouper les polices de caractères selon de grandes tendances, souvent typiques d’une époque et ce en s’appuyant sur un certain nombre de critères : pleins et déliés, formes des empattements, axe d’inclinaison, taille de l’œil…</p>

</div>

<figure>
  <img src="1954-classification-maximilien-vox.jpg" alt="">
</figure>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide"  -->

<div>

### 1957 🇨🇭

**Adrien Frutiger** (1928–2015) dessine l’**Univers** chez **Deberny & Peignot** pour les machines à écrire de marque IBM.

</div>

<figure>
  <img src="1957-Specimen_Univers_Frutiger.jpg" alt="">
</figure>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide"  -->

<div>

### 1970 🇺🇸

**Herb Lubalin** & Tom Carnase dessinent l’**Avant Garde Gothic** à partir du logo du magazine du même nom.

</div>

<figure>
  <img src="1970-Avant-Garde-Gothic.jpg" alt="">
</figure>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide"  -->

<div>

### 1984 🇺🇸

Apple sort son premier **Macintosh**, fourni avec 
9 polices sytème conçues 
par **Susan Kare**.

</div>

<figure>
  <img src="1984-macintosh.jpg" alt="">
  <img src="1984-Original_Mac_fonts.png" alt="">
</figure>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide"  -->

<div>

### 1995 🇺🇸

**Mattew Carter** publie le **Walker** pour le Walker Art Center, première famille modulaires proposant différents types d’empattements.

</div>

<figure>
  <img src="1995-MatthewCarter-walker.jpg" alt="">
</figure>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide"  -->

<div>

### 1999 🇺🇸

Adobe publie la première version de **InDesign**, successeur de PageMaker inspiré à la fois par PageMaker et XPress.

<p class="text-sm">Il a été le premier logiciel de PAO à offrir l’utilisation de l’Unicode pour le traitement de texte, la typographie avancée avec le support de polices OpenType, des fonctions de transparence avancées, des styles de mise en page et l’alignement optique des marges.</p>

</div>

<figure>
  <img src="1999-adobe-indesign.jpg" alt="">
</figure>

--

<!-- .slide: data-background="#000" data-background-transition="slide" -->

<iframe width="720" height="480" src="https://www.youtube-nocookie.com/embed/aeVs1KEFT5A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="background: #000"></iframe>

--

<!-- .slide: data-background-transition="slide" data-background="#fff" -->

## XXI<sup>e</sup> siècle

<p class="subtitle">Des années 2000 à aujourd’hui</p>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide" -->

<div>

### 2001

Adobe et Microsoft introduisent le format **OpenType**.

<p class="text-sm">Il succède aux formats TrueType (développé par Apple et Microsoft) et PostScript Type 1 (Adobe).</p>

</div>

<figure>
  <img src="2001-opentype-font-otf-ttf.png" alt="">
</figure>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide" -->

<div>

### 2010

Lancement par Goggle de **Google Font Directory**, 
qui deviendra par la suite Google Fonts.

<p class="text-sm">Seulement 18 polices de caractères figurent au <br>sein du catalogue initial (contre plus de 1300 aujourd’hui).</p>

</div>

<figure>
  <img src="2010-google-fonts.png" alt="">
  <figcaption>↑ <a href="https://web.archive.org/web/20100522110453/code.google.com/webfonts">Google Font Directory</a>, 2010</figcaption>
</figure>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide" -->

<div>

### 2017

Prototypo publie le **Spectral** 
en partenariat avec Production Type, **première Google Font paramétrique**.

<p class="text-sm">Le Spectral est principalement destiné à être utilisé au sein des applications de la suite bureautique collaborative de Google.</p>

</div>

<figure>
  <img src="2017-Spectral.png" alt="">
  <video loop="1" autoplay="1" preload="metadata" controls="controls">
    <source type="video/mp4" src="spectral.mp4">
    </video>
  <figcaption>↑ <a href="https://www.prototypo.io/blog/news/introducing-spectral-the-first-parametric-google-font-by-prototypo/">Introducing Spectral, the first parametric Google font by Prototypo</a></figcaption>
</figure>

--

<!-- .slide: class="col-2" data-background="#fff" data-background-transition="slide"  -->

<div>

### 2016

Co-annonce de Google, Microsoft, Apple et Adobe du format [OpenType variables fonts](https://medium.com/variable-fonts/https-medium-com-tiro-introducing-opentype-variable-fonts-12ba6cd2369).

</div>

<figure>
  <img src="2016-variable-fonts.png" alt="">
  <figcaption>↑ Représentation spaciale d’une fonte variable à 3 axes</em>.</figcaption>
</figure>

---

<p class="subtitle">Observer et décrire</p>

# La lettre

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

## Anatomie

<img src="anatomie.jpg" alt="">

--

<!-- .slide: class="top-0 text-center" -->

## Ligne de base et hauteur d’x

<hr style="position: absolute; top: 288px; left: 0; right: 0">

<div class="r-stack">
<p class="h1 text-center w-full fragment current-visible" style="height: 5rem; margin: 12rem 0;"><span style="font-family: Helvetica">3 polices,</span> <span style="font-family: var(--r-serif-font)">1 corps,</span> <span style="font-family: Impact"> 3 hauteurs d’x</span></span></p>

<p class="h1 text-center w-full fragment current-visible" style="height: 5rem; margin: 12rem 0;"><span style="font-family: Helvetica">3 polices,</span> <span style="font-family: var(--r-serif-font); font-size: 121%">3 corps,</span> <span style="font-family: Impact; font-size: 82%"> 1 hauteur d’x</span></span></p>

<p class="h1 text-center w-full fragment" style="height: 5rem; margin: 12rem 0; text-transform: uppercase;"><span style="font-family: Helvetica; font-size: 74%">3 polices,</span> <span style="font-family: var(--r-serif-font); font-size: 80%">3 corps,</span> <span style="font-family: Impact; font-size: 65%"> 1 hauteur d’x</span></span></p>
</div>

<hr style="position: absolute; top: 319px; left: 0; right: 0">

--

## Comment décrire <br>une police de caractères&#8239;?

--

> Il me faudrait une **linéale géométrique** pour du **titrage**, un peu genre **Futura**, mais avec un plus gros **œil** et un peu plus **chaleureuse**…

<p class="h1 text-center" style="color: var(--r-link-color)">🧐</p>

--

## Propriétés formelles
<p class="subtitle">(objectives)</p>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Hauteur (corps)

<div class="col-2" style="justify-content: space-around; flex-gap:10rem; gap:10rem; margin: 5rem 0 0">

<table>
  <thead>
    <tr>
      <th>point</th>
      <th>pica</th>
      <th>pouce</th>
      <th>mm</th>
    </tr>
  </thead>
  <tbody>
    <tr class="mono">
      <td>1</td>
      <td>1/12</td>
      <td>1/72</td>
      <td>0,35</td>
    </tr>
    <tr class="mono">
      <td>12</td>
      <td>1</td>
      <td>1/6</td>
      <td>4,25</td>
    </tr>
    <tr class="mono">
      <td>72</td>
      <td>6</td>
      <td>1</td>
      <td>254</td>
    </tr>
  </tbody>
</table>

<p style="display:inline-block;font-family:var(--r-serif-font); font-size: 100pt; line-height: 1; border-top: 1px dashed; border-bottom: 1px dashed; background-size: 10pt 10pt;background-image: repeating-linear-gradient(0deg, rgba(23, 47, 51, 0.333), rgba(23, 47, 51, 0.333) 1px, #fff 1px, #fff); background-repeat: repeat-y; max-height: 100.5pt">Typo</p>

</div>

<figure>
  <img src="corps.png" alt="" width="400">
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Largeur (chasse)

<div class="col-2" style="justify-content: space-between; flex-gap:10rem; gap:10rem; margin: 2rem 0">

<figure class="text-left">
  <img src="chasse-awt.jpg" alt="" width="343">
  <figcaption style="margin-top: 0.5rem">American wood type, <br>Rob Roy Kelly, 1880s</figcaption>
</figure>

<figure class="text-left">
  <img src="proportional-monospaced.png" alt="">
  <figcaption style="margin-top: 0.5rem"><span style="font-family:var(--r-main-font);">Chasse variable (proportionnelle)</span> <br>Chasse fixe (monochasse)</figcaption>
</figure>

</div>

<figure style="margin-top: -4rem; z-index: -1; position: relative;">
  <img src="chasse.png" alt="" width="400">
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Poids (graisse)

<p style="display: flex; width: 100%; max-width: 100%; justify-content: space-between; font-size: 10rem; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI'; margin-top: 7.5rem; color: var(--r-background-color">
  <span style="width:calc(100%/9); font-weight: 100">a</span>
  <span style="width:calc(100%/9); font-weight: 200">a</span>
  <span style="width:calc(100%/9); font-weight: 300">a</span>
  <span style="width:calc(100%/9); font-weight: 400">a</span>
  <span style="width:calc(100%/9); font-weight: 500">a</span>
  <span style="width:calc(100%/9); font-weight: 600">a</span>
  <span style="width:calc(100%/9); font-weight: 700">a</span>
  <span style="width:calc(100%/9); font-weight: 800">a</span>
  <span style="width:calc(100%/9); font-weight: 900">a</span>
</p>

<p class="mono" style="display: flex; width: 100%; max-width: 100%; justify-content: space-between; margin:0; font-size: .875rem; letter-spacing: .02em">
  <span style="width:calc(100%/9)">100 <br>Ultralight</span>
  <span style="width:calc(100%/9)">200 <br>Thin</span>
  <span style="width:calc(100%/9)">300 <br>Light</span>
  <span style="width:calc(100%/9)">400 <br>Regular</span>
  <span style="width:calc(100%/9)">500 <br>Medium</span>
  <span style="width:calc(100%/9)">600 <br>Semibold</span>
  <span style="width:calc(100%/9)">700 <br>Bold</span>
  <span style="width:calc(100%/9)">800 <br>Heavy</span>
  <span style="width:calc(100%/9)">900 <br>Black</span>
</p>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Style : italique

<!--
<figure style="margin-top: 7rem">
  <img src="italic-faux-italic-MinionPro.png" alt="" width="470">
  <img src="italic-faux-italic-Noto.png" alt="" width="470">
  <figcaption>Vrai italique vs faux italique</figcaption>
</figure>-->

<figure>
  <img src="italic.svg" alt="" style="margin-top: -4rem">
  <figcaption style="margin:-1rem auto 0; max-width: 39em">Les italiques sont presque toujours inclinés et sont conçu pour mettre l’accent sur le texte par contraste avec le romain. <br>Ils possèdent une structure différente le plus souvent basée sur des formes d’écriture cursives.</figcaption>
</figure>


--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Style : faux italique

<figure>
  <img src="faux-italic.svg" alt="" style="margin-top: -2rem">
  <figcaption style="margin:-1rem auto 0; max-width: 37.5em">Romain, faux italique, (vrai) italique.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Style : oblique

<figure>
  <img src="oblique.svg" alt="" style="margin-top: -2rem">
  <figcaption style="margin:-4rem auto 0; max-width: 37.5em">Les obliques partagent la même strucuture que le romain (contrairement aux italiques). Ils ne sont pas simplement inclinés: des corrections optiques sont apportées pour éviter les distorsions et une mauvaise répartition du poids.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Angle

<figure style="margin-top:3rem">
  <img src="angle-italic-Garamond-16.png" alt="">
  <figcaption style="margin-top:0">Garamond Italic 16°</figcaption>
</figure>

<figure style="margin-top:3rem">
  <img src="angle-italic-Minion-Pro-12.png" alt="">
  <figcaption style="margin-top:0">Minion Pro Italic 12°</figcaption>
</figure>

<figure style="margin-top:3rem">
  <img src="angle-italic-FF-Seria-0.png" alt="">
  <figcaption style="margin-top:0">FF Seria Italic 0°</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Contraste

<div class="col-2">

<figure class="my-0 relative">
  <img src="contraste-translation.png" alt="" width="400">
  <img src="contraste-expansion.png" alt="" width="400">
  <img src="contraste-rotation.png" alt="" width="400">
  <figcaption style="display: flex; flex-direction: column; justify-content: space-around; position: absolute; top: 0; left: 3rem; bottom: 0; margin:0; transform: translateY(4.52rem);">
    <span>Translation</span> 
    <span>Expansion</span>
    <span>Rotation</span>
  </figcaption>
</figure>

<figure class="my-0 relative">
  <img class="my-0" src="cube-Gerrit-Noordzij.png" alt="" width="400">
  <figcaption style="position: absolute; bottom:-1.9rem; left:0">Cube de <br>Gerrit Noordzig</figcaption>
</figure>

</div>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Corps optiques

<figure class="my-0 relative" style="margin-top: 2rem">
  <img src="optical-sizes.svg" alt="" width="720">
  <figcaption style="max-width: 45em; margin: 2rem auto 0;">
    Les corps optiques (optical sizes) sont différentes versions d’une police de caractères optimisées pour une utilisation à des tailles spécifiques: en général, plus le corps est petit, moins le contraste est important, plus les formes sont ouvertes et plus la hauteur d’x est grande.
  </figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" data-background-image="ArrowType-NameSans-optical-size.png" data-background-size="contain" class="bg-top" -->

<a href="https://name.arrowtype.com/usage.pdf" target="_blank" class="fixed inset-0" title="How to use Name Sans, Arrow Type (PDF)"></a>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Super famille

<figure style="margin-top:-2.5rem">
  <img src="univers-fonts.jpg" alt="" width="270" style="margin-right: 2rem; margin-left:-1rem">
  <img src="univers.jpg" alt="" width="600">
  <figcaption style="position:absolute; left: 78%; right:0; bottom:2rem; text-align: left">Les 27 styles <br>de la famille Univers, Adrian Frutiger, Deberny & Peignot, 1957.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Fontes variables

<iframe allowfullscreen="allowfullscreen" src="https://player.vimeo.com/video/572060744?autoplay=0" width="854" height="480" frameborder="0"></iframe>

--

<!-- .slide: data-background-color="#fff" data-background-image="v-fonts.png" class="bg-top" -->

<a href="https://v-fonts.com/" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-visibility="hidden" -->

<form class="flex mb-4" oninput="outputWeight.value=rangeOutput('weight')">
  <label for="weight">Graisse</label>
  <div class="range">
    <output class="mono" name="outputWeight">regular</output>
    <input type="range" list="weights" id="weight" name="weight" min="100" max="900" step="100" value="400">
    <datalist id="weights">
      <option value="100" label="thin (hairline)">
      <option value="200" label="extra-light">
      <option value="300" label="light">
      <option value="400" label="regular">
      <option value="500" label="medium">
      <option value="600" label="semi-bold">
      <option value="700" label="bold">
      <option value="800" label="extra-bold">
      <option value="900" label="black (heavy)">
    </datalist>
  </div>
</form>

<form class="flex mb-4" oninput="outputWidth.value=rangeOutput('width')">
  <label for="width">Chasse</label>
  <div class="range">
    <output class="mono" name="outputWidth">normal</output>
    <input type="range" list="widths" id="width" name="width" min="100" max="900" step="100">
    <datalist id="widths">
      <option value="100" label="ultra-condensed">
      <option value="200" label="extra-condensed">
      <option value="300" label="semi-condensed">
      <option value="400" label="condensed">
      <option value="500" label="normal">
      <option value="600" label="semi-expanded">
      <option value="700" label="expanded">
      <option value="800" label="extra-expanded">
      <option value="900" label="ultra-expanded">
    </datalist>
  </div>
</form>

<form class="flex mb-4" oninput="outputWidth.value=rangeOutput('slant')">
  <label for="slant">Angle</label>
  <div class="range">
    <output class="mono" name="outputWidth">0°</output>
    <input type="range" id="slant" name="slant" min="-3" max="3">
    <datalist id="slants">
      <option value="-3" label="-15°">
      <option value="-2" label="-10°">
      <option value="-1" label="-5°">
      <option value="0" label="0°">
      <option value="1" label="5°">
      <option value="2" label="10°">
      <option value="3" label="15°">
    </datalist>
  </div>
</div>

<div class="flex mb-4">
  <label for="contrast">Contraste</label>
  <div class="range">
    <input type="range" id="contrast" name="contrast" min="-5" max="5">
  </div>
</div>

<div class="flex mb-4">
  <label for="optical-size">Corps optique</label>
  <div class="range">
    <input type="range" id="optical-size" name="optical-size" min="1" max="5">
  </div>
</div>

<div class="flex mb-4">
  <label for="x-height">Hauteur d’x</label>
  <div class="range">
    <input type="range" id="x-height" name="x-height" min="1" max="5">
  </div>
</div>

--

## Classifications
<p class="subtitle">(subjectives)</p>

--

> **_Organiser_ des informations en _catégories_ en fonction de certains _critères_.**
Déterminer les _classes_ dans lesquelles les éléments pourront être classés.

--

<!-- .slide: data-background-color="#fff" class="text-center top-0" --> 


### Thibaudeau (1921)

<figure>
  <img src="Thibaudeau-bdc.png" alt="" width="320">
  <img src="Thibaudeau-cap.png" alt="" width="320">
  <figcaption>En 1921, Francis Thibaudeau publie «La lettre d’imprimerie». Il y présente et expose sa classification basée sur la présence et de la forme des empattements.
</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="text-center top-0" --> 


### Vox-ATypI (1954 / 1962)

<div class="text-sm" style="position:absolute;left:0;top:6rem;bottom:0;width:140px;text-align:left;">
  <p style="padding:2.125rem 0;margin:1rem 0;border-right: 1px dashed">Caractères d’inspiration <strong>classique</strong></p>
  <p style="padding:3.5rem 0;margin:1rem 0;border-right: 1px dashed">Caractères d’inspiration <strong>moderne</strong></p>
  <p style="padding:3.5rem 0;margin:1rem 0;border-right: 1px dashed">Caractères d’inspiration <strong>calligraphique</strong></p>
</div>

<figure style="display:flex; justify-content:flex-end">
  <img src="vox-atypi.png" alt="" width="210" style="margin-right: 320px">
  <img src="vox-atypi-exemples.png" alt="" width="280">
</figure>

<div style="position:absolute;left:370px;top:6.66rem;width:290px;font-size:.875rem;text-align:left">
  <p style="margin:.75rem 0">Inspiré de la lettre romaine rénovée à la Renaissance.</p>
  <p style="margin:.75rem 0">Rappelle la forme des créations classiques (Renaissance & Baroque).</p>
  <p style="margin:.75rem 0">Caractères de labeur dont le dessin rappelle les types les plus utilisés au XVIII<sup>e</sup> siècle.</p>
  <p style="margin:.75rem 0">Caractères qui évoquent la typographie pure du début du XIX<sup>e</sup> siècle.</p>
  <p style="margin:.75rem 0">Caractères qui, par leurs formes géométriques, évoquent l’ère de la mécanique.</p>
  <p style="margin:.75rem 0">Caractères sans empattement de la typographie moderne.</p>
  <p style="margin:.75rem 0">Inspiré des inscriptions monumentales de l’antiquité (terminales élargies).</p>
  <p style="margin:.75rem 0">Caractères qui imitent l’écriture courante ou la calligraphie.</p>
  <p style="margin:.75rem 0">Inspiré des lettres dessinées au moyen âge (avant l’imprimerie).</p>
  <p style="margin:.75rem 0">Lettres gothiques utilisées par Gutemberg. <br>Les pleins et les déliés sont très contrastés.
  <p style="margin:.75rem 0">Caractères non-latins (grec, hébreu, arabe, etc.)</p>
</div>

--

<!-- .slide: data-background-color="#fff" data-background-image="fontshop.png" class="bg-top" -->

<a href="https://www.fontshop.com/families" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#232323" data-background-image="hoeflerandco.png" class="bg-top" -->

<a href="https://www.fontshop.com/families" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background-image="adobe-fonts.png" class="bg-top" -->

<a href="https://fonts.adobe.com/fonts" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" class="bg-top text-center" -->

### Meta-classification

<figure style="margin-top: 5rem">
  <img src="classification-25_systems.png" alt="" width="440" style="margin-right: 2rem">
  <img src="classification-results.png" alt="" width="400">
  <figcaption style="margin-top: 3rem"><a href="ParsonsJournalForInformationMapping-Childers+Griscti+Leben-2013.pdf"><em>25 Systems for Classifying Typography: A Study in Naming Frequency</em></a> <br>Taylor Childers, Jessica Griscti, Liberty Leben, 2013</figcaption>
</figure>

--

## Connotations
<p class="subtitle">(collectives)</p>

--

> **_Sens particulier_ qui vient _s’ajouter_ au sens ordinaire, en fonction du _contexte_.**
(Par extension) _Image_, _représentation_ qu’_évoque_ un mot, une idée.

--

<!-- .slide: data-background-color="#fff" data-background-image="adobe-fonts-mots-cles.png" class="bg-top" -->

<a href="https://fonts.adobe.com/fonts" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background-image="myfonts-tags.png" class="bg-top" -->

<a href="https://www.myfonts.com/tags/" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: class="top-0" -->

### Connotations applicatives

- **📕 presse, magazine**
- **🗺️ cartographie**
- **➡️ signalétique**
- **📊 graphiques et tableaux**
- **📖 littérature**
- **💼 corporate**
- **🎁 branding, packaging**
- **👁️‍🗨️ publicité, commercial**
- …

--

<!-- .slide: data-background-color="#fff" class="text-center" -->

<figure>
  <img src="magazines-feminins.jpg" alt="">
</figure>

--

<!-- .slide: class="top-0" -->

### Connotations culturelles

- **🎥 cinéma** <small>affiches, génériques, genres, accessoires</small>
- **📺 TV** <small>habillages, génériques, publicités</small>
- **📚 édition** <small>livres, BD, mangas, magazines</small>
- **💽 musique** <small>logos, affiches, pochettes de disques, clips</small>
- **🏀 sport** <small>logos, produits dérivés</small>
- **🏢 architecture** <small>enseignes, devantures, vitrines</small>
- **👔 mode** <small>logos, étiquettes, imprimés</small>
- **🖋️ design graphique** <small>affiches, logos, packaging</small>
- **🛋️ design produit** <small>véhicules, objets en tous genres</small>
- **📱 numérique** <small>interfaces, applications, sites web</small>

--

<!-- .slide: data-background-color="#000" data-background-image="book-covers.png" -->

<a href="https://www.typenetwork.com/news/article/10-great-book-covers-using-tn-fonts" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#000" data-background-image="movie-posters.jpg" -->

<a href="https://www.typenetwork.com/news/article/10-great-movie-posters-using-tn-fonts" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background-image="fonts-identified-collage.jpg" -->

<a href="https://www.typewolf.com/blog/fonts-in-popular-culture-identified-vol-3" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#000" data-background-image="trajan-movie-titles.jpg" data-background-size="contain" class="bg-top" -->

--

<!-- .slide: class="top-0" -->

### Connotations historiques <br>& politiques

<div style="margin-top:6rem">

- **🏛️ périodes historiques** <small>Antiquité, Moyen-Âge, Renaissance…</small>
- **👑 siècles** <small>… XV, XVI, XVII, XVIII, XIX, XX, XXI…</small>
- **🗓️ décennies** <small>10s, 20s, 30s, 40s, 50s, 60s, 70s, 80s, 90s, 00s…</small>
- **🎨 mouvements artistiques** <br><small>baroque, classique, rococo, néoclassique, romantique, art-nouveau, constructiviste, impressionniste, futuriste, pop, moderne, post-moderne, punk, hip-hop, hardcore…</small>
- **✊ mouvements et événements politiques** <br><small>logos, affiches, campagnes présidentielles…</small>

</div>

--

<!-- .slide: data-background-color="#fff" data-background-image="campaign-colors-1968-2020.png" class="bg-top" -->

<a href="https://pudding.cool/2020/08/campaign-colors/"></a>

--

<!-- .slide: class="top-0" -->

### Connotations <br>géographiques

<div style="margin-top: 10rem">

- **🌎 région du monde**
- **🇺🇸 pays**
- **🏙️ ville**
- **🗽 quartier**

</div>

--

### Connotations diverses

<table>
  <thead>
    <tr>
      <th scope="col">Apparence / Forme</th>
      <th scope="col">Personnalité</th>
      <th scope="col">Sentiments</th>
      <th scope="col">Sens</th>
      <th></th>
    </tr>
  </thead>
  <tbody style="vertical-align: top">
    <tr>
      <td>accueillant, adorable, attrayant, confiant, costaud, dynamique, élégant, gai, gentil, magnifique, parfait, plaisant, ravissant, sauvage, séduisant, sexy, svelte, tendu, timide, vif</td>
      <td>amusant, brave, brillant, chaleureux, combatif, déterminé, dominant, doué, entraînant, farfelu, franc, généreux, harmonieux, intrépide, loufoque, mystérieux, plaisant, sage, sincère, spirituel</td>
      <td>calme, coquin, fidèle, fier, heureux, jovial, mature, rieur, sûr de soi</td>
      <td>moelleux, solide, souple, tendre</td>
      <td scope="row" style="font-size: 1.9rem">🙂</td>
    </tr>
    <tr>
      <td>circulaire, compact, creux, décomposé, droit, élancé, géométrique, maigre, plat, primitif, raide, rond, simple, tordu, triangulaire, vaste</td>
      <td>calme, instinctif, serein</td>
      <td>prudent, timide, tranquille</td>
      <td>brut, clair, doux, dur, ferme, léger, mat, mou, perçant, piquant, pointu, tonnant, tranchant</td>
      <td scope="row" style="font-size: 1.9rem">😐</td>
    </tr>
    <tr>
      <td>abîmé, hideux, hirsute, maladroit, nerveux, sale, sombre, trompeur</td>
      <td>agressif, bourru, dangereux, débile, effrayant, égoïste, fatigant, fourbe, jaloux, lâche, méchant, snob</td>
      <td>déprimant, dérangeant, triste, troublé, mélancolique</td>
      <td>beuglant, bruyant, fade, grinçant, strident</td>
      <td scope="row" style="font-size: 1.9rem">🙁</td>
    </tr>
  </tbody>
</table>

--

<!-- .slide: data-background-color="#fff" data-background="fontsinuse-filters.png" data-background-size="contain" class="bg-top" -->

<a href="https://fontsinuse.com" target="_blank" class="fixed inset-0"></a>

--

<p class="subtitle">Exercice</p>

## Que connotent ces fontes&#8239;?

<p class="h1" style="color: var(--r-link-color)">💬</p>

--

<!-- .slide: data-background-color="#fff" class="text-center" --> 

<figure style="padding-top: 2rem">
  <img src="GillSans.gif" alt="">
  <figcaption style="margin-top: 0rem">Gill Sans Regular</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="text-center" -->

<figure style="padding-top: 2rem">
  <img src="Optima.gif" alt="">
  <figcaption style="margin-top: 0rem">Optima Regular</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="text-center" -->

<figure style="padding-top: 2rem">
  <img src="GothamBold.gif" alt="">
  <figcaption style="margin-top: 0rem">Gotham Bold</figcaption>
</figure>

--

<!-- .slide: data-background-color="#000" data-background-iframe="https://www.dailymotion.com/embed/video/x284bw3" data-background-size="contain" -->

---

<p class="subtitle">Fonts pairing & typesetting</p>

# Le texte

--

## Fonts pairing

<p class="subtitle">Comment associer des polices de caractères&#8239;?</p>

--

La selection de plusieurs polices de caractères devant cohabiter peut se faire selon **différents critères** :

- **propriétés formelles** : empattements, hauteur d’x, chasse, graisse, contraste, angle, style…
- **propriétés techniques** : support de certaines langues (glyphset), fonctionnalités OpenType, license…
- **connotations** : applicatives, culturelles, historiques, politiques, géographique…
- **contexte de création** : auteur, fonderie, influences, date, pays, revival / original… 
- et bien d’autres encore !

--

<blockquote lang="en">
  <p>Combining typefaces <br>is like making a <em>salad</em>.</p>
  <footer class="mono text-sm">— Ellen Lupton, <a href="http://thinkingwithtype.com/letter/">Thinking with Type</a></footer>
</blockquote>

<p style="color:var(--r-heading-color); font-size: 8rem; position: absolute;bottom: -10rem; right: 10rem">🥗</p>

--

- **Commencez avec un petit nombre d’éléments <br>représentant différentes couleurs, goûts et textures.** 
<p class="text-sm">Recherchez le contraste plutôt que l’harmonie, <br>des différences marquées plutôt que des transitions molles.</p class="text-sm">

- **Donnez à chaque ingrédient un rôle à jouer :**
<small>tomates douces 🍅 concombres croquants 🥒 et le choc piquant d’un anchois occasionnel 🐟</small>

- **Essayez par exemple de mélanger des gros caractères légers avec des petits caractères noirs** pour un entrecroisement de saveurs et de textures contrastées.

--

<!-- .slide: data-background-color="#fff" class="text-center" -->

<figure>
  <img src="mixing-typefaces.png" alt="">
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Hauteur d’x similaire

<figure style="margin-top: 9rem">
  <img src="x-height-Futura-Baskerville.jpg" alt="">
  <figcaption style="margin-top: 6rem">Les polices de caractères avec une hauteur d’x similaire fonctionnent mieux ensemble car elles ont un poids visuel similaire.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

### Super familles

<figure>
  <img src="super-familles.jpg" alt="">
  <figcaption style="margin-top: 0">Les polices de caractères d’une même famille étendue sont conçues <br>spécialement pour fonctionner ensemble (ex: serif + sans-serif).</figcaption>
</figure>

--

<!-- .slide: data-background-color="#F1FBFC" class="top-0 text-center" -->

<figure style="margin-top: 8rem">
  <img src="pairing-typefaces-Typekit-image01.png" alt="">
</figure>

--

<!-- .slide: data-background-color="#FCFDEC" class="top-0 text-center" -->

### Contraste faible

<figure style="margin-top: 10rem" class="r-stack">
  <img src="pairing-typefaces-Typekit-image02.png" alt="">
  <img src="pairing-typefaces-Typekit-image03.png" alt="" class="fragment" data-fragment-index="2">
  <figcaption style="position: absolute; right: 13.5rem; bottom: 0rem" class="fragment" data-fragment-index="2">✅</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fffbf6" class="top-0 text-center" -->

### Contraste plus fort

<figure style="margin-top: 5rem" class="r-stack">
  <img src="pairing-typefaces-Typekit-image04.png" alt="">
  <img src="pairing-typefaces-Typekit-image05.png" alt="" class="fragment" data-fragment-index="2">
  <figcaption style="position: absolute; right: 13.9rem; bottom: 2.1rem" class="fragment" data-fragment-index="2">✅</figcaption>
</figure>

--

<!-- .slide: data-background-color="#ecf9f5" class="top-0 text-center" -->

### Histoire et contexte

<figure style="margin-top: 8rem">
  <img src="pairing-typefaces-Typekit-image06.png" alt="">
</figure>

--

<!-- .slide: data-background-color="#fff" data-background-image="combining-typefaces-chart.jpg" data-background-size="contain" -->

--

### 13 façons de choisir un caractère typographique <br>selon Michael Bierut

1. Parce qu’**il marche**
2. Parce que **son histoire vous plaît**
3. Parce que **vous aimez son nom**
4. À cause de **l’identité de son créateur**
5. Parce qu’**il était déjà là**
6. Parce que **vous y êtes forcé**
7. Parce qu’**il vous rappelle quelque chose**

--

### 13 façons de choisir un caractère typographique <br>selon Michael Bierut

8. Parce qu’**il est beau**
9. Parce qu’**il est moche**
10. Parce qu’**il est banal**
11. Parce qu’**il est spécial**
12. Parce que **vous êtes un adepte**
13. Parce que **ça tombe sous le sens**

<p class="text-sm mono">Source: <a href="https://bureaubrut.com/product/direct-accessible-et-fute/"><em>Direct, accessible et futé</em></a>, Michael Bierut, Bureau Brut Publishing Vol.1</p>

--

### En résumé

- **évitez les associations trop similaires** <small>(des patates avec des frites)</small>
- **assignez un rôle distinct à chaque fonte** <small>(titre, texte, légende, citation…)</small>
- **jouez avec le contraste** au niveau des graisses et des corps
- **restez simple** : 2 familles suffisent la plupart du temps
- **expérimentez** ! Certaines combinaisons inatendues peuvent parfois très bien fonctionner dans un contexte donné 

--

### Guides et articles

- [Just My Type](https://justmytype.co/typekit/) (Typekit)
- [Font Pair](https://www.fontpair.co/) (Google Fonts)
- [Canva’s ultimate guide to font pairing](https://www.canva.com/learn/the-ultimate-guide-to-font-pairing/)
- [Best Practices of Combining Typefaces](https://www.smashingmagazine.com/2010/11/best-practices-of-combining-typefaces/), Smashing Magazine
- [Typographic Doubletakes](https://www.typography.com/blog/typographic-doubletakes), Hoefler&Co.
- [Type Study: pairing Typefaces](https://blog.typekit.com/2012/05/23/type-study-pairing-typefaces/), Typekit
- [Type Pairing Lookbooks](https://www.typewolf.com/lookbooks), Typewolf (payant)

--

## Typesetting

<p class="subtitle">Comment rythmer, composer et structurer un texte&#8239;?</p>

--

## Rythme

**Le rythme en typographie est comme le rythme en musique.**
Un texte peut soit couler comme un chef-d'œuvre symphonique interprété par un orchestre en accord, soit être une chanson décousue et déstructurée. Tout comme en musique, où l’ordre est généralement plus agréable à nos oreilles que le chaos, un texte bien composé avec un rythme établi est plus facile à lire et plus agréable à parcourir par nos yeux.

<small>Les oreilles et les yeux ne sont que des outils sensoriels, c'est notre esprit qui traite ces informations. Et notre esprit est une machine à reconnaître des formes. C’est pourquoi un texte bien réglé, rythmé et proportionné triomphera toujours d'un texte décousu. Contrairement à la musique, il existe deux types de rythme en typographie : horizontal et vertical.</small>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

<figure style="margin-top: 5rem">
  <img src="rhythm.jpg" alt="" width="720">
  <figcaption>Rythme horizontal et vertical en typographie.</figcaption>
</figure>

--

<!-- .slide: class="top-0" -->

### Rythme horizontal : interlettrage (letter-spacing)

<div class="col-3" style="align-items: flex-start">

<div>

**Interlettrage <br>normal**

<p class="text-sm mono" style="margin-top:-1.5rem">0</p>

<p style="font-size: 1rem; line-height: 120%">L’espacement d’un groupe entier de lettres est appelé approche ou interlettrage. Il est assez usuel d’approcher – en fait d’espacer – les capitales et les petites capitales, ce qui leur confère un aspect plus prestigieux. <br>En augmentant légèrement l’approche sur l’ensemble d’un texte, le designer peut donner à celui-ci une apparence plus aérée. L’approche resserrée est rarement convaincante : elle ne devrait être utilisée qu’exeptionnellement, pour ajuster une ou plusieurs lignes d’un texte justifié.</p>

</div>

<div>

**Interlettrage <br>augmenté**

<p class="text-sm mono" style="margin-top:-1.5rem">+0.05em</p>

<p style="font-size: 1rem; letter-spacing: 0.05em">L’espacement d’un groupe entier de lettres est appelé approche ou interlettrage. Il est assez usuel d’approcher – en fait d’espacer – les capitales et les petites capitales, ce qui leur confère un aspect plus prestigieux. <br>En augmentant légèrement l’approche sur l’ensemble d’un texte, le designer peut donner à celui-ci une apparence plus aérée. L’approche resserrée est rarement convaincante : elle ne devrait être utilisée qu’exeptionnellement, pour ajuster une ou plusieurs lignes d’un texte justifié.</p>

</div>

<div>

**Interlettrage <br>resséré**

<p class="text-sm mono" style="margin-top:-1.5rem">-0.05em</p>

<p style="font-size: 1rem; letter-spacing: -0.05em">L’espacement d’un groupe entier de lettres est appelé approche ou interlettrage. Il est assez usuel d’approcher – en fait d’espacer – les capitales et les petites capitales, ce qui leur confère un aspect plus prestigieux. <br>En augmentant légèrement l’approche sur l’ensemble d’un texte, le designer peut donner à celui-ci une apparence plus aérée. L’approche resserrée est rarement convaincante : elle ne devrait être utilisée qu’exeptionnellement, pour ajuster une ou plusieurs lignes d’un texte justifié.</p>

</div>

</div>

--

<!-- .slide: data-background-color="#fff" class="top-0 text-center" -->

<figure style="margin-top: 10rem">
  <img src="letter-spacing-1.jpg" alt="" width="450" style="margin-right: 3rem">
  <img src="letter-spacing-2.jpg" alt="" width="450">
  <figcaption style="margin-top: 5rem; columns:2; column-gap: 3rem; text-align: left;">Réduire légèrement l’interlettrage des titres les rend plus compacts et plus proches de ce à quoi ressemble optiquement le texte de labeur. <br>Augmenter un peu l’interlettrage des textes composés en capitales ou petites capitales améliore leur lisibilité.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0" -->

### Rythme horizontal : crénage (kerning)

<figure style="margin-top: 4rem">
  <img src="kerning-bad.jpg" alt="" width="500" style="margin-right: 3rem">
  <img src="kerning-bad-good.jpg" alt="" width="320">
  <figcaption style="margin-top: 2rem; text-align: left">L’espacement entre les différentes lettres est loin d’être égal. <br>Chaque lettre possède un espacement à gauche et à droite par défaut (l’approche). Le crénage consiste à créer des exceptions d’approches entre certaines paires de lettres peut résoudre des problèmes d’équilibre et de rappartition du vide.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#2b313f" -->

<iframe src="https://type.method.ac/" width="900" height="600"></iframe>

--

<!-- .slide: class="top-0" -->

### Rythme vertical : interlignage

<div class="col-3" style="align-items: flex-start">

<div>

**Interlignage <br>moyen**

<p class="text-sm mono" style="margin-top:-1.5rem">125% (16px/20px)</p>

<p style="font-size: 1rem; line-height: 125%">La distance qui sépare la ligne de pied d’une ligne de texte de la ligne de pied suivante est appelée interlignage. Le paramètre par défaut dans la plupart des logiciels de PAO est de traitement d’image fixe un interlignage légèrement supérieur à la hauteur des lettres capitales. Augmenter l’interlignage permet d’obtenir des blocs de texte moins denses, d’aspect plus ouvert.
Quand l’interlignage augmente, chaque ligne de texte devient un objet visuel indépendant plutôt qu’un élément d’un ensemble homogène.</p>

</div>

<div>

**Interlignage <br>nul**

<p class="text-sm mono" style="margin-top:-1.5rem">100% (16px/16px)</p>

<p style="font-size: 1rem; line-height: 100%">La distance qui sépare la ligne de pied d’une ligne de texte de la ligne de pied suivante est appelée interlignage. Le paramètre par défaut dans la plupart des logiciels de PAO est de traitement d’image fixe un interlignage légèrement supérieur à la hauteur des lettres capitales. Augmenter l’interlignage permet d’obtenir des blocs de texte moins denses, d’aspect plus ouvert.
Quand l’interlignage augmente, chaque ligne de texte devient un objet visuel indépendant plutôt qu’un élément d’un ensemble homogène.</p>

</div>

<div>

**Interlignage <br>augmenté**

<p class="text-sm mono" style="margin-top:-1.5rem">150% (16px/24px)</p>

<p style="font-size: 1rem; line-height: 150%">La distance qui sépare la ligne de pied d’une ligne de texte de la ligne de pied suivante est appelée interlignage. Le paramètre par défaut dans la plupart des logiciels de PAO est de traitement d’image fixe un interlignage légèrement supérieur à la hauteur des lettres capitales. Augmenter l’interlignage permet d’obtenir des blocs de texte moins denses, d’aspect plus ouvert.
Quand l’interlignage augmente, chaque ligne de texte devient un objet visuel indépendant plutôt qu’un élément d’un ensemble homogène.</p>

</div>

</div>

--

<!-- .slide: data-background-color="#fff" class="top-0" -->

### Grille de lignes de base et web

<figure style="margin-top: 3rem">
  <img src="rhythm-vertical.png" alt="" width="740">
  <figcaption style="margin-top: 2rem; text-align: left; max-width: 43em">La hauteur de ligne du h3 est égale à 2 lignes, ses marges sont égales à 3 lignes en haut et 1 ligne en bas. Aligner son texte sur une grille de lignes de base permet de créer un rythme vertical régulier sur lequel les éléments viennent se caler, comme le tempo d’une musique.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0" -->

### Gris typographique (couleur)

<div class="col-2" style="align-items: flex-start; margin-top: 5rem">

<div class="text-left">
  <p class="text-sm mono" style="height: 4rem">Minion Pro <br>16px/24px</p>
  <p style="font-family: var(--r-serif-font); font-size: 16px; line-height: 24px">Le gris typographique est l’impression produite sur l’œil par la vision générale d’un texte ; on parle aussi de couleur du texte. Il ne s’agit pas de sa couleur au sens de la teinte des pigments colorant les caractères, mais au sens de la densité moyenne du gris, résultat optique de la juxtaposition de multiples caractères noirs sur fond blanc (le concept s’étend bien sûr à d’autres couleurs que le noir). Le gris typographique conditionne la première impression qu’un lecteur a d’un texte et surtout l’aisance avec laquelle ce lecteur pourra le lire. Les différents facteurs qui influencent le gris typographique et son homogénéité sont la police (ou « fonte »), le corps (taille du caractère), l’interlignage, la graisse, l’utilisation de capitales, la justification et l’approche entre les caractères (interlettrage).</p>
</div>

<div class="text-left" >
  <p class="text-sm mono" style="height: 4rem">Georgia <br>16px/24px</p>
  <p style="font-family: 'Georgia', serif; font-size: 16px; line-height: 24px; margin-top:-2px;">Le gris typographique est l’impression produite sur l’œil par la vision générale d’un texte ; on parle aussi de couleur du texte. Il ne s’agit pas de sa couleur au sens de la teinte des pigments colorant les caractères, mais au sens de la densité moyenne du gris, résultat optique de la juxtaposition de multiples caractères noirs sur fond blanc (le concept s’étend bien sûr à d’autres couleurs que le noir). Le gris typographique conditionne la première impression qu’un lecteur a d’un texte et surtout l’aisance avec laquelle ce lecteur pourra le lire. Les différents facteurs qui influencent le gris typographique et son homogénéité sont la police (ou « fonte »), le corps (taille du caractère), l’interlignage, la graisse, l’utilisation de capitales, la justification et l’approche entre les caractères (interlettrage).</p>
</div>

</div>

--

<!-- .slide: class="top-0" -->

### Alignements

<div class="col-4" style="align-items: flex-start">

<div>

**Justifié**

<p class="text-sm" style="margin-top:-1rem">Régulier à gauche <br>et à droite</p>

<p style="font-size: 0.75rem; text-align: justify">La disposition du texte en colonnes présentant des bords droits ou irréguliers s’appelle l’alignement. Chaque mode d’alignement engendre autant de qualités esthétiques propres que de problèmes particuliers en ce qui concerne la mise en page. Le mode justifié, qui présente un bord régulier à gauche et à droite, a constitué une norme depuis l’invention de l’imprimerie en caractères mobiles, un technique qui facilitait la réalisation de colonnes parfaitement rectilignes page après page. Un texte justifié permet une utilisation optimale de l’espace et offre également à la page un aspect plus ordonné. Des “trous” hideux peuvent pourtant apparaître si la longueur des lignes est trop réduite par rapport au corps utilisé. Effectuer des césures permet alors de couper les mots trop longs et de conserver l’homogénéité visuelle des lignes. On peut aussi modifier l’interlettrage pour ajuster parfaitement la longueur d’une ligne.</p>

</div>

<div>

**Ferré à gauche**

<p class="text-sm" style="margin-top:-1rem">Régulier à gauche, irrégulier à droite</p>

<p style="font-size: 0.75rem">En <em>fer à gauche</em>, le bord gauche de la colonne de texte est régulier tandis que le bord droit est irrégulier. Les espaces entre les mots sont toujours égaux, donc il n’y a jamais de “trous” à l’intérieur des lignes. Ce mode d’alignement, rarement utilisé avant le XXe siècle, respecte la nature du texte plutôt que de le forcer dans une “boite” déterminée a priori. Malgré ses avantages, le fer à gauche recèle malgré tout quelques dangers. D’abord, le graphiste doit contrôler la silhouette du drapeau, le bord irrégulier formé à droite de la colonne. Un bon “drapeau” doit présenter un profil modérément irrégulier, avec des lignes ni trop longues, ni trop courtes, et aussi peu de césures que possible. Un drapeau est “mauvais” lorsqu’il apparaît trop régulier (ou trop irrégulier) ou quand il prend une silhouette incongrue — escalier, croissant ou promontoire.</p>

</div>

<div>

**Ferré à droite**

<p class="text-sm" style="margin-top:-1rem">Irrégulier à droite, régulier à droite</p>

<p style="font-size: 0.75rem; text-align: right">Le <em>fer à droite</em> est une variante du classique fer à gauche. Les graphiste considèrent généralement que le fer à droite est difficile à lire parce qu’il contraint l’œil du lecteur à retrouver un point d’appui différent au début de chaque ligne. C’est peut-être vrai, mais cela pourrait aussi n’être qu’une idée reçue. En tout état de cause, le fer à droite et rarement employé pour des textes longs. Par contre, il est souvent approprié pour la composition de notes marginales, d’encadrés, de citations extraites du texte, et de n’importe quel élément périphérique se rapportant au texte principal ou à une image. Il est également possible d’associer ou de dissocier visuellement deux ensembles textuels composé l’un en fer à gauche, l’autre en fer à droite : il suffit pour cela soit de disposer leurs drapeaux face-à-face, soit de les rapprocher par la juxtaposition de leur bord régulier.</p>

</div>

<div>

**Centré**

<p class="text-sm" style="margin-top:-1rem">Irrégulier à droite <br>et à gauche</p>

<p style="font-size: 0.75rem; text-align: center">Un texte <em>centré</em> est symétrique, comme la facade d’un monument classique. Le mode centré est souvent employé pour les invitations, les diplômes, les pages de titre et les pierres tombales. Il est possible de conserver des lignes présentant des longueurs extrêmement contrastées. Les coupes de lignes se font souvent dans le but de mettre en valeur un groupe de mots plus important (comme le nom de la marié ou la date de la cérémonie) ou pour permettre l’introduction d’un concept nouveau sur une ligne entièrement nouvelle.</p>

</div>

</div>

--

<!-- .slide: data-background-color="#fff" data-background-image="NYTimes_below_the_fold.jpg" data-background-size="contain" -->

--

### Échelle modulaire 📐

**Une échelle modulaire est un outil pour créer un système de taille de police (et plus largement de rapports et de mesures) logique et harmonieux.**

<p class="text-sm">Utiliser un échelle modulaire est une possibilité, rien ne vous y oblige. <br>Vous pouvez utiliser une échelle pour mesurer ou définir la taille de tout élément ou espace négatif dans une composition, y compris les grilles et les dimensions globales de la composition elle-même.</p>

--

<blockquote>
  <p lang="en">A <em>modular scale</em>, like a musical scale, <br>is a <strong>prearranged set of harmonious proportions</strong>.</p>
  <footer class="mono text-sm">— Robert Bringhurst, <em>The Elements of Typographic Style</em></footer>
</blockquote>

--

<table>
  <thead>
    <tr>
      <th>Interval</th>
      <th>Ratio (fraction)</th>
      <th>Ratio (décimale)</th>
      <th>Base</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Unison</td>
      <td class="mono">1:1</td>
      <td class="mono">1</td>
      <td class="mono">16</td>
    </tr>
    <tr>
      <td>Minor second</td>
      <td class="mono">15:16</td>
      <td class="mono">1.067</td>
      <td class="mono">17.072</td>
    </tr>
    <tr>
      <td>Major second</td>
      <td class="mono">8:9</td>
      <td class="mono">1.125</td>
      <td class="mono">18</td>
    </tr>
    <tr>
      <td>Minor third</td>
      <td class="mono">5:6</td>
      <td class="mono">1.2</td>
      <td class="mono">19.2</td>
    </tr>
    <tr>
      <td>Major third</td>
      <td class="mono">4:5</td>
      <td class="mono">1.25</td>
      <td class="mono">20</td>
    </tr>
    <tr>
      <td>Perfect fourth</td>
      <td class="mono">3:4</td>
      <td class="mono">1.333</td>
      <td class="mono">21.333</td>
    </tr>
    <tr>
      <td>Aug. fourth</td>
      <td class="mono">1:√2</td>
      <td class="mono">1.414</td>
      <td class="mono">22.624</td>
    </tr>
    <tr>
      <td>Perfect fifth</td>
      <td class="mono">2:3</td>
      <td class="mono">1.5</td>
      <td class="mono">24</td>
    </tr>
    <tr>
      <td>Minor sixth</td>
      <td class="mono">5:8</td>
      <td class="mono">1.5</td>
      <td class="mono">25.6</td>
    </tr>
    <tr>
      <td>Golden section</td>
      <td class="mono">1:φ</td>
      <td class="mono">1.618</td>
      <td class="mono">25.888</td>
    </tr>
    <tr>
      <td>Major sicth</td>
      <td class="mono">3:5</td>
      <td class="mono">1.667</td>
      <td class="mono">26.672</td>
    </tr>
    <tr>
      <td>Minor seventh</td>
      <td class="mono">9:16</td>
      <td class="mono">1.778</td>
      <td class="mono">28.448</td>
    </tr>
    <tr>
      <td>Major seventh</td>
      <td class="mono">8:15</td>
      <td class="mono">1.875</td>
      <td class="mono">30</td>
    </tr>
    <tr>
      <td>Octave</td>
      <td class="mono">1:2</td>
      <td class="mono">2</td>
      <td class="mono">32</td>
    </tr>
  </tbody>
</table>

--

<!-- .slide: data-background-color="#fff" data-background="type-scale.com.png" data-background-size="contain" -->

<a href="https://type-scale.com/" target="_blank" class="fixed inset-0">

--

## Hiérarchie

**La hiérarchie typographique est le reflet d’un système d’organisation du texte : elle met en valeur certains éléments par rapport à d’autres.**

<p class="text-sm">C’est une aide pour le lecteur en lui indiquant les points d’entrée et de sortie du texte et en lui offrant la possibilité d’y retrouver des informations ponctuelles.</p>

--

### Plusieurs paramètres (typo)graphiques <br>sont à notre disposition :

<div class="col-3" style="align-items: flex-start"> 

<div>

- police
- corps
- graisse
- style
- casse

</div>

<div>

- interlettrage
- interlignage
- marges
- alinéas
- retours à la ligne

</div>

<div>

- lettrine
- couleurs
- symboles
- filets, soulignés
- effets

</div>

</div>

--

<!-- .slide: data-background-color="#fff" class="text-center top-0" --> 

### Hiérarchie : variations

<figure style="margin-top: 7rem">
  <img src="Thinking_with_Type_Text_34.gif" alt="">
</figure>

--

<!-- .slide: class="top-0" -->

### Longueur de ligne

<div style="border-left: 1px dashed; position: relative;">

<div>
  <div class="text-center text-sm mono heading-color" style="margin-left:.5rem; border-radius: 50%; background: rgba(255, 255, 255, 0.1); width: 2.5rem; height: 2.5rem; padding: .3em">0</div>
</div>

<div style="border-left: 1px dashed; position: absolute;left: 280px;top: 0;bottom: 0">
  <div class="text-center text-sm mono heading-color" style="margin-left:.5rem; border-radius: 50%; background: darkred; width: 2.5rem; height: 2.5rem; padding: .3em">30</div>
</div>

<div style="border-left: 1px dashed; position: absolute;left: 420px;top: 0;bottom: 0">
  <div class="text-center text-sm mono heading-color" style="margin-left:.5rem; border-radius: 50%; background: var(--r-selection-background-color); width: 2.5rem; height: 2.5rem; padding: .3em">45</div>
</div>

<div style="border-left: 1px dashed; position: absolute;left: 560px;top: 0;bottom: 0">
  <div class="text-center text-sm mono heading-color" style="margin-left:.5rem; border-radius: 50%; background: darkgreen; width: 2.5rem; height: 2.5rem; padding: .3em">60</div>
</div>

<div style="border-left: 1px dashed; position: absolute;left: 700px;top: 0;bottom: 0">
  <div class="text-center text-sm mono heading-color" style="margin-left:.5rem; border-radius: 50%; background: var(--r-selection-background-color); width: 2.5rem; height: 2.5rem; padding: .3em">75</div>
</div>

<div style="border-left: 1px dashed; position: absolute;left: 840px;top: 0;bottom: 0">
  <div class="text-center text-sm mono heading-color" style="margin-left:.5rem; border-radius: 50%; background: darkred; width: 2.5rem; height: 2.5rem; padding: .3em">90</div>
</div>

<p style="font-size: 1.3rem; margin: 1.5em 0 0"><strong>Trop court</strong></p>
<p lang="en" style="font-size: 1.333rem; margin-top: 0; max-width: 16em">If a line is too short, the eye will have to travel back too often, breaking the reader’s rhythm.</p>

<p style="font-size: 1.3rem; margin: 1.5em 0 0"><strong>Idéal</strong></p>
<p lang="en" style="font-size: 1.333rem; margin-top: 0; max-width: 30em">The optimal line length for your body text is considered to be 50–60 characters per line, including spaces (<em>Typographie</em>, E.&nbsp;Ruder). Other sources suggest that up to 75 characters is acceptable.</p>

<p style="font-size: 1.3rem; margin: 1.5em 0 0"><strong>Trop long</strong></p>
<p lang="en" style="font-size: 1.333rem; margin-top: 0; max-width: 40em">If a line of text is too long, the user’s eye will have a hard time focusing on the text. This is because the length makes it difficult to get an idea of where the line starts and ends. Furthermore it can be difficult to continue from the correct line in large blocks of text</p>

</div>

<p class="fixed bottom-0" lang="en"><small>Source&#8239;: <em><a href="baymard.com/blog/line-length-readability">Readability: the Optimal Line Length</a></em>, Baymard Institute</p>

--

<!-- .slide: data-background-color="#fff" -->

<iframe src="https://betterwebtype.com/triangle/" width="800" height="600"></iframe>

--

<p class="subtitle">Études de cas</p>

## Fonts pairing & typesetting

<p class="h1" style="color: var(--r-link-color)">🧐</p>

--

<!-- .slide: data-background-color="#f8f8f6" data-background-image="brassneck.png" data-background-size="contain" -->

<!-- Compact LT + Courier -->

<a href="https://brassneck.ca/the-brewery/" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#e8dada" data-background-image="ChateauBoll.jpg" data-background-size="contain" class="bg-top" -->

<!-- Carlton + Trio Grotesk + Domaine Text -->

<a href="https://www.typewolf.com/site-of-the-day/chateau-boll" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#f6e6d9" data-background-image="graza.jpg" class="bg-top" -->

<!-- ITC Garamond + GT Alpina Typewriter -->

<a href="https://www.typewolf.com/site-of-the-day/graza" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#ffc017" data-background-image="medium.png" class="bg-top" -->

<a href="https://medium.com/" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background-image="heckhouse.png" class="bg-top" -->

<a href="https://heckhouse.com/" target="_blank" class="fixed inset-0" title="Portfolio of Bethany Heck"></a>

---

<!-- .slide: data-background-transition="slide"-->

<p class="subtitle">Code, écosystème et économie</p>

## Typographie & web

--

<!-- .slide: data-background-color="#fff" class="top-0" -->

## Unicode & UTF-8

<div class="col-2" style="align-items: flex-start; margin-top: -2rem">

**Unicode** : set de caractères universel et standardisé, utilisé pour traduire des characters en nombres.

**UTF-8** : encodage de caractère utilisé pour traduire des nombres en données binaires.

</div>

<figure>
  <img src="Unicode_BMP.png" alt="" width="315">
  <img src="Unicode_SMP.png" alt="" width="315">
  <img src="Unicode_SIP.svg" alt="" width="315">
  <figcaption style="text-align: left">↑  Représentations graphiques du <a href="https://fr.wikipedia.org/wiki/Unicode#Plan_multilingue_de_base_.28PMB.2C_0000_.C3.A0_FFFF.29">Plan multilingue de base (PMB)</a>, du <a href="https://fr.wikipedia.org/wiki/Unicode#Plan_multilingue_compl.C3.A9mentaire_.28PMC.2C_10000_.C3.A0_1FFFF.29">Plan multilingue complémentaire (PMC)</a> et du <a href="https://fr.wikipedia.org/wiki/Unicode#Plan_id.C3.A9ographique_compl.C3.A9mentaire_.28PIC.2C_20000_.C3.A0_2FFFF.29">Plan idéographique complémentaire (PIC)</a> Unicode (version 9.0). Chaque case numérotée représente 256 points de code.</figcaption>
</figure>

--

<!-- .slide: data-background-color="#fff" class="top-0" -->

## Web fonts : formats

<div class="col-2" style="align-items: flex-start">

<p class="text-sm">
  <strong>Embedded OpenType (EOT)</strong> <br>Format créé en 1998 par Microsoft. Il utilise des techniques de DRM pour empêcher les polices d’être copiées et utilisées sans licence. Un subset simplifié de ce format a été formalisé sous le nom de CWT (Compatibility Web Type ou EOT-Lite). <br>
</p>

<p class="text-sm">
  <strong>Scalable Vector Graphics Fonts (SVG)</strong> <br>SVG fonts est un standard W3C de police utilisant SVG graphic qui est désormais un subset du format OpenType, appelé SVG OpenType Fonts. Il permet notamment d’afficher des polices multicolores et de faire des animations.
</p>

</div>

<div class="col-2" style="align-items: flex-start; margin-top: -1rem">

<p class="text-sm">
  <strong>TrueType / OpenType (TTF / OTF)</strong> <br>Créé par Apple vers la fin des 1980s, le format TrueType concurrence alors le format Type 1 du standard PostScript, alors développé par Adobe. Son successeur, le format OpenType, est annoncé publiquement en 1996 par Adobe. Il conserve la structure de base du TrueType en y ajoutant des structures de données complexes enrichissant les possibilités typographiques. <br>
</p>

<p class="text-sm">
  <strong>Web Open Font Format (WOFF / WOFF2)</strong> <br>Fichier de police de caractères SFNT (TrueType, OpenType) comprimé à l’aide d’un outil de codage WOFF1, permettant une réduction de taille de fichier de plus de 40 %. Le 12 avril 2010, Microsoft, Mozilla Foundation et Opera Software ASA présentent le WOFF 1.0, alors uniquement supporté par Firefox 3.6. Le WOFF 2.0 est publiée en 2013. <br>
</p>

</div>

--

<!-- .slide: data-background-transition="slide" -->

## CSS

### @font-face

```css
@font-face {
  font-family: 'Fontname';
  src: local("Fontname"),
       url('http://your.site/fonts/fontname/fontname.woff2') format('woff2'),
       url('http://your.site/fonts/fontname/fontname.woff') format('woff'),
       url('http://your.site/fonts/fontname/fontname.svg#Fontname') format('svg'),
       url('http://your.site/fonts/fontname/fontname.ttf') format('truetype'),
       url('http://your.site/fonts/fontname/fontname.otf') format("opentype"),
       url('http://your.site/fonts/fontname/fontname.eot'),
       url('http://your.site/fonts/fontname/fontname.eot?#iefix') format('embedded-opentype');
  font-weight: normal;
  font-style: normal;
}
```

<p class="text-sm mono">Source: <a href="https://developer.mozilla.org/fr/docs/Web/CSS/@font-face" target="_blank">@font-face</a>, MDN</p>

--

<!-- .slide: data-background-color="#fff" data-background-transition="slide" class="top-0" -->


## OpenType features

<figure>
  <img src="opentype-features.png" alt="" width="800">
  <figcaption style="text-align: left;">Source: <a hreflang="en" href="http://ilovetypography.com/OpenType/opentype-features.html">An introduction to OpenType features</a>, I Love Typography</figcaption>
</figure>

--

<!-- .slide: data-background-transition="slide" -->

## CSS

<h3><code>font-variant</code> <br><code>font-feature-settings</code></h3>

```css
selector {
  font-variant: small-caps common-ligatures tabular-nums slashed-zero;
}

selector {
  font-feature-settings: "c2sc", "smcp";
}
```

<p class="text-sm mono" style="margin-top: 5rem">Source: <a href="https://developer.mozilla.org/fr/docs/Web/CSS/font-variant" target="_blank">font-variant</a> + <a href="https://developer.mozilla.org/fr/docs/Web/CSS/font-feature-settings" target="_blank">font-feature-settings</a>, MDN</p>

--

<!-- .slide: data-background-transition="slide" -->

### Unités relatives

<div class="col-2" style="align-items: flex-start; margin-top: -2rem">

<div class="text-sm">

<p>
  <strong><code>em</code></strong> <br>La taille de police héritée de l’élément.
</p>

<p>
  <strong><code>ex</code></strong> <br>Hauteur d’x de la font de l’élément.
</p>

<p>
  <strong><code>ch</code></strong> <br>Largeur du caractère « 0 » (zéro) dans la police actuelle.
</p>

<p>
  <strong><code>rem</code></strong> <br>Valeur initiale de la <code>font-size</code> de l'élément racine. Quand utilisée avec <code>font-size</code> sur l'élément racine, elle représente sa valeur initiale.
</p>

</div>

<div class="text-sm">

<p>
  <strong><code>vh</code></strong> <br>1/100<sup>e</sup> de la hauteur du viewport.
</p>

<p>
  <strong><code>vw</code></strong> <br>1/100<sup>e</sup> de la largeur du viewport.
</p>

<p>
  <strong><code>vmin</code></strong> <br>1/100<sup>e</sup> de la valeur minimale entre la hauteur et la largeur du viewport.
</p>

<p>
  <strong><code>rem</code></strong> <br>1/100<sup>e</sup> de la valeur maximale entre la hauteur et la largeur du viewport.
</p>

</div>

</div>

<p class="text-sm mono">Source: <a href="https://developer.mozilla.org/fr/docs/Web/CSS/length" target="_blank">CSS length</a>, MDN</p>

--

<!-- .slide: data-background-transition="slide" -->

### Unités absolues

<p class="text-sm">
  <strong><code>px</code></strong> <br>Pour l’affichage sur écran, correspond typiquement à un pixel de l’affichage. Pour les écrans en haute résolution et les imprimantes, un pixel CSS correspond à plusieurs pixels du périphérique, de sorte que le nombre de pixels par pouce (ppi) reste aux alentours de 96.
</p>

<p class="text-sm">
  <strong><code>mm, q, cm | in | pt, pc</code></strong> <br>Pour l'affichage sur écran, le nombre de pixels par unité est déterminé par l’estimation du système (souvent incorrecte)
de la résolution de son affichage.
</p>

<ul class="text-sm">
  <li><code>1in</code> est toujours égal à <code>96px</code></li>
  <li><code>3pt</code> est toujours égal à <code>4px</code></li>
  <li><code>25.4mm</code> est toujours égal à <code>96px</code></li>
</ul>

--

<!-- .slide: data-background-transition="slide" -->

## CSS Fonts

```css
selector {
  font-family: "Minion Pro", "Garamond", sans-serif;
  font-style: normal; /* normal | italic | oblique */
  font-weight: normal; /* normal | bold | bolder | lighter | 100 ... 900 ] */
  font-stretch: normal; /* ultra-condensed | extra-condensed | condensed | semi-condensed |
                  semi-expanded | expanded | extra-expanded | ultra-expanded */
  font-size: 1rem; /* 16px | 100% */
  line-height: 1.5; /* 150% | 24px */
}
```

<p class="text-sm mono" style="margin-top: 5rem">Source: <a href="https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Fonts" target="_blank">CSS Fonts</a>, MDN</p>

--

<!-- .slide: data-background-transition="slide" -->

## CSS Text

```css
selector {
    letter-spacing: 0.01em; /* 1px */
    word-spacing: auto; /* auto | loose | normal | strict */
    hyphens: auto; /* none | manual | auto */
    text-align: left; /* left | right | center | justify */
    text-indent: 1rem; /* 16px */
    text-transform: none; /* none | capitalize | uppercase | lowercase /*
}
```

<p class="text-sm mono" style="margin-top: 5rem">Source: <a href="https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Fonts" target="_blank">CSS Fonts</a>, MDN</p>

--

<!-- .slide: data-background-color="#fff" data-background="Better_Web_Type_Cheat_Sheet-1.png" data-background-size="contain" data-background-transition="slide" -->

<a href="Better_Web_Type_Cheat_Sheet-1.pdf" class="fixed inset-0"></a>

--

<!-- .slide: data-background-transition="slide" class="top-0 text-center" -->

## Distributeurs

<table style="margin-top: 6rem">
  <thead>
    <tr>
      <th>Site</th>
      <th>Pricing</th>
      <th>Families</th>
    </tr>
  </thead>
  <tbody>
  <tr>
    <td><a href="https://fonts.google.com/" target="_blank">Google Fonts</a></td>
    <td>free</td>
    <td class="mono" style="text-align: right;">1300+</td>
  </tr>
  <tr>
    <td><a href="https://fonts.com/" target="_blank">Fonts.com</a></td>
    <td>free / payant</td>
    <td class="mono" style="text-align: right;">20000+</td>
  </tr>
  <tr>
    <td><a href="https://myfonts.com/" target="_blank">Myfonts</a></td>
    <td>free / payant</td>
    <td class="mono" style="text-align: right;">25000+</td>
  </tr>
  <tr>
    <td><a href="https://fonts.adobe.com/" target="_blank">Adobe Fonts</a></td>
    <td>payant (abonnement)</td>
    <td class="mono" style="text-align: right;">20000+</td>
  </tr>
  <tr>
    <td><a href="https://www.fontshop.com/" target="_blank">FontShop</a></td>
    <td>free / payant</td>
    <td class="mono" style="text-align: right;">150+</td>
  </tr>
  <tr>
    <td><a href="https://www.fontspring.com/" target="_blank">Fontspring</a></td>
    <td>free / payant</td>
    <td class="mono" style="text-align: right;">10000+</td>
  </tr>
  <tr>
    <td><a href="https://www.typography.com/webfonts" target="_blank">Hoefler&Co.</a></td>
    <td>payant</td>
    <td class="mono" style="text-align: right;">50+</td>
  </tr>
  </tbody>
</table>

--

<!-- .slide: data-background-color="#fff" data-background="FontStandFoundries-0.png" data-background-size="contain" data-background-transition="slide" class="text-center" -->

## Fonderies

<div style="height: 500px; width: 200px"></div>

<a href="https://fontstand.com/foundries" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background="FontStandFoundries-2.png" data-background-size="contain" data-background-transition="slide" -->

<a href="https://fontstand.com/foundries" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background="FontStandFoundries-3.png" data-background-size="contain" data-background-transition="slide" -->

<a href="https://fontstand.com/foundries" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background="typefoundry.directory.png" data-background-transition="slide" class="text-center bg-top" -->

<a href="https://typefoundry.directory/" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background="typefoundry.directory.png" data-background-transition="slide" class="text-center bg-top" -->

<a href="https://typefoundry.directory/" target="_blank" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background="french-type-foundries.png" data-background-size="contain" data-background-transition="slide" class="text-center" -->

<a href="https://aisforfonts.com/french-type-foundries" target="_blank" class="fixed inset-0"></a>


--

## Pricing & licensing

<p class="subtitle">Combient coûte une police de caractères <br>et quelle licence choisir ?</p>

--

<!-- .slide: data-background-color="#fff" data-background="myfonts-recoleta.png" data-background-size="contain" class="bg-top" -->

<a href="https://www.myfonts.com/fonts/latinotype/recoleta?tab=familyPackages" target="_blank" class="fixed inset-0" title="Recoleta, MyFonts.com"></a>

--

<!-- .slide: data-background-color="#000" data-background="klimtype-domaine-buy.png" data-background-size="contain" class="bg-top" -->

<a href="https://klim.co.nz/buy/domaine/" target="_blank" class="fixed inset-0" title="Klim Type Foundry, Buy Domaine"></a>

--

<!-- .slide: data-background-color="#fff" data-background="hoefler-gotham-packages.png" data-background-size="cover" class="bg-top" -->

<a href="https://www.typography.com/fonts/gotham/styles" target="_blank" class="fixed inset-0" title="Hoefler&Co., Gotham Packages"></a>

--

### En résumé

- **si seul vous (ou votre studio) avez besoin d’utiliser une police de caractères, vous seul devez posséder une licence**. <br><small>Si votre client doit pouvoir utiliser une police, il doit alors aussi posséder un license à son nom.</small>
- **le prix d'une police de caractères dépend du type** <br><small>(desktop, web, app…)</small> **et du nombre** <small>(1, 5, 100…)</small> **de licences nécessaires**
- **chaque distributeurs et chaque fonderie possède son propre système de licensing** <small>(self-hosted, abonnement…)</small> <br><small>Il faut donc de s’y intéresser et ne pas hésiter à les contacter si vous avez un doute. <br>Cela fait partie de leur métier de répondre à vos questions !</small>

---

<!-- .slide: data-background-transition="slide" -->

<p class="subtitle">Pour aller plus loin</p>

## Ressources

--

<!-- .slide: data-background-transition="slide" -->

### Dessin de caractère

<div class="col-2" style="align-items: flex-start">
<div>

**Casual**

<ul class="list-image">
  <li><img src="FontStruct.png" width="24" height="24"> <a href="https://fontstruct.com/" target="_blank">FontStruct</a> <small>(web)</small></li>
  <li><img src="prototypo.png" width="24" height="24"> <a href="https://fontstruct.com/" target="_blank"><s>ProtoTypo</s></a> <small>(web)</small></li>
  <li><img src="glyphr_studio_logo.png" width="24" height="24"> <a href="https://fontstruct.com/" target="_blank">glyphr studio</a> <small>(web)</small></li>
  <li><img src="glyphsminiicon.png" width="24" height="24"> <a href="https://glyphsapp.com/news/glyphs-mini-2" target="_blank">Glyphs Mini 2</a> <small>(49€, Mac)</small></li>

</div>

<div>

**Pro**

<ul class="list-image">
  <li><svg width="24" height="20" xmlns="http://www.w3.org/2000/svg" fill="#87f32e" viewBox="0 0 189 180"><path d="M97.125 42.1714c0 27.5143 19.425 46.0286 45.937 46.0286C169.575 88.2 189 69.6857 189 42.1714V0H97.125v42.1714zm0 49.8858C97.125 142.971 135.712 180 189 180V92.0572H97.125zM91.875 0C38.5875 0 0 37.8 0 90s38.5875 90 91.875 90V0z"></path></svg> <a href="https://glyphsapp.com/" target="_blank">Glyphs 3</a> <small>(299€, Mac)</small></li>
  <li><img src="FontLab7.png" width="24" height="24"> <a href="https://www.fontlab.com/" target="_blank">FontLab 7</a> <small>(649$, Mac + Windows)</small></li>
  <li><img src="robofont.png" width="24" height="24"> <a href="https://robofont.com/" target="_blank">Robofont</a> <small>(400€, Mac)</small></li>
  <li><img src="FontForge.png" width="24" height="24"> <a href="https://fontforge.org/" target="_blank">FontForge</a> <small>(Windows + Mac + Linux)</small></li>
</ul>

</div>
</div>

--

<!-- .slide: data-background-transition="slide" -->

### Gestion de polices de caractères

<div class="col-2" style="align-items: flex-start">
<div>

**Gratuit**

<ul class="list-image">
  <li><img src="livre-des-polices.png" width="24" height="24"> Livre des polices <small>(Mac)</small></li>
  <li><svg viewBox="0 0 64 64" width="24" height="20" fill="#360398"><path d="M60.7,36.2L55,30.4L43.5,19l-3.8-3.8c-4.2-4.2-11.1-4.2-15.4,0L20.5,19L9,30.4l-5.7,5.7c-3.2,3.2-3.2,8.3,0,11.5h0 c3.2,3.2,8.3,3.2,11.5,0l5.7-5.7l5.7,5.7c3.2,3.2,8.3,3.2,11.5,0c3.2-3.2,3.2-8.3,0-11.5l5.7,5.7l5.7,5.7c3.2,3.2,8.3,3.2,11.5,0 l0,0C63.9,44.5,63.9,39.3,60.7,36.2z"></path></svg> <a href="https://fontba.se/" target="_blank">FontBase</a> <small>(Mac + Windows + Linux)</small></li>
  <li><img src="typeface3.png" width="24" height="24"> <a href="https://typefaceapp.com/" target="_blank">Typeface</a> <small>(free trial/40€, Mac)</small></li>
</div>

<div>

**Payant**

<ul class="list-image">
  <li><img src="RightFont.png" width="24" height="24"> <a href="https://rightfontapp.com/" target="_blank">RightFont</a> <small>(49€, Mac)</small></li>
  <li><img src="FontExplorerX.png" width="24" height="24"> <a href="https://www.fontexplorerx.com/" target="_blank">FontExplorer X Pro</a> <small>(89$, Mac)</small></li>
  <li><img src="SuitcaseFusion.png" width="24" height="24"> <a href="https://www.extensis.com/suitcase-fusion" target="_blank">Suitecase Fusion</a> <small>(86$/an, Mac + Windows)</small></li>
</ul>

</div>
</div>

--

<!-- .slide: data-background-transition="slide" -->

### Identification de police

<div class="col-2" style="align-items: flex-start">
<div>

**En ligne / Mobile**

<ul class="list-image">
  <li><img src="WhatTheFont.png" width="24" height="24"> <a href="https://www.myfonts.com/WhatTheFont/" target="_blank">WhatTheFont!</a> <small>(web, iOS, Android)</small></li>
  <li><img src="matcherator.png" width="24" height="24"> <a href="https://www.fontsquirrel.com/matcherator" target="_blank">Matcherator</a> <small>(web)</small></li>
  <li><img src="identifont.png" width="24" height="24"> <a href="http://www.identifont.com/" target="_blank">Identifont</a> <small>(web)</small></li>
  <li><img src="quora-typeface-identification.jpeg" width="24" height="24"> <a href="https://www.quora.com/topic/Typeface-Identification" target="_blank">Quora Typeface Identification</a> <small>(web)</small></li>
</div>

<div>

**Extensions / Navigateur**

<ul class="list-image">
  <li>Inspecteur <small>(outils de développement)</small></li>
  <li><img src="FontsNinja.png" width="24" height="24"> <a href="https://www.fonts.ninja/" target="_blank">Fonts Ninja</a> <small>(Chrome, Safari, Firefox)</small></li>
  <li><img src="CSSPepper.png" width="24" height="24"> <a href="https://csspeeper.com/" target="_blank">CSS Peeper</a> <small>(Chrome)</small></li>
</ul>

</div>
</div>

--

<!-- .slide: data-background-transition="slide" class="text-center top-0" -->

### Bibliographie

<figure class="image-links" style="margin: 6rem auto 0; max-width: 80%;">
  <div style="display: flex; justify-content: center; align-items: baseline; flex-wrap: wrap">
    <a href="https://www.placedeslibraires.fr/livre/9782350173344-comprendre-la-typographie-ellen-lupton/" target="_blank"><img src="EllenLupton-ComprendreLaTypographie.jpg" alt="" width="127"></a>
    <a href="https://www.placedeslibraires.fr/livre/9782919380374-observer-comprendre-et-utiliser-la-typographie-francais-gautier-d-roller-f/" target="_blank"><img src="DamienGauthier+FlorenceRoller-Observer-comprendre-et-utiliser-la-typographie.jpg" alt="" width="160"></a>
    <a href="https://www.placedeslibraires.fr/livre/9782212126211-typographie-la-lettre-le-mot-la-page-jacques-bracquemond-jean-luc-dusong/" target="_blank"><img src="JacquesBraquemond-Typographie.jpg" alt="" title=" Jacques Bracquemond, Jean-Luc Dusong, Typographie – la lettre, le mot, la page, Eyrolles" width="146" height="210"></a>
    <a href="https://typographica.org/typography-books/the-elements-of-typographic-style-4th-edition/" target="_blank"><img src="RobertBringhurst-TheElementsOfTypographicStlye.jpg" alt="" title="Robert Bringhurst, The Elements of Typographic Stlye" width="99" height="168"></a>
    <a href="https://www.placedeslibraires.fr/livre/9783836554527-la-fontaine-aux-lettres-joep-pohlen/" target="_blank"><img src="JoepPohlen-LaFontaineAuxLettres.jpg" alt="" title="La Fontaine aux lettres, Joep Pohlen, Taschen" width="125" height="176"></a>
    <a href="https://editions-b42.com/produit/le-detail-en-typographie/" target="_blank"><img src="JostHochuli-Detail-en-typographie.jpg" alt="" title="Jost Hochuli, le détail en typographie, B42" width="85" height="147"></a>
    <a href="https://livre.fnac.com/a2619277/Muriel-Paris-Le-petit-manuel-de-la-composition-typographique" target="_blank"><img src="MurielParis-PetitManuelDeCompositionTypographique.jpg" alt="" title="Muriel Paris, Le Petit Manuel de Composition Typographique" width="103" height="154"></a>
    <a href="https://betterwebtype.com/web-typography-book/" hreflang="en" target="_blank"><img src="better-web-typography.png" alt="" title="Better Web Typography for a Better Web, Matej Latin" width="104" height="160"></a>
    <a href="https://www.placedeslibraires.fr/livre/9782212141481-typographie-web-jason-santa-maria/" target="_blank"><img src="JasonSantaMaria-TypographieWeb.jpg" alt="" title="Jason Santa Maria, Typographie Web, Eyrolles" width="100" height="154"></a>
    <a href="http://editions-b42.com/books/La-Typographie-moderne/" target="_blank"><img src="RobinKinross-LaTypographieModerne.jpg" alt="" title="Robin Kinross, La Typographie Moderne, B42" width="86" height="147"></a>
    <a href="https://www.placedeslibraires.fr/livre/9782356540096-trait-le-noordzij-gerrit/" target="_blank"><img src="GerritNoordizj-LeTrait.jpg" alt="" title="Gerrit Noordizj, Le Trait" width="87" height="147"></a>
  </div>
</figure>

--

<!-- .slide: data-background="#fff" data-background-image="poussetafonte.com.png" data-background-transition="slide" class="bg-top" -->

<a href="https://www.poussetafonte.com/" target="_blank" title="Pousse ta fonte" class="fixed inset-0"></a>

--

<!-- .slide: data-background="#fff" data-background-image="FreshFonts.png" data-background-transition="slide" class="bg-top" -->

<a href="https://medium.com/fresh-fonts" target="_blank" title="Fresh Fonts, Noemi Stauffer" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#e6ddde" data-background-image="typewolf-guides.png" data-background-transition="slide" class="bg-top" -->

<a href="https://www.typewolf.com/guides" target="_blank" title="Typewolf Guides, Jeremiah Shoaf" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#f7fdf4" data-background-image="FontReviewJournal.png" data-background-transition="slide" class="bg-top" -->

<a href="https://fontreviewjournal.com/" target="_blank" title="Font Review Journal, Bethany Heck" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#000" data-background-image="WomenInType.png" data-background-transition="slide" class="bg-top" -->

<a href="https://www.women-in-type.com/" target="_blank" title="Women in Type" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background-image="OHnoType-Teaching.png" data-background-transition="slide" class="bg-top" -->

<a href="https://ohnotype.co/blog/tagged/teaching" target="_blank" title="OH no Type Company, Teaching" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background-image="Cours-de-typographie-AdrienZammit.jpg" data-background-transition="slide" class="bg-top" -->

<a href="http://formes-vives.org/documents/Cours-de-typographie-AdrienZammit-partie1.pdf" target="_blank" title="Cours de typographie – Adrien Zammit" class="fixed inset-0"></a>

--

<!-- .slide: data-background-color="#fff" data-background-image="are.na.timothee-goguely-typographie.png" data-background-transition="slide" class="bg-top" -->

<a href="https://www.are.na/timothee-goguely/typography-dux3iyitwao" target="_blank" title="Are.na / Timothée Goguely / Typography" class="fixed inset-0"></a>

--

#### Crédits

<p class="text-sm">
  Slides : <br><a href="https://revealjs.com/">reveal.js</a> (thème par Timothée Goguely)
</p>

<p class="text-sm">
  Polices de caractères : <br><a href="https://nodotypefoundry.com/product/nt-bau/">NT Bau</a> + <a href="https://nodotypefoundry.com/product/bau-mono/">NT Bau Mono</a> (Nodo Type Foundry), Minion Pro (Robert Slimbach)
</p>
