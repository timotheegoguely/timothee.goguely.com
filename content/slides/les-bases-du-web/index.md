---
title: Les bases du web
slug: les-bases-du-web
description: LISAA – G2/G3
date: 2022-09-21
cover: https://timothee.goguely.com/slides/les-bases-du-web/cover.png
layout: slides
tags:
  - cours
  - web
  - HTML
  - CSS
transition: slide
backgroundTransition: slide

---

# Les bases du web

<hr>

LISAA
<a href="https://mastodon.design/@timotheegoguely">@timotheegoguely</a>
<small><time class="mono" datetime="2023-02">février 2023</time></small>

---

<!-- .slide: data-visibility="hidden" -->

<span class="heading-color">Timothée Goguely</span>
Designer et développeur web

<small>
  <a href="https://timothee.goguely.com">timothee.goguely.com</a>
</small>

---

<p class="subtitle">De quoi parle-t-on</p>

# Définitions

--

## Qu’est-ce que <br>le web&#8239;?

--

> Le **World Wide Web** (abrégé **www** ou **Web**) est un système hypertexte public fonctionnant sur Internet. Le Web permet de consulter, avec un navigateur, des pages accessibles sur des sites. 

L’image de la toile d’araignée vient des hyperliens qui lient les pages web entre elles.

<p class="text-sm">Source&#8239;: <em><a href="http://fr.wikipedia.org/wiki/World_Wide_Web">World Wide Web</a></em>, Wikipédia</p>

--

## Internet ≠ Web

--

**[Internet](http://fr.wikipedia.org/wiki/Internet)** est le réseau informatique mondial, 
c’est l’infrastructure globale, basée sur le protocole IP, 
et sur laquelle s’appuient de nombreux autres services. 
Dont le web.

Le **[World Wide Web](http://fr.wikipedia.org/wiki/Web)**, c’est le système qui nous permet 
de naviguer de pages en pages en cliquant sur des liens 
grâce à un navigateur.

<p class="text-sm"><a href="http://fr.wikipedia.org/wiki/ARPAnet" target="_blank">ARPAnet</a>, l’ancêtre d’Internet, est né aux alentours de <strong>1967</strong>, <br>tandis que Tim Berners-Lee n’aura l’idée du système hypertexte <br>distribué sur le réseau informatique, la base du web, qu’en <strong>1989</strong>.<p>

<p class="text-sm">Source&#8239;: <em><a href="https://www.miximum.fr/blog/quelle-est-la-difference-entre-web-et-internet/">Quelle est la différence entre web et internet ?</a></em>, Thibault Jouannic</p>

--

> **Internet** est le **réseau**. <br>Le **web** est un **service**.

Le web n’est qu’un des services accessibles via Internet, 
et il y en a bien d’autres <small>(emails, messagerie instantanée, streaming, etc.)</small>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <iframe src="https://www.infrapedia.com/" height="560"></iframe>
  <figcaption><a href="https://www.infrapedia.com/" target="_blank">Infrapedia</a>, carte interactive des infrastructures d’Internet</figcaption>
</figure>

---

<p class="subtitle">Brève et partielle</p>

# Histoire du <br>World Wide Web

--

<p class="subtitle">Tim Berners-Lee et le CERN</p>

## Origines du web

<p class="text-sm mono">Sources: <a href="https://web30.web.cern.ch/web-history.html" target="_blank">web30.web.cern.ch/web-history.html</a> + Wikipédia</p>

--

<!-- .slide: class="col-2" -->

<div>

### Mars 1989
**Sir Tim Berners-Lee soumet sa première proposition pour ce qui est devenu le World Wide Web.**

<p class="text-sm">Alors qu’il travaille au <a href="https://fr.wikipedia.org/wiki/Organisation_europ%C3%A9enne_pour_la_recherche_nucl%C3%A9aire" target="_blank"><abbr title="Conseil européen pour la recherche nucléaire">CERN</abbr></a>, il rédige une proposition visant à développer un système d'information distribué. Il en présente <a href="https://cds.cern.ch/record/369245/files/dd-89-001.pdf" target="_blank">une version légèrement modifiée</a> en mai 1990.<p>

</div>

<figure>
  <img src="1989-Tim-Berners-Lee-CERN-Information-Management-A-Proposal.png" alt="" width="320">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Novembre 1990
**Proposition de gestion pour un projet de World Wide Web**

<p class="text-sm">En novembre 1990, Sir Tim Berners-Lee et son collègue du CERN, Robert Cailliau, soumettent une proposition officielle de gestion pour <a lang="en" href="https://cds.cern.ch/record/2639699/files/Proposal_Nov-1990.pdf" target="_blank">WorldWideWeb: Proposal for a HyperText Project</a>.</p>

</div>

<figure>
  <img src="tim-beneers-lee.jpg" alt="Tim Berners-Lee">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Décembre 1990
**Le premier navigateur/éditeur, site web et serveur au monde est mis en ligne au CERN**

<p class="text-sm">À Noël 1990, Sir Berners-Lee définit les concepts de base du Web, à savoir le HTML, le HTTP et l’URL, et il écrit le premier logiciel de navigation/édition et de serveur. <a href="https://info.cern.ch/" target="_blank">info.cern.ch</a> est l'adresse du <a href="https://cds.cern.ch/record/42413" target="_blank">premier serveur Web au monde</a>, fonctionnant sur un ordinateur NeXT au CERN. <a href="http://info.cern.ch/hypertext/WWW/TheProject.html" target="_blank">La première adresse de page web au monde</a> fournissait des informations sur le projet World Wide Web.</p>

</div>

<figure>
  <img src="1990-first-browser-editor.jpg" alt="Premier navigateur/éditeur" width="240">
  <img src="1990-premier-serveur-web.jpg" alt="Premier serveur web">
  <figcaption>HyperMedia Browser/Editor v1.0 <br>et le premier serveur web.</p>
</figure>

--

<!-- .slide: data-background="#fff" data-background-transition="slide" -->

<figure class="text-center">
  <iframe src="https://info.cern.ch/hypertext/WWW/TheProject.html" height="560"></iframe>
  <figcaption><a href="https://info.cern.ch/">info.cern.ch</a>, le premier site web, 1990</figcaption>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Mars 1991
**Le navigateur Line Mode est créé au CERN**

<p class="text-sm">Un simple navigateur « en mode ligne » est mis à la disposition des utilisateurs des ordinateurs centraux du CERN. Bien qu'il ait moins de fonctionnalités que le navigateur/éditeur NeXT plus sophistiqué, il avait le grand avantage de pouvoir fonctionner sur un plus grand nombre d'ordinateurs.</p>

</div>

<figure>
  <img src="1991-lmb-terminal.jpg" alt="" loading="lazy">
  <figcaption>Le navigateur Line Mode, écrit par Nicola Pellow pendant son stage d'étudiant au CERN.</p>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Août 1991
**Sir Berners-Lee annonce le logiciel WWW sur l’Internet**

<p class="text-sm">En août 1991, Sir Berners-Lee annonce son logiciel WWW sur les groupes de discussion Internet et l'intérêt pour le projet s’étend au-delà de la communauté des physiciens.</p>
<p class="text-sm"><a href="https://www.w3.org/People/Berners-Lee/1991/08/art-6484.txt" target="_blank">La première annonce est faite le 6 août 1991 à alt.hypertext</a>, un groupe de discussion pour les passionnés d'hypertexte. Il y décrit le projet et a fourni des instructions pour obtenir le logiciel WWW du CERN.</p>

</div>

<figure>
  <img src="1991-alt.hypertext.png" alt="alt.hypertext" width="400" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Décembre 1991
**Premier serveur web hors d’Europe**

<p class="text-sm">Le 12 décembre 1991, le premier serveur web hors Europe est installé au Stanford Linear Accelerator Center (SLAC) en Californie. Il donne accès à SPIRES, une base de données contenant des informations destinées aux scientifiques travaillant dans le domaine de la physique des hautes énergies (HEP), et permettant notamment de rechercher des publications.</p>

</div>

<figure>
  <img src="1991-SLAC-p3-3_180.webp" alt="Stanford Linear Accelerator Center (SLAC) en Californie" width="280">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Janvier 1992 
**WWW passe du prototype à la production**

<p class="text-sm">Le logiciel WWW du CERN passe d’un premier prototype à un service utile et fiable. Grâce au bulletin d’information du CERN, des milliers de scientifiques apprennent comment utiliser le web pour accéder à un ensemble d’informations utiles, par exemple des numéros de téléphone, des adresses électroniques, des groupes de discussion, ainsi que de la documentation informatique et logicielle.</p>

</div>

<figure>
  <img src="1992-CERN-computer-newsletter-n-204.jpg" alt="" width="360">
  <figcaption>CERN Computer Newsletter no. 204</p>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Septembre 1992
**Un nombre restreint mais croissant de serveurs et de navigateurs Web**

<p class="text-sm">Fin 1992, le projet WWW dispose d’une liste croissante des <a href="https://www.w3.org/History/19921103-hypertext/hypertext/DataSources/WWW/Servers.html" target="_blank">premiers serveurs web</a>. Ils sont principalement situés sur des sites universitaires collaborant avec le CERN, mais l’intérêt commençait à s'étendre au-delà du milieu universitaire. <br>Les <strong>premiers navigateurs graphiques</strong> (par exemple MIDAS de Tony Johnson de SLAC, Viola de Pei Wei par l'éditeur technique O'Reilly Books et Erwise des étudiants finlandais de l'université de technologie d'Helsinki) étaient également en cours de développement.</p>

</div>

<figure>
  <img src="violawww.png" alt="" width="440">
  <figcaption>ViolaWWW Hypermedia Browser (v3.3), <br>Pei Wei, O'Reilly Books</p>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Janvier 1993
**Première pré-version du navigateur Mosaic**

<p class="text-sm">Le Centre national pour les applications de superinformatique (<em lang="en">National Center for Supercomputing Applications – NCSA</em>) de l’Université de l’Illinois fourni des pré-versions de son navigateur <a href="https://en.wikipedia.org/wiki/Mosaic_(web_browser)" target="_blank">Mosaic</a> pour système Unix <a href="https://en.wikipedia.org/wiki/X_Window_System">X Window System</a>. La première version officielle esy publiée le 21 avril 1993. Mosaic gagne rapidement en popularité avec son interface graphique conviviale et son installation facile. Des versions de Mosaic fonctionnant sur PC et Mac sont disponibles plus tard dans l'année.</p>

</div>

<figure>
  <img src="1993-NCSA_Mosaic_Browser_Screenshot.png" alt="" width="440">
  <figcaption>NCSA Mosaic Browser v2.7b6</p>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Avril 1993
**Le CERN met le World Wide Web dans le domaine public**

<p class="text-sm">Le 30 avril 1993, le CERN publie une déclaration plaçant le Web dans le domaine public, garantissant qu’il s’agit d’une norme ouverte. Cette décision a eu un effet immédiat sur la diffusion du Web. D’autres mesures d’autorisation sont prises pour permettre au Web d’évoluer et de s’épanouir. Fin 1993, on compte plus de 500 serveurs web connus et le WWW représente 1 % du trafic Internet.</p>

</div>

<figure>
  <img src="1993-cern-statement.jpg" alt="" width="320">
  <figcaption><em>Statement concerning CERN W3 software release into public domain</em>, CERN</p>
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Mai 1994
**Première conférence internationale sur le Web 
tenue au CERN**

<p class="text-sm">Robert Cailliau organise la <a href="https://www94.web.cern.ch/WWW94/Welcome0522.html" lang="en" target="_blank">First International World-Wide Web Conference</a> au CERN. Elle réunit 380 utilisateurs et développeurs, et est saluée comme le “Woodstock du Web”.</p>

</div>

<figure>
  <img src="1994-first-international-world-wide-web-conference.gif" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### Octobre 1994
**Création du World Wide Web Consortium (W3C)**

<p class="text-sm">En octobre 1994, Sir Tim Berners-Lee fonde le <a href="https://www.w3.org/" target="_blank" lang="en">World Wide Web Consortium</a> au laboratoire d’informatique du Massachusetts Institute of Technology (MIT) – en collaboration avec le CERN et avec le soutien de la DARPA et de la Commission européenne. Il s’installe au MIT, d’où il reste directeur du W3C.</p>

</div>

<figure>
  <img src="w3c-logo.png" alt="W3C logo" style="background: #fff">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 1998
**Google**

<p class="text-sm">4 septembre 1998, Larry Page et Sergey Brin, étudiants de l'Université de Stanford, créent le moteur de recherche Google. Google a commencé à l'origine comme un projet de recherche dont le but était de trouver des résultats de recherche pertinents à l'aide d'un algorithme mathématique. L'algorithme, plus tard appelé PageRank, analysait les relations entre les pages Web individuelles en fonction de leurs références croisées, évaluant ainsi leur importance.</p>

</div>

<figure>
  <img src="1998-google.png" alt="" loading="lazy">
</figure>

--

<iframe width="960" height="540" src="https://www.youtube-nocookie.com/embed/k0gvAyCubGQ" title="A brief history of the World Wide Web – YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

--

<p class="subtitle">Web participatif, social et intelligence collective</p>

## Le web « 2.0 »

<p class="text-sm">Sources&#8239;: <a href="https://www.webdesignmuseum.org/web-design-history" target="_blank">Web Design History</a>, Web Design Musem + Wikipédia</p>

--

<!-- .slide: class="col-2" -->

<div>

### 2001
**Wikipédia**

<p class="text-sm">Janvier 2001, mise en ligne de la première version de la célèbre encyclopédie libre co-fondée par Jimmy Wales et Larry Sanger.</p>

**Mac OS X 10.0**

</div>

<figure>
  <img src="FirstVersions_Wikipedia-home.png" alt="" width="160" loading="lazy">
  <img src="2001-Mac-OS-X-10.png" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2002
**Mozilla 1.0**

<p class="text-sm">5 juin 2022, Mozilla publie le navigateur Web Mozilla 1.0. La base de Mozilla 1.0 était Gecko, un moteur de rendu open source qui a considérablement amélioré la prise en charge des normes Web.</p>

</div>

<figure>
  <img src="2002-mozilla-1-0.png" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2003
**Myspace**

<p class="text-sm">Août 2003, lancement du réseau social Myspace fondé aux États-Unis par Chris DeWolfe et Tom Anderson mettant gratuitement à disposition de ses membres enregistrés un espace web personnalisé, permettant de présenter diverses informations personnelles et d'y faire un blog.</p>

</div>

<figure>
  <img src="myspacescreen640x480.jpg" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2004
**Facebook**

<p class="text-sm">Février 2004, lancement sous le nom de « The Facebook » du célèbre réseau social fondé par Mark Zuckerberg et ses camarades de l'université Harvard. Facebook a franchit en 2017 le nombre de de 2 milliard d'utilisateurs actif.</p>

</div>

<figure>
  <img src="FirstVersions_Thefacebook-profile-screenshot.png" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2005
**Youtube**

<p class="text-sm">14 février 2005, première version de <br>l'hébergeur de vidéo en ligne YouTube, qui se <br>fera racheté par Google l'année suivante <br>en octobre 2006.</p>

</div>

<figure>
  <img src="FirstVersions_YouTube-2005-December.png" alt="" width="320" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2006
**Twitter**

<p class="text-sm">21 mars 2006, Jack Dorsey, Noah Glass, Evan Williams et Biz Stone lancent officiellement le service de réseautage social et de microblogging Twitter. Ce réseau social permet aux utilisateurs <br>de lire et d'envoyer de courts messages (140 caractères maximum) appelés «&#8239;tweets&#8239;».</p>

</div>

<figure>
  <img src="FirstVersions_Twitter-interface-Jul2006.png" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2007
**iPhone**

<p class="text-sm">9 janvier 2007, lancement de la première génération d'iPhone par Apple. L'appareil révolutionne la conception des téléphones portables en éliminant la plupart des boutons matériels et en renonçant à un stylet pour son interface à écran, au profit de quelques boutons physiques et d'un écran tactile</p>

</div>

<figure>
  <img src="2007-iphone2g.jpg" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2008
**Google Chrome**

<p class="text-sm">2 septembre 2008, Google publie la version bêta du navigateur de Google Chrome pour Windows. En décembre 2008, la première version stable de Google Chrome 1.0 sort. Depuis janvier 2009, Chrome est disponible pour MacOS, et en février 2012, sa première version bêta pour Android 4 est sortie. Au tournant d'avril et de mai 2012, Chrome a dépassé la popularité d'Internet Explorer et est devenu le navigateur Web le plus utilisé.</p>

</div>

<figure>
  <img src="2008-chrome-v1.webp" alt="" loading="lazy">
</figure>

--


<!-- .slide: class="col-2" -->

<div>

### 2009
**Bouton « Like » Facebook**

<p class="text-sm">9 février 2009, Facebook présente pour la première fois sa fonction « bouton J'aime ». <br>Cliquer sur le bouton avec une icône représentant un pouce vers le haut indique une réaction positive de l'utilisateur au contenu de la page web sur laquelle se trouve le bouton.</p>

</div>

<figure>
  <img src="2009-facebook-like-button.png" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2010
**Responsive Web Design**

<p class="text-sm">25 mai 2010, le web design Ethan Marcotte publie son article culte intitulé <a href="https://alistapart.com/article/responsive-web-design/" target="_blank">Responsive Web Design</a> dans le magazine en ligne <em>A List Apart</em>. L'auteur y décrit une nouvelle façon de styliser les documents HTML de façon à adapter leur affichage en fonction de la taille et de la résolution des écrans.</p>

**Instagram**

<p class="text-sm">6 octobre 2010, Kevin Systrom et Michel Mike Krieger lancent Instagram, le célèbre réseau social et service de partage de photos et de vidéos qui sera racheté en 2021 par le groupe Meta.</p>


</div>

<figure>
  <img src="2010-a-list-apart-responsive-web-design.png" alt="" width="320" loading="lazy">
  <img src="2010-instagram.png" alt="" width="200" loading="lazy">
</figure>

</div>

--

<!-- .slide: class="col-2" -->

<div>

### 2011
**Twitch**

<p class="text-sm">Juin 2011, lancement de la plateforme de streaming et VOD Twitch. Twitch est racheté le 25 août 2014 par Amazon alors que la société négociait son rachat par YouTube plus tôt dans l'année.</p>

**Adobe Creative Cloud**

<p class="text-sm">Octobre 2011, Adobe lance la première version de sa suite logiciel Creative Cloud et bascule vers un modèle d'abonnement.</p>

</div>

<figure>
  <img src="2011-twitch.webp" alt="" loading="lazy">
  <img src="2011-adobe-creative-cloud.jpg" width="80" height="80" alt="" loading="lazy">
</figure>


--

<!-- .slide: class="col-2" -->

<div>

### 2013
**Bootstrap 3**

<p class="text-sm">19 août 2013, une équipe de développeurs de GitHub publie le framework CSS Boostrap 3. <br>Cette nouvelle version de Bootstrap applique systématiquement une mise en page web responsive et mobile first. Les templates et des composants sont redesignés dans un style flat design.</p>

</div>

<figure>
  <img src="2013-bootstrap-3.png" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2014
**Material Design**

<p class="text-sm">25 juin 2014, Google presente un ensemble de principes d'UI/UX design appelé Material Design lors de la conférence Google I/O. Depuis 2015, Google a redesigné la plupart de ses applications et services en appliqant de ces principes.</p>

**HTML5**

<p class="text-sm">28 octobre 2014, finalisation de la dernière révision majeure du langage HTML.</p>

</div>

<figure>
  <img src="2014-google-design.png" alt="" loading="lazy">
  <img src="HTML5-logo.svg" alt="" loading="lazy" width="120" style="background:#fff;padding:1rem 0">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2015
**Microsoft Edge**

<p class="text-sm">30 mars 2015, Microsoft publie la première version de son navigateur web Microsoft Edge pour Windows 10. Microsoft Edge a ensuite été inclus comme navigateur par défaut sur les systèmes d'exploitation Windows 10 Mobile et Xbox One, remplaçant définitivement les anciens navigateurs Internet Explorer 11 et Internet Explorer Mobile.</p>

</div>

<figure>
  <img src="2015-microsoft-edge.png" alt="" loading="lazy">
  <img src="microsoft-edge-logo.png" alt="" loading="lazy" width="72" height="72" style="background:#fff;padding:1rem;">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2016
**Variable fonts**

<p class="text-sm">14 septembre 2016, annonce conjointe de Microsoft, Google, Apple et Adobe de la technologie <a href="https://medium.com/variable-fonts/https-medium-com-tiro-introducing-opentype-variable-fonts-12ba6cd2369" target="_blank">OpenType Font Variations</a>. Une police variable (variable font) est un fichier de police capable de stocker une gamme continue de variantes définie à partir d'un ou plusieurs axes (graisse, chasse, contraste, etc.)</p>

**Figma**
<p class="text-sm">27 septembre 2016, première diffusion publique de Figma, l'application de design d'interface créée par Dylan Field et Evan Wallace.</p>

</div>

<figure>
  <img src="variable-fonts.png" alt="" width="240" loading="lazy" style="background:#fff;padding:1rem;">
  <img src="2015-figma-v1.0.png" alt="" width="400" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

<div>

### 2017
**Annonce de la fin du support de Flash par Adobe**

<p class="text-sm">25 juin 2017, Adobe Systems annonce qu'il cessera de prendre en charge Flash en 2020 et ne publiera plus de mises à jour de sécurité supplémentaires pour Adobe Flash Player. Dans le domaine du web multimédia et de l'interactivité, Flash a progressivement été remplacé par la technologie HTML5, WebGL ou WebAssembly.</p>

</div>

<figure>
  <img src="2017-adobe-announced-termination-of-flash.png" alt="" loading="lazy">
</figure>

--

<!-- .slide: class="col-2" -->

### à suivre …

---

<p class="subtitle">HTTP, navigateurs, HTML, CSS, JS</p>

# Comment fonctionnent <br>les sites web&#8239;?

--

## Le protocole HTTP

Le protocole **HTTP** (HyperText Transfer Protocol), 
conçu par Tim Berners-Lee à partir de 1989, 
permet le transfert de fichiers localisés grâce à une **URL**, 
entre un **navigateur (le client)** et un **serveur web**. 

<p class="text-sm">Cette opération se fait en 2 temps avec l'envoi d'une requête <br>puis la réception de la réponse.</p>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="protocole-http.png" alt="Schema du fonctionnement du protocole HTTP.">
</figure>

<p class="text-sm">Source&#8239;: <em><a href="https://www.schoolmouv.fr/cours/la-page-web-http-et-langages-html-et-css-/fiche-de-cours">La page web (HTTP et langages HTML et CSS)</a></em>, schoolmouv.fr</p>

--

## Les navigateurs

Un **navigateur web** (*browser* en anglais) est un logiciel permettant de consulter les informations disponibles sur le web.

<figure>
  <img src="navigateurs.png" alt="Icônes des navigateurs Google Chrome, Microsoft Edge, Mozilla Firefox, Opera, Apple Safari et Microsoft Internet Explorer." width="540">
</figure>

<p class="text-sm">Il existe de nombreux navigateurs web, pour toutes sortes de <strong>terminaux</strong> <br>(ordinateur personnel, tablette tactile, téléphones mobiles…) et pour différents <strong>systèmes d’exploitation</strong> (GNU/Linux, Windows, Mac OS, iOS et Android).</p>

--

<iframe width="100%" height="540" src="https://www.youtube-nocookie.com/embed/es9DNe0l0Qo" title="YouTube video player – Usage Share of Internet Browsers 1996 - 2019" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

--

<figure class="text-center">
  <img src="wikipedia-world_wide_web.png" alt="" loading="lazy">
  <figcatpion class="text-sm">Page Wikipédia en français sur le <a href="https://fr.wikipedia.org/wiki/World_Wide_Web" target="_blank">World Wide Web</a>, affichée depuis le navigateur Firefox.</figcatpion>
</figure>

--

<figure class="text-center">
  <img src="wikipedia-world_wide_web-html.png" alt="" loading="lazy">
  <figcatpion class="text-sm">Code HTML de la page (Affichage > Style de la page > Aucun style).</figcatpion>
</figure>

--

<figure class="text-center">
  <img src="wikipedia-world_wide_web-code_source.png" alt="" loading="lazy">
  <figcatpion class="text-sm"><a href="view-source:https://fr.wikipedia.org/wiki/World_Wide_Web" target="_blank">Code source</a> de la page (Clic droit > Code source de la page)</figcatpion>
</figure>

--

## HTML, CSS <br>et JavaScript

--

<!-- .slide: data-background="#fff" -->

### HTML (Hypertext Markup Langage)

<img style="position:absolute;top:-3rem;right:0" src="HTML5-logo.svg" alt="Logo HTML" width="120" loading="lazy">

- Date de création&#8239;: **1991** <small>(lors du lancement du Web)</small>
- Type : langage de **balises**
- Rôle&#8239;: **structurer et donner du sens au contenu web** <br><small>(textes, liens, images, etc.)</small>
- Version actuelle&#8239;: **5.2**


--

<!-- .slide: data-background="#fff" -->

Prenons pour exemple un simple texte.
Les balises HTML lui donnent une structure et un but :

```html

<p>Player 1: Chris</p>

```

![HTML](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript/just-html.png)

--

<!-- .slide: data-background="#fff" -->

### CSS (Cascading Style Sheets)

- Date de création&#8239;: **1996**
- Type : langage de **règles de style**
- Rôle&#8239;: **gérer l’apparence, mettre en forme le contenu HTML** <br><small>(agencement, positionnement, décoration, couleurs, tailles, etc.)</small>
- Version actuelle&#8239;: **3**

<img style="position:absolute;top:-3rem;right:0" src="CSS3-logo.svg" alt="Logo CSS" width="90" loading="lazy">

--

<!-- .slide: data-background="#fff" -->

Nous pouvons ensuite ajouter du CSS 
pour modifier l’apparence graphique :

```css

  p {
    font-family: 'helvetica neue', helvetica, sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    text-align: center;
    border: 2px solid rgba(0,0,200,0.6);
    background: rgba(0,0,200,0.3);
    color: rgba(0,0,200,0.6);
    box-shadow: 1px 1px 2px rgba(0,0,200,0.4);
    border-radius: 10px;
    padding: 3px 10px;
    display: inline-block;
    cursor:pointer;
  }
 
```

![CSS](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript/html-and-css.png)

--

<!-- .slide: data-background="#fff" -->

### JS (JavaScript)

- Date de création&#8239;: **1996**
- Type : langage de **programmation**
- Rôle&#8239;: **permet d'implémenter des mécanismes complexes sur une page web** <br><small>(créer du contenu mis à jour de façon dynamique, contrôler le contenu multimédia, animer des images, et presque tout ce à quoi on peut penser…)</small>
- Version actuelle&#8239;: **ES6 (ECMAScript 2021)**

<img style="position:absolute;top:-3rem;right:0" src="JS-logo.png" alt="Logo JS" width="90" loading="lazy">

--

<!-- .slide: data-background="#fff" -->

Enfin, on peut utiliser JavaScript pour ajouter un comportement dynamique. Essayez de cliquer sur l'étiquette texte bleue pour voir ce qui se passe :

```js

  let para = document.querySelector('p');

  para.addEventListener('click', updateName);

  function updateName() {
    let name = prompt('Enter a new name');
    para.textContent = 'Player 1: ' + name;
  }
  
```

<iframe class="sample-code-frame" title="Une définition générale sample" id="frame_une_définition_générale" src="https://yari-demos.prod.mdn.mozit.cloud/fr/docs/Learn/JavaScript/First_steps/What_is_JavaScript/_sample_.une_d%C3%A9finition_g%C3%A9n%C3%A9rale.html" loading="lazy" width="100%" height="80"></iframe>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <iframe src="http://www.csszengarden.com/" frameborder="0" height="540" style="border:1px solid currentColor; border-radius:.125rem;" loading="lazy"></iframe>
  <figcaption><a href="http://www.csszengarden.com/" target="_blank">CSS Zen Garden</a> (2003), ou comment comprendre la séparation fond (HTML) / forme (CSS).</figcaption>
</figure>



---

<p class="subtitle">Apprendre le développement web</p>

# Outils & ressources

--

<!-- .slide: data-background="#fff" -->

## Éditeurs

--

<!-- .slide: data-background="#fff" class="col-2" -->

<div>

### Éditeurs de texte

<p>Un simple logiciel de traitement de texte comme TextEdit sur Mac ou Bloc-Notes sur Windows suffit pour écrire du code.</p>
<p class="text-sm">Mais il y a des logicels dédiés <br>beaucoup plus pratiques !</p>

</div>

<img src="text-edit.png" alt="" width="440" loading="lazy">

--

<!-- .slide: data-background="#fff" class="col-2" -->

<div>

### Éditeurs de code 👍

<p>Il existe de nombreux éditeurs de texte conçus spécialement pour le code.</p>
<p class="text-sm">Coloration syntaxique, auto-complétion, correcteurs, compileurs, minifiers, etc.</p>

</div>

<figure class="text-center">
  <img src="code-editors.png" alt="" width="480" loading="lazy" style="margin-top: 4rem;">
  <figcaption>Quelques éditeurs de code populaires :<br><a href="https://brackets.io/">Brackets</a>, <a href="https://atom.io/">Atom</a>, <a href="https://www.sublimetext.com/">Sublime Text</a> et <a href="https://code.visualstudio.com/">VS Code</a>.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<figure class="text-center">
  <iframe src="https://www.sublimetext.com/" frameborder="0" height="560" loading="lazy" style="border-radius:.125rem"></iframe>
  <figcaption><a href="https://www.sublimetext.com/" target="_blank">Sublime Text 4<a></figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->


<h3>Éditeurs <abbr title="What you see is what you get">WYSIWYG</abbr> 👎</h3>

<div style="display:flex;gap:2rem">
  <p style="margin-right:10%">Inutiles et déconseillés, <br>car ils ne permettent pas un <br>contrôle complet du code généré.</p>
  <img src="dreamweaver.jpg" alt="Logo Adobe Dreamweaver" width="120" height="120" loading="lazy">
  <img src="wix-logo.png" alt="Logo Wix" width="160" height="120" loading="lazy" style="object-fit:contain">
</div>

--

<!-- .slide: data-background="#fff" -->

<div>

### Environnements de <br>développement en ligne 🌐

<p>Il existe également des plateformes en ligne <br>proposant un environnement de code complet.</p>
<p class="text-sm">Live preview, intégraton de bibliothèques externes, <br>partage, remix, etc.</p>

</div>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="codepen.io.png" alt="" loading="lazy">
  <figcaption><a href="https://codepen.io/" target="_blank">CodePen</a>, un environnement de développement social en ligne <br>pour les designers et développeur·se·s front-end.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="glitch.com.png" alt="" loading="lazy" style="border:1px solid currentColor">
  <figcaption><a href="https://glitch.com//" target="_blank">Glitch</a>, un environnement de développement collaboratif <br>avec de nombreux exemples et tutoriels.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

## Navigateurs, extensions <br>& outils de développement

--

<!-- .slide: data-background="#fff" class="col-2" -->

<div>

### Navigateurs web

<p class="text-sm">Lorsque l’on fait du développement web, <br>un navigateur est indispensable pour tester son code avant de le publier en ligne.</p>

<p class="text-sm">Il existe des navigateurs dédiés au développement web tels que <a href="https://www.mozilla.org/fr/firefox/developer/" target="_blank">Firefox Developer Edition</a>.</p>

</div>

<figure style="margin-top:4rem;display:flex;flex-direction:column;align-items:center;">
  <img src="navigateurs.png" alt="" width="480" loading="lazy">
  <img src="firefox-developer-edition.jpg" alt="" width="72" loading="lazy">
</figure>

--

<!-- .slide: data-background="#fff" class="col-2" -->

<div>

### Extensions navigateur

<p class="text-sm">En fonction du navigateur que vous utilisez, plusieurs extensions sont à votre disposition pour vous aider à développer.</p>

<ul class="text-sm">
  <li><a href="https://addons.mozilla.org/fr/firefox/addon/web-developer/" target="_blank">Web Developer</a></li>
  <li><a href="https://addons.mozilla.org/fr/firefox/addon/wave-accessibility-tool/" target="_blank">WAVE Accessibility extension</a></li>
  <li><a href="https://addons.mozilla.org/fr/firefox/addon/headingsmap/" target="_blank">HeadingsMap</a></li>
</ul>

</div>

<figure style="margin-top:0; display: flex; gap: 2rem; justify-content: center;">
  <img src="web-developer.png" alt="" width="64" loading="lazy">
  <img src="wave-accessibility-tool.png" alt="" width="64" loading="lazy">
  <img src="headingsmap.png" alt="" width="64" loading="lazy">
</figure>

--

<!-- .slide: data-background="#fff" class="text-center" -->

### Outils de développement

<figure class="text-center">
  <img src="firefox-developer-edition-1.png" alt="" width="400" loading="lazy">
  <img src="firefox-developer-edition-2.gif" alt="" width="400" loading="lazy">
  <img src="firefox-developer-edition-3.gif" alt="" width="400" loading="lazy">
  <img src="firefox-developer-edition-4.gif" alt="" width="400" loading="lazy">
  <figcaption>Outils de développement du navigateur <a href="https://www.mozilla.org/fr/firefox/developer/" target="_blank">Firefox Developer Edition</a> <br><code>(cmd + alt + i)</code></figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

## Sites web

--

<!-- .slide: data-background="#fff" -->

<figure class="text-center">
  <img src="caniuse.com.png" alt="" loading="lazy">
  <figcaption><a href="https://caniuse.com/">Can I use…</a> permet de connaître la compatibilité d’une fonctionnalité <br>ou d'une propriété avec les différents navigateurs desktop et mobile.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="mdn.png" alt="" loading="lazy">
  <figcaption><a href="https://developer.mozilla.org/fr/" target="_blank">MDN (Mozilla Developer Network) web docs</a>, site de documentation de référence et disponible presque entièrement en français.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="stackoverflow-how-can-i-horizontally-center-an-element.png" alt="" width="" style="border:1px solid currentColor" loading="lazy">
  <figcaption><a href="https://stackoverflow.com/" target="_blank">Stack Overflow</a>, LA plateforme de Q&A concernant le code (tous langages confondus).</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="css-tricks.com.png" alt="" loading="lazy">
  <figcaption><a href="https://css-tricks.com/" target="_blank">CSS-Tricks</a>, une site en anglais d’articles, de guides et de tutoriels CSS, <br>mais aussi HTML, JS, design et développement web en général.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="openclassrooms.png" alt="" loading="lazy" style="border:1px solid currentColor">
  <figcaption><a href="https://openclassrooms.com/fr/" target="_blank">OpenClassrooms</a>, l’un des meilleurs sites en français pour apprendre à coder.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="grafikart.fr.png" alt="" loading="lazy">
  <figcaption><a href="https://grafikart.fr/" target="_blank">Grafikart</a>, un autre excellent site en français pour apprendre le développement web.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<figure>
  <img src="ateliers.esad-pyrenees.fr-web.png" alt="" loading="lazy" style="border:1px solid currentColor">
  <figcaption><a href="https://ateliers.esad-pyrenees.fr/web/" target="_blank">ÉSAD Pyrénées – Ateliers web</a>, ressources, références et exemples des ateliers web de l’École supérieure d’art et de design des Pyrénées, par Julien Bidoret.</figcaption>
</figure>

--

<!-- .slide: data-background="#fff" -->

<div class="text-center">

<span style="color:initial" class="h2">🤔</span><br>
Des questions ?

</div>
--

#### Crédits

<p class="text-sm">
  Slides : <br><a href="https://revealjs.com/">reveal.js</a> (thème par Timothée Goguely)
</p>

<p class="text-sm">
  Polices de caractères : <br><a href="https://nodotypefoundry.com/product/nt-bau/">NT Bau</a> + <a href="https://nodotypefoundry.com/product/bau-mono/">NT Bau Mono</a> (Nodo Type Foundry), Minion Pro (Robert Slimbach)
</p>
