---
title: Écoconception numérique ?
slug: ecoconception-numerique
description: Webinaire NEC 93 – Hub Francil’IN
date: 2021-11-19
cover: https://timothee.goguely.com/slides/ecoconception-numerique/cover.png
layout: slides
tags:
  - écoconception
transition: slide
backgroundTransition: slide

---

# Écoconception numérique&#8239;?

<hr>

Webinaire NEC 93 – Hub Francil’IN
<a href="https://twitter.com/TimotheeGoguely">@TimotheeGoguely</a>
<small><time class="mono" datetime="2021-11-19">19 novembre 2021</time></small>

---

<span class="heading-color">Timothée Goguely</span>
Designer et développeur web
spécialisé en écoconception + accessibilité numérique

Coopérateur [@Oxamyne](https://twitter.com/oxamyne)
Co-fondateur de [design↔commun](https://designcommun.fr/) + [Plateaux numériques](https://plateaux-numeriques.fr/) <br>avec [Gauthier Roussilhe](https://gauthierroussilhe.com)

<small>
  <a href="https://timothee.goguely.com">timothee.goguely.com</a>
</small>

---

<p class="subtitle">Quoi</p>

## Définition & chiffres

--

### Définition

L’écoconception vise à créer des services numériques&#8239;:
- à **faible empreinte environnementale**
- qui répondent aux **besoins exprimés par les usagers** <br>de façon efficace, quitte à les réinterroger

<small>Source&#8239;: <a href="https://gauthierroussilhe.com/post/ecoconception-critique.html">Gauthier Roussilhe</a></small>

--

### Quelques chiffres sur le numérique en 2020

- **🌫 entre 3 et 4 % des émissions de <abbr title="gaz à effet de serre">GES</abbr>** mondiales <br>+&#8239;6% par an, pourrait doubler d’ici 2025
- **🛢 4,2% de la consommation d’<abbr title="ressources naturelles non renouvelables">énergie primaire</abbr>** mondiale
- **⚡️ 5,5% de la consommation d’électricité** mondiale
- **💧 0,2% de la consommation d’eau** mondiale
- **💻 +&#8239;21 % de déchets électroniques** en 5 ans
- **📱 1 smartphone = 500× son poids en matière première**

<small>Sources&#8239;: <a href="https://www.greenit.fr/ressources/">GreenIT.fr</a> et <a href="https://theshiftproject.org/article/impact-environnemental-du-numerique-5g-nouvelle-etude-du-shift/" lang="en">The Shift Project</a></small>

--

<figure>
  <img src="part-numerique-emissions-GES-mondiales.png" alt="" class="min-w-100">
  <figcaption>Évolution 2013–2025 de la part du numérique dans les émissions de GES mondiales <br><span class="opacity-60">The Shift Project – Forecast Model 2021</span></figcaption>
</figure>

--

<figure>
  <img src="1024px-Electronic_waste_at_Agbogbloshie_Ghana.jpg" alt="" class="min-w-100">
  <figcaption>Déchets électroniques à Agbogbloshie, Ghana <br><span class="opacity-60">Muntaka Chasant / CC BY-SA 4.0 (Wikimedia Commons)</span></figcaption>
</figure>

--

<figure>
  <img src="Garzweiler_Tagebau-1230.jpg" alt="" class="min-w-100">
  <figcaption>Bucket-wheel excavators 288 et 258, mine à ciel ouvert de Garzweiler, Allemagne <br><span class="opacity-60">© Raimond Spekking / CC BY-SA 4.0 (Wikimedia Commons)</span></figcaption>
</figure>

---

<p class="subtitle">Pourquoi</p>

## 3 objectifs

--

### 🌍 Préparer l’avenir

- faire rentrer l’écosystème numérique (infrastructures + services) dans les **objectifs des accords de Paris (+2°C)**
- **réduire l’empreinte environnementale du service**, <br>qu’il soit numérique ou non

Notes:
L’accord de Paris vise à réduire par 5 les émissions de gaz à effet de serre en France en 30 ans (de 11tCO<sub>2</sub>e / pers en 2018 à 2tCO<sub>2</sub>e / pers en 2050)

--

### 📉 Mesurer et réduire les impacts

- **guider** la définition des améliorations prioritaires
- **chiffrer** la réduction des impacts permise par l’écoconception

--

### ☑️ Répondre à des besoins réels

- assurer de la **pertinence des services** et de ne pas allouer des ressources matérielles et humaines inutilement
- **faut-il numériser** dans le contexte donné ? **si oui, comment** ?

---

<p class="subtitle">Comment</p>

## 7 piliers

--

1. <span class="heading-color">📱</span> <a href="#/4/2">Favoriser la durée de vie des équipements</a>
2. <span class="heading-color">⛰</span> <a href="#/4/4">Favoriser la réduction de la consommation globale de ressources non-renouvelables et des déchets électroniques</a>
3. <span class="heading-color">⏳</span> <a href="#/4/6">Favoriser la durée de vie du service en tant que tel</a>
4. <span class="heading-color">🤳</span> <a href="#/4/8">Optimiser pour les conditions d’usage les plus difficiles</a>
5. <span class="heading-color">🚪</span> <a href="#/4/10">Être une porte d’entrée pour intégrer les autres bonnes pratiques du web</a>
6. <span class="heading-color">👥</span> <a href="#/4/12">Fédérer les communautés de pratiques</a>
7. <span class="heading-color">💬</span> <a href="#/4/14">Sensibiliser à la sobriété</a>

--

### 1. 📱 Favoriser la durée de vie des équipements

- **fabrication des équipements** = plus gros facteur d’impact environnemental d’un service numérique <br><small>⚡️ 30% du bilan énergétique global <br>🌫 39% des émissions de <abbr title="gaz à effet de serre">GES</abbr> <br>💧 74% de la consommation d’eau <br>⛰ 76% de la contribution à l’épuisement des ressources abiotiques</small>
- tout service numérique se doit donc de <br>**ne pas favoriser le renouvellement des équipements**

--

#### 💬 Qu’est-ce qui défavorise la durée de vie des équipements&#8239;?
- **puissance de calcul** nécessaire trop importante
- **poids et nombre des scripts et assets** nécessaires trop importants
- absence de **rétro-comptabilité**
- **obsolescence matérielle** par mise à jour de la couche logicielle
- **garanties matérielles** trop courtes
- incitations à la **sur-consommation / renouvellement**


--

### 2. ⛰ Favoriser la réduction de la consommation globale de ressources non-renouvelables et des déchets électroniques

Le volume et l’intensité des ressources consommées par l’usage d’un service et l’usure des équipements qui en résulte, doivent baisser drastiquement sur les facteurs suivants&#8239;:
- <span class="heading-color">🛢</span> **consommation d’énergie primaire** <small>(cela inclut l’électricité)</small>
- <span class="heading-color">⛏</span> **matières premières** <small>(métaux, terres rares, etc.)</small>
- <span class="heading-color">💧</span> **eau**

--

#### 💬 Qu’est-ce qui augmente la consommation de ressources&#8239;?

- applications et services numériques nécessitant une plus grande consommation d’**équipements connectés**
- applications et services numériques nécessitant / provoquant un **large transfert de données** pour fonctionner
- toute logique d’**augmentation du temps en ligne** et d’**augmentation de la capture de données**
- utilisation de **matériel peu efficace** ou/et avec une **courte durée de vie**
- augmentation du **nombre d’utilisateurs**

--

### 3. ⏳ Favoriser la durée de vie du service en tant que tel

Concevoir un service numérique prend du temps.
Il est donc important de s’assurer que le service répond à des besoins **pertinents et durables**.

Un service doit être pensé dès le départ pour faciliter la **maintenance** par des nouvelles équipes ou des personnes extérieures au projet.

--

#### 💬 Qu’est-ce qui défavorise la durée de vie d’un service&#8239;?

- **manque de pertinence** <small>(mauvaise compréhension des besoins)</small>
- **manque d’utilisabilité** <small>(UX, accessibilité, rétro-comptabilité, etc.)</small>
- **manque de maintenance** <small>(langage avec une faible communauté, dépendances des librairies, pas de mise à jour de sécurité)</small>
- **manque d’assistance** <small>(absence de service d’aide et d’accompagnement, documentation absente ou non mise à jour)</small>
- **facteurs externes** <small>(changement d’équipe, de direction, de programme, fin du service …)</small>

--

### 4. 🤳 Optimiser pour les conditions d’usage les plus difficiles

Partir de l’hypothèse que la personne utilisant un service numérique dispose d’un&#8239;:
- **faible accès au réseau** <small>(faible couverture mobile / bande passante)</small>
- **appareil vieux ou d’entrée de gamme** <small>(faibles puissance de calcul / mémoire)</small>

--

#### 💬 Quelles sont les conditions les plus difficiles&#8239;?

- **peu de bande passante** disponible
- **peu de puissance de calcul** et de mémoire disponible
- la personne **paye pour ses données**
- la personne a une **connexion intermittente**
- la personne a des **besoins d’accessibilité** <small>(vision, ouïe, mobilité)</small>

--

### 5. 🚪 Être une porte d’entrée pour intégrer les autres bonnes pratiques du web

Il ne faut pas d’écoconception numérique sans inclure dès le départ les bonnes pratiques en termes d’**accessibilité**, de **sécurité**, de **gouvernance**, de **respect de la vie privée**, etc.

L’écoconception numérique fait partie d’un **cercle vertueux**.

--

#### 💬 Quelles sont les autres bonnes pratiques&#8239;?

- **accessibilité** web <small>(<abbr title="Référentiel général d’amélioration de l’accessibilité"><abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr></abbr>)</small>
- **performances**
- **sécurité** des données
- respect de la **vie privée**, gouvernance des données <small>(<abbr title="Règlement général sur la protection des données">RGPD</abbr>)</small>
- **open data**
- **logiciel libre**
- respect de l’**attention**
- cycle de vie de l’information et de la donnée
- …

--

### 6. 👥 Fédérer les communautés de pratiques

- utiliser des **outils durables**, entretenus par des **communautés actives et pérennes** <small>(maintenance, marché du travail)</small>
- **documenter** le processus et les choix de conception afin d’augmenter la capacité de maintenance sur les services déployés

Notes:
L’écoconception numérique est un champ relativement jeune, il est donc important de documenter le travail fait et de l’ouvrir à la communauté pour améliorer les méthodes et faciliter la collaboration.

--

#### 💬 Comment participer aux communautés de pratiques&#8239;?

- utiliser les **outils déjà présents** et les enrichir
- **documenter** la mise en place des services éco-conçus
- **remonter les bonnes pratiques** et autres informations pour améliorer les connaissances sur le sujet
- utiliser des **licences** permettant le partage des données, logiciels, etc.


--

### 7. 💬 Sensibiliser à la sobriété

- **communiquer** sur les mesures environnementales des services déployés et sur les réductions permises
- **intégrer la démarche de sobriété et d’écoconception numérique dans une stratégie pré-existante** afin d’être aligné dans une politique interne déjà identifiée et défendue

Notes:
- L’utilisation d’équivalences peut être un axe de communication. 
- La démarche d’écoconception doit être rendue perceptible soit par un langage visuel spécifique, soit par des éléments de communication dédiés.
- En interne, la démarche de communication doit permettre de s’adresser à la base des employés d’une organisation mais aussi après de sa direction
- La sobriété et l’écoconception numérique ne peuvent pas être à eux seuls le moteur d’une stratégie de transition écologique, mais elle peut accompagner et supporter le mouvement. 
- L’exemplarité vient donc de la stratégie globale de l’organisation et non pas de sa capacité à écoconcevoir des services numériques de façon isolée.

--

#### 💬 Comment sensibiliser efficacement&#8239;?

- choisir des **indicateurs cohérents** avec le travail de la communauté et les intégrer au sein des services éco-conçus
- utiliser les **services de mesure reconnus par la communauté** <small>(EcoIndex)</small>
- utiliser des **équivalences grand public** de manière consistante

--

### 🏛️ Comment comprendre ces piliers&#8239;?

**Ces 7 piliers sont liés les uns aux autres dans le but d’offrir le meilleur service possible avec le moins de ressources possibles**, ce ne sont pas des choix à la carte. Par exemple: 

- optimiser pour les conditions d’usage les plus difficiles
- facilite l’accès depuis de vieux appareils
- augmente donc leur durée de vie
- et augmente l’accessibilité du service

--

### ⚖️ Comment appliquer ces piliers dans le contexte d’un projet&#8239;?

L’écoconception numérique requiert de réduire l’empreinte environnementale d’un service mais il est aussi nécessaire de déterminer ponctuellement quand il est nécessaire “d’investir” dans une fonction ou un usage clé. Il s’agit alors de savoir arbitrer à la fois des choix d’usages et des choix techniques. 

**Dans le contexte d’un projet, la pratique consiste souvent à organiser les arbitrages entre les parties prenantes.**

--

L’arbitrage ne peut pas se faire seulement sur les principes généraux. Il est important de déterminer dès le début du projet&#8239;:
* **le contexte d’application**&#8239;: définit les enjeux à l’échelle **macroscopique** <small>infrastructure matérielle et énergétique, définition des flux (trafic, origine, etc.), enjeux d’usage et besoins identifiés, enjeux politiques et législatifs</small>
* **le(s) scénario(s) d’usage clés**&#8239;: définit les enjeux à l’échelle **microscopique** <small>spécificité de l’usager et de son quotidien, perception de l’usager, utilisation quotidienne du service</small>

---

<p class="subtitle">Cas d’usage №1</p>

## Commown

--

<div class="text-center">
<svg width="300" height="75" viewBox="0 0 1500 375" fill="#ebe6e5" xmlns="http://www.w3.org/2000/svg" aria-label="Commown" role="img">
  <path d="M645.859 227.48a4.994 4.994 0 00-4-1.65 6.438 6.438 0 00-3.78 1.65 47.444 47.444 0 01-9.11 6.17 28.445 28.445 0 01-13.7 3c-7.727 0-13.774-2.55-18.14-7.65-4.367-5.1-6.547-11.767-6.54-20v-40.41a34.329 34.329 0 011.67-10.77 25.722 25.722 0 014.87-8.83 22.688 22.688 0 017.74-5.88 24.006 24.006 0 0110.4-2.13c5.62 0 10.08 1 13.33 3a46.556 46.556 0 017.27 5.15c1.48 1.35 2.77 2 3.87 1.93a5.545 5.545 0 003.49-1.93l4.23-4.23c2.593-2.567 2.593-5.143 0-7.73a51.916 51.916 0 00-5.06-4.32 37.615 37.615 0 00-7.08-4.14 51.06 51.06 0 00-9-3 47.723 47.723 0 00-11-1.19 41.995 41.995 0 00-16.75 3.31 40.703 40.703 0 00-13.34 9.11 41.891 41.891 0 00-8.84 14 47.983 47.983 0 00-3.22 17.76v40.49a48.547 48.547 0 003.18 17.81 41.742 41.742 0 008.84 14 39.3 39.3 0 0013.34 9 43.167 43.167 0 0016.75 3.22c8.32 0 15.126-1.413 20.42-4.24a57.866 57.866 0 0013.8-10.12 5.645 5.645 0 001.65-3.68 5.01 5.01 0 00-1.65-4.05l-3.64-3.65zM738.929 179.45a44.007 44.007 0 00-13.7-9.28 43.298 43.298 0 00-33.68 0 43.82 43.82 0 00-13.69 9.28 44.445 44.445 0 00-9.3 13.71 43.429 43.429 0 000 33.69 44.445 44.445 0 009.3 13.71 43.993 43.993 0 0013.71 9.28 43.294 43.294 0 0033.68 0 43.9 43.9 0 0023-23 43.323 43.323 0 000-33.69 43.795 43.795 0 00-9.3-13.71l-.02.01zm-5.85 41.31a26.782 26.782 0 01-5.61 8.74 27.743 27.743 0 01-8.48 5.89 26.545 26.545 0 01-21.16 0 27.28 27.28 0 01-8.46-5.89 26.458 26.458 0 01-5.62-8.74 29.688 29.688 0 010-21.53 26.246 26.246 0 015.62-8.74 27.28 27.28 0 018.46-5.89 26.418 26.418 0 0121.16 0 27.743 27.743 0 018.48 5.89 26.584 26.584 0 015.61 8.74 29.832 29.832 0 010 21.53M871.729 175.68a30.086 30.086 0 00-10-6.53 34.504 34.504 0 00-13.06-2.39c-4.387-.1-8.746.714-12.8 2.39a29.378 29.378 0 00-9.01 5.85c-1.087 1.1-2.25 1.1-3.49 0a41.61 41.61 0 00-9.09-5.71 28.844 28.844 0 00-12.42-2.53 34.472 34.472 0 00-13 2.39 29.984 29.984 0 00-10 6.53 29.38 29.38 0 00-6.45 9.95 33.293 33.293 0 00-2.37 12.37v47.84a5.394 5.394 0 00.317 2.184c.254.699.65 1.337 1.163 1.876a5.473 5.473 0 004 1.46h5.53a5.098 5.098 0 005.23-3.334 5.098 5.098 0 00.29-2.186V198a17.978 17.978 0 01.91-5.7 15.614 15.614 0 012.76-5 13.41 13.41 0 014.79-3.58 15.632 15.632 0 016.8-1.39 15.094 15.094 0 016.72 1.39 13.438 13.438 0 014.68 3.58 15.115 15.115 0 012.78 5c.619 1.836.933 3.762.93 5.7v47.84a5.037 5.037 0 00.275 2.194c.252.702.656 1.34 1.184 1.867a5.067 5.067 0 004.061 1.459h5.51a5.078 5.078 0 005.52-5.52V198c-.003-1.93.277-3.851.83-5.7a13.593 13.593 0 017.35-8.55 17.86 17.86 0 0113.82 0 13.714 13.714 0 014.68 3.58 15.29 15.29 0 012.78 5c.6 1.83.907 3.744.91 5.67v47.84a5.394 5.394 0 00.317 2.184c.254.699.65 1.337 1.163 1.876a5.473 5.473 0 004 1.46h5.53c.73.032 1.459-.08 2.146-.33a5.56 5.56 0 001.854-1.13 5.452 5.452 0 001.47-4.06V198a33.14 33.14 0 00-2.31-12.41 28.886 28.886 0 00-6.42-9.95M1004.22 175.68a29.979 29.979 0 00-10.001-6.53 34.61 34.61 0 00-13.08-2.39c-4.383-.1-8.739.714-12.79 2.39a29.163 29.163 0 00-8.92 5.89 2.28 2.28 0 01-1.75.824 2.276 2.276 0 01-1.75-.824 41.53 41.53 0 00-9.11-5.71 28.74 28.74 0 00-12.42-2.57 34.528 34.528 0 00-13.07 2.39 30.133 30.133 0 00-10 6.53 28.92 28.92 0 00-6.44 9.95 33.489 33.489 0 00-2.34 12.37v47.84a5.051 5.051 0 001.459 4.061 5.067 5.067 0 004.061 1.459h5.51a5.088 5.088 0 005.243-3.33c.255-.7.353-1.448.287-2.19V198a17.43 17.43 0 01.93-5.7 14.83 14.83 0 012.74-5 13.545 13.545 0 014.78-3.58 15.827 15.827 0 016.82-1.39 15.168 15.168 0 016.72 1.39 13.862 13.862 0 014.69 3.58 15.474 15.474 0 012.77 5c.603 1.84.91 3.764.91 5.7v47.84a5.394 5.394 0 00.317 2.184c.254.699.65 1.337 1.163 1.876a5.502 5.502 0 004 1.46h5.53a5.471 5.471 0 004-1.46 5.406 5.406 0 001.49-4.06V198a19.44 19.44 0 01.83-5.7 13.593 13.593 0 017.35-8.55 17.807 17.807 0 0113.8 0 13.604 13.604 0 014.7 3.58 15.658 15.658 0 012.77 5c.606 1.839.917 3.763.92 5.7v47.84a5.053 5.053 0 001.455 4.057 5.071 5.071 0 004.056 1.463h5.51c.74.04 1.48-.069 2.18-.319a5.667 5.667 0 001.88-1.141c.5-.543.89-1.182 1.14-1.88.26-.698.36-1.44.32-2.18V198a33.34 33.34 0 00-2.29-12.41 28.786 28.786 0 00-6.44-9.95M1105.15 179.45a43.973 43.973 0 00-13.7-9.28 43.262 43.262 0 00-33.67 0 43.86 43.86 0 00-13.72 9.28 44.398 44.398 0 00-9.29 13.71 43.323 43.323 0 000 33.69 43.776 43.776 0 0023 23 43.25 43.25 0 0033.67 0 43.9 43.9 0 0023-23 43.323 43.323 0 000-33.69 43.875 43.875 0 00-9.3-13.71h.01zm-5.88 41.32a26.554 26.554 0 01-5.62 8.74 27.473 27.473 0 01-8.48 5.89 26.549 26.549 0 01-10.58 2.2c-3.63 0-7.23-.749-10.57-2.2a27.269 27.269 0 01-8.46-5.89 26.188 26.188 0 01-5.62-8.74 29.832 29.832 0 010-21.53 26.138 26.138 0 015.62-8.74 27.269 27.269 0 018.46-5.89 26.373 26.373 0 0110.57-2.21c3.64 0 7.24.753 10.58 2.21a27.473 27.473 0 018.48 5.89 26.178 26.178 0 015.62 8.74 29.729 29.729 0 010 21.53M1241.17 168.6h-5.54a5.12 5.12 0 00-2.19.289c-.69.255-1.33.66-1.85 1.186a5.128 5.128 0 00-1.19 1.86 5.072 5.072 0 00-.28 2.185V222c0 1.933-.31 3.854-.92 5.69a15.446 15.446 0 01-2.76 5 13.975 13.975 0 01-4.69 3.57c-2.19.917-4.54 1.39-6.91 1.39s-4.72-.473-6.91-1.39c-1.8-.835-3.4-2.053-4.68-3.57a14.038 14.038 0 01-2.67-5 19.59 19.59 0 01-.84-5.69v-47.88a5.092 5.092 0 00-1.46-4.053 5.07 5.07 0 00-4.05-1.467h-5.52a5.102 5.102 0 00-2.19.286 5.115 5.115 0 00-3.05 3.046c-.25.7-.35 1.447-.28 2.188V222c.01 1.935-.31 3.858-.93 5.69a14.993 14.993 0 01-2.76 5 13.677 13.677 0 01-4.68 3.57c-2.11.97-4.41 1.445-6.73 1.39-2.34.051-4.67-.424-6.8-1.39a13.387 13.387 0 01-4.79-3.57 15.262 15.262 0 01-2.76-5c-.6-1.838-.89-3.759-.88-5.69v-47.88a5.078 5.078 0 00-.29-2.185 4.976 4.976 0 00-1.18-1.86 5.048 5.048 0 00-1.86-1.186 5.074 5.074 0 00-2.18-.289h-5.54a5.143 5.143 0 00-2.18.3 5.06 5.06 0 00-1.85 1.186 5.122 5.122 0 00-1.49 4.034V222c-.03 4.245.75 8.457 2.3 12.41a29.001 29.001 0 006.45 9.94 29.731 29.731 0 0010 6.53 34.61 34.61 0 0013.07 2.39c4.28.078 8.53-.8 12.43-2.57a40.59 40.59 0 009.1-5.7c1.24-1.11 2.39-1.11 3.5 0a28.361 28.361 0 008.94 5.88 31.545 31.545 0 0012.79 2.39c4.45.051 8.86-.76 13-2.39a29.731 29.731 0 0010-6.53 29.076 29.076 0 006.43-9.94 33.14 33.14 0 002.31-12.41v-47.88a5.104 5.104 0 00-1.47-4.041 5.13 5.13 0 00-1.86-1.186 5.077 5.077 0 00-2.18-.293M1327.47 176.61a32.607 32.607 0 00-10.67-7.18 38 38 0 00-28 0 32.134 32.134 0 00-10.6 7.089 31.98 31.98 0 00-6.98 10.661 32.882 32.882 0 00-2.48 12.69v46c-.07.742.02 1.49.27 2.191a5.11 5.11 0 001.18 1.866c.53.527 1.17.932 1.87 1.185.7.253 1.45.348 2.19.278h5.52a5.05 5.05 0 002.19-.275c.7-.252 1.34-.656 1.87-1.184.53-.527.93-1.165 1.18-1.867.26-.702.35-1.451.28-2.194v-46a18.694 18.694 0 014.33-12.23c2.88-3.507 7.27-5.257 13.17-5.25 5.9.007 10.28 1.757 13.15 5.25a18.753 18.753 0 014.33 12.23v46a5.001 5.001 0 001.43 4.082 5.003 5.003 0 004.08 1.438h5.52c.74.068 1.49-.029 2.19-.282a5.102 5.102 0 003.05-3.048c.25-.701.35-1.448.28-2.19v-46a33.26 33.26 0 00-2.48-12.69 32.29 32.29 0 00-6.9-10.57M523.379 177.9a11.731 11.731 0 00-11.52 9.74l-32.49 11.26c1.66-15.17-1.18-30.75-7.14-41-4.8-8.23-5.44-15-6.05-21.44-.54-5.71-1.05-11.11-4.2-16.82a19.707 19.707 0 00-5.8-6.57c-.14-.09-.27-.2-.41-.3-.31-.21-.63-.39-1-.59l-.66-.39c-.29-.16-.6-.3-.9-.45-.3-.15-.55-.27-.84-.4l-.87-.35c-.33-.12-.65-.25-1-.36-.35-.11-.59-.19-.89-.27l-1.09-.31-.9-.21-1.2-.23c-.314-.06-.624-.11-.93-.15-.43-.07-.86-.12-1.29-.17-.43-.05-.65-.07-1-.09-.35-.02-.91-.07-1.37-.09h-3.51c-.52 0-1 0-1.59.07l-1 .06c-.61 0-1.24.1-1.88.17l-.87.08c-.95.11-1.91.24-2.89.39-.676.1-1.345.247-2 .44-10.05 2.92-12.73 15.12-14.71 24.17-.45 2-.87 4-1.32 5.51-.78 2.7-4 6.38-7.33 10.28-2 2.37-4.27 5-6.39 7.79-.08-.23-.16-.44-.25-.67-.21-.57-.42-1.12-.63-1.66l-.33-.88c-.28-.7-.55-1.39-.8-2-.72-1.8-1.33-3.35-1.74-4.61-2.59-8-1-19.57-.08-22.26a63.744 63.744 0 005.32-4.4l.15-.14.72-.69.11-.11c.21-.21.42-.42.62-.64l.18-.18.58-.63.18-.2.53-.63.18-.22c.14-.18.28-.36.41-.55l.23-.29.29-.44c.09-.13.19-.27.27-.41.08-.14.15-.26.22-.39.093-.141.176-.288.25-.44.09-.17.16-.34.23-.51.07-.17.1-.21.14-.31.103-.261.19-.528.26-.8a3.354 3.354 0 00-2.17-4l-.35-.11h-.1l-.56-.19-.65-.21-.36-.12-.42-.13-1-.32-.2-.06-.73-.22-.19-.06-2.62-.76h-.06c-.95-.27-1.95-.54-3-.8h-.14l-2-.49-.38-.09-1-.21-.57-.12-1.25-.24h-.24a29.948 29.948 0 00-3.31-.41h-.64a35.668 35.668 0 00-14.09-2.55H367.749c-.73 0-1.47.13-2.22.23h-.07c-.37.06-.75.11-1.12.18a1.815 1.815 0 00-.25.09 26.881 26.881 0 00-17.84 12.33c-8.39 13-8.86 21.64-9.11 26.3 0 .41 0 .79-.07 1.15-.62 1.16-3.5 4.29-6.06 7.07-2.88 3.13-6.54 7.11-10.3 11.69l-.79-1.74c-.14-.29-.27-.59-.4-.89-.13-.3-.39-.84-.58-1.27-.19-.43-.33-.75-.49-1.12-.16-.37-.34-.74-.5-1.11-.16-.37-.35-.83-.53-1.24l-.42-1c-.18-.43-.34-.85-.51-1.26-.17-.41-.25-.6-.36-.89l-.46-1.22-.28-.76c-.15-.42-.26-.79-.38-1.17-.06-.18-.13-.39-.18-.57-.12-.39-.19-.73-.26-1.06a52.04 52.04 0 0011-3.54c.53-.25 1-.52 1.51-.8a3.335 3.335 0 001.501-1.975 3.335 3.335 0 00-.281-2.465c0-.11-.15-.28-.26-.49l-.06-.1-.45-.79a61.66 61.66 0 00-6.59-9.32c-.11-.13-.21-.26-.33-.38-.18-.19-.37-.38-.55-.58l-.58-.62c-.2-.21-.41-.4-.62-.6l-.57-.56c-.22-.2-.45-.39-.68-.58-.23-.19-.37-.33-.56-.48-.24-.19-.49-.36-.73-.54l-.55-.4c-.21-.14-.43-.26-.65-.4l-.64-.39c-2.49-4.16-12.76-16.43-43.25-5.49-21.29 7.64-21.72 20.92-22 29.71a36.038 36.038 0 01-.43 5.4c-.77 2.19-6.91 19.76-9.68 29-.7 2.32-1.66 5.13-2.67 8.12-2.53 7.41-5.51 16.21-7.1 24.62h-9.87a7.127 7.127 0 00-5.55 2.66l-15.84 19.72a7.2 7.2 0 00-.85 1.37h-8.72a11.737 11.737 0 00-12.702-4.868 11.74 11.74 0 1011.492 19.398h44.55c2.7 14.76 6.54 29.7 10.18 33a12.529 12.529 0 008.57 3.12c4.5 0 9.53-2 14-5.77 2.46-2.07 4.1-4.95 6.18-8.6a78.75 78.75 0 0117.11-21.74h21.96c.11.39.23.8.35 1.19 1.67 5.63 3.24 11 4.32 15.37 1.8 7.44 9.37 16 21.74 17.26.611.067 1.225.1 1.84.1 11.7 0 13.69-12.47 15.45-23.54.56-3.52 1.13-7.09 2-10.38h32.26v.18c.84 9.42 1.31 14.06 2.73 16.47 4 6.86 11.07 13.38 17.24 13.38.391.003.782-.024 1.17-.08 5.78-.82 8.51-5.15 11.68-10.16a60.738 60.738 0 0113.12-15.69c1.66-1.39 3.38-2.76 5.12-4.1h55.95a11.72 11.72 0 10-1.29-14.9h-34.52c5.17-3.73 9.11-6.86 10.91-9.54a52.355 52.355 0 005.66-11.2l43-14.92a11.731 11.731 0 0014.072-3.585 11.729 11.729 0 00-.197-14.52 11.739 11.739 0 00-4.066-3.204 11.744 11.744 0 00-5.049-1.141l.03.01zm-344.68 69.55a4.927 4.927 0 01-3.483-8.42 4.927 4.927 0 017.587.758 4.934 4.934 0 01-.622 6.222 4.931 4.931 0 01-3.482 1.44zm36.47-11.45l7.65-9.52h5.64c.23 2.72.59 6 1.07 9.52h-14.36zm288.11 1.59a4.929 4.929 0 01.961 9.765 4.932 4.932 0 01-5.061-2.096 4.936 4.936 0 01.614-6.225 4.926 4.926 0 013.486-1.444zM443.299 124c1.82.13 4.28 3.18 6.11 6.63-.58.407-1.12.777-1.62 1.11l-.33.22-1.07.7-.41.26-.74.47-.37.22-.54.32-.3.17-.41.22-.21.12-.32.15-.12.06-.29.13a4.464 4.464 0 01-.56-.12l-.23-.06-.52-.14-.34-.11-.6-.19-.41-.14-.68-.25-.46-.17-.8-.31-.46-.19-1-.43-.32-.14-1.43-.66a7.996 7.996 0 012.06-4.44c1.83-2.12 4.53-3.58 6.4-3.43h-.03zm-62.97-6.24c.57-1.11 1.65-3 3.68-6.16 3 .21 8.46 1.63 13 3a53.988 53.988 0 01-6.9 5.94 57.705 57.705 0 01-9.81-2.79l.03.01zm-80.35 15.79c1.21-1.18 4.19-3.41 11.39-6.68 3.17 1.78 6.9 6.6 9.45 10.64a58.377 58.377 0 01-8.71 2.4h-.11c-3.32-1.47-9.93-4.45-12-6.35l-.02-.01zM265.009 269c-1.75 3.07-3.13 5.5-4.68 6.81-5 4.2-10.93 5.4-13.82 2.8-1.84-1.84-5.24-14.61-7.9-28.05h39.71a85.75 85.75 0 00-13.31 18.44zm42.14-41.25h-.14a3.329 3.329 0 00-2.31 2.06 52.74 52.74 0 01-4.28 3.06c-1.66 1.05-3.21 2.09-4.71 3.12h-59.61c-.5-3.49-.88-6.73-1.09-9.52h73.47c-.44.42-.88.85-1.33 1.26v.02zm-71.07-15.51a193.194 193.194 0 016.61-22.47c1-3 2-5.92 2.75-8.35 2.85-9.49 9.59-28.69 9.65-28.89.045-.12.082-.244.11-.37.421-2.252.632-4.539.63-6.83.28-8.62.57-17.54 17.6-23.65 1.84-.66 3.58-1.21 5.25-1.69-6.42 4.48-8.76 8.7-7.28 12.89 2.35 6.66 7.36 9.17 12.2 11.6 4.63 2.32 9.88 4.95 14.65 11.8 2 2.89 3.88 6.19 5.71 9.38 4.48 7.85 8.35 14.63 13.95 15.84a8.413 8.413 0 001.69.18 8.601 8.601 0 001-.08c1.6 8.94 1.75 20.14-2.54 30.64h-81.98zm110.84 47.66c-2.05 12.91-3.66 18.52-10 17.84-8.73-.87-14.62-6.79-15.93-12.19-1.07-4.38-2.58-9.54-4.2-15h31.86c-.7 3.1-1.21 6.28-1.7 9.34l-.03.01zm8.12-23.9h-42.43c-.31-1.12-.61-2.24-.91-3.37a49.115 49.115 0 005.73-6.15h51.51c-3.05 2-6.14 3.79-9.21 5.58a18.165 18.165 0 00-4.69 3.94zm11.16 0c4.927-2.817 9.637-6 14.09-9.52h4.32c.65 3 1.16 6.28 1.59 9.52h-20zm24.09-31.87a53.582 53.582 0 01-5.27 8.12h-59.84c5.75-16.66 2.28-34.31-1.33-43.91 4.31-5.49 8.62-10.22 12.17-14.08 5.62-6.11 7.57-8.35 7.78-10.73 0-.5.07-1 .1-1.66.23-4.31.62-11.54 8-23a23.256 23.256 0 014-4.76 10.761 10.761 0 00-.8 6.21c.9 4.13 4.31 7.29 7.6 10.35 3.29 3.06 6.53 6.08 7.17 9.67.331 2.169.524 4.357.58 6.55.35 7 .77 15.83 9.27 23a48.34 48.34 0 015.14 5.11c2.48 2.77 4.63 5.18 7.73 5.18h.65a6.395 6.395 0 003-1.17 48.537 48.537 0 01-5.91 25.18l-.04-.06zm24.66 62.71c-2.94 4.67-4.36 6.75-7 7.12-2.25.3-7.78-3.37-11.72-10.07-.72-1.34-1.32-7.87-1.8-13.28h33.83a68.542 68.542 0 00-13.35 16.23h.04zm49.45-44.05c-1.76 2.62-8.32 7.22-15.27 12.09l-1.67 1.12h-54.54c-.41-3.21-.91-6.41-1.52-9.52h50.63a7.14 7.14 0 002.33-.4l22.65-7.85a40.222 40.222 0 01-2.61 4.5v.06zm7.89-21.45l-31.43 10.92h-47.64c1-1.58 2-3.23 3-5a55.255 55.255 0 005.64-37.13.335.335 0 010-.1 48.67 48.67 0 00-1-4.24c0-.07 0-.14-.05-.2.183-.172.345-.367.48-.58a103.367 103.367 0 018.47-10.78c4-4.66 7.51-8.69 8.69-12.79.51-1.77 1-3.79 1.42-5.93.16-.69.31-1.39.47-2.11.11.27.21.55.31.8.38 1 .7 1.8.9 2.41.72 2.29-.32 4.45-2.21 8.08a34.363 34.363 0 00-4.16 11.37c-1.25 8.79.32 16.06 4.56 21 3.69 4.33 9.08 6.59 16 6.72h1.13c18.81 0 23.83-9.2 24.71-17 .41-3.74-.4-7.47-1.18-11.08-1.05-4.81-2-9.39-.45-14.16a48.555 48.555 0 006.46 19.73c5.65 9.69 8.12 25.38 5.82 40l.06.07zm51.03-6.86a4.926 4.926 0 01-4.555-3.043 4.935 4.935 0 011.069-5.373 4.923 4.923 0 012.524-1.349 4.924 4.924 0 015.061 2.096c.541.811.831 1.764.831 2.739a4.94 4.94 0 01-4.93 4.93"></path>
</svg>
</div>

[Commown](https://commown.coop/) est une **coopérative qui loue du matériel électronique réparable et responsable**. La coopérative est basée sur un système d’**économie de la fonctionnalité** où le client loue l’équipement et Commown s’occupe de l’entretien et de la réparation. 

Afin d’aligner sa mission avec ses outils, la coopérative a décidé d’**écoconcevoir son site**. Le site existant reposait sur un Wordpress (avec le page builder Divy) et aucune politique de contenu et d’écoconception n’avait été menée avant afin d’alléger le site.

--

Après un travail en <span lang="en">sprint</span> d’un mois, l’ensemble du contenu du site et son architecture ont été repensés afin de préparer un développement rapide. 

La dette technique de Wordpress étant trop grande, le site a été conçu avec les outils open-source [Hugo](https://gohugo.io) et [Netlify CMS](https://www.netlifycms.org). 


L’ensemble des principes d’écoconception présentés ici ont été utilisés. Cette refonte a permis de **diviser le poids moyen de pages par 22,6**. 

--

<figure>
  <img src="commown.coop-avant.png" width="476" height="264" alt="Page d’accueil de commown.coop, avant">
  <img src="commown.coop-apres.png" width="476" height="264" alt="Page d’accueil de commown.coop, après">
  <figcaption>Page d’accueil de <a href="https://commown.coop">commown.coop</a>, avant / après</figcaption>
</figure>

--

<table>
  <caption>Comparatif des indicateur clés du site de Commown, avant / après. <br>De façon générale, la plupart des indicateurs ont été atteints.</caption>
  <thead>
    <tr>
      <th></th>
      <th scope="col">Avant</th>
      <th scope="col">Après</th>
      <th scope="col">Division par</th>
      <th scope="col">Valeur cible</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Poid moyen d’une page</th>
      <td>3649 Ko</td>
      <td>159 Ko</td>
      <td>22,6 ↘︎</td>
      <td>150 Ko</td>
    </tr>
    <tr>
      <th scope="row">Nombre moyen de requêtes par page</th>
      <td>65</td>
      <td>16</td>
      <td>3,9 ↘︎</td>
      <td>15</td>
    </tr>
    <tr>
      <th scope="row">Nombre d’éléments du DOM</th>
      <td>894</td>
      <td>418</td>
      <td>2,1 ↘︎</td>
      <td>ND</td>
    </tr>
    <tr>
      <th scope="row" lang="en">First Contentful Paint</th>
      <td>5,62 s</td>
      <td>1,09 s</td>
      <td>5,2 ↘︎</td>
      <td>1,5 s</td>
    </tr>
    <tr>
      <th scope="row" lang="en">Time to Interactive</th>
      <td>12,87 s</td>
      <td>1,13 s</td>
      <td>11,4 ↘︎</td>
      <td>2 s</td>
    </tr>
    <tr>
      <th scope="row">Temps de chargement en 3G (780 Kbps)</th>
      <td>40,96 s</td>
      <td>1,66 s</td>
      <td>11,4 ↘︎</td>
      <td>1,5 s</td>
    </tr>
  </tbody>
</table>

---

<p class="subtitle">Cas d’usage №2</p>

## Plateaux numériques

--

[Plateaux numériques](https://plateaux-numeriques.fr/) est un service (en cours d’élaboration) 
de **création de sites web pour les mairies de villages**.

L’objectif est d’accompagner les équipes municipales dans la création de sites web **utiles**, **accessibles**, **abordables**, **ancrés dans le territoire** et **à faible impact environnemental**.

--

- **utile**&#8239;: conçu pour répondre aux besoins de base d’une mairie de petite taille
- **accessible**&#8239;: sites très légers et conformes <abbr title="Référentiel général d’amélioration de l’accessibilité"><abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr></abbr> pouvant être consultés par tous les citoyens quelque soit l’âge de leur smartphone ou la couverture du réseau télécom
- **ancré dans le territoire**&#8239;: collaboration _in situ_ avec des référents locaux et des professionnels du web, pour prendre soin du site et garder l’emploi et les compétences au niveau local

--

- **durable**&#8239;: pensé afin de créer des sites avec une faible empreinte écologique et qui n’accélèrent pas le renouvellement des équipements (smartphone, ordinateurs) grâce à leur simplicité et à leur légéreté
- **commun**&#8239;: vocation à devenir un bien commun, c’est-à-dire un projet dont les municipalités sont parties prenantes du projet et bénéficient mutuellement de leur travail et de leur investissement

--

<figure>
  <img src="lalouvesc.fr.png" alt="Page d’accueil de Lalouvesc.fr">
  <figcaption>Page d’accueil du <a href="https://lalouvesc.fr">site web de Lalouvesc</a> (Ardèche), <br>commune pilote partenaire du projet Plateaux numériques.</figcaption>
</figure>

--

En cours&#8239;:

- **accessiblité**&#8239;: accompagnement par [Access42](https://access42.net/) pour la mise en conformité et la déclaration de conformité au <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>
- **gouvernance**&#8239;: création de l’association portant le projet et de ses règles de gouvernance
- **licences**&#8239;: migration vers des logiciel 100% libres et définition de la licence d’utilisation du service
- **documentation**&#8239;: rédaction de la documentation technique et du guide d’utilisation du service

---


<p class="subtitle">Pour aller plus loin</p>

## Ressources

--

- <a href="https://www.greenit.fr/">GreenIT.fr</a>, la communauté des acteurs de la sobriété numérique et du numérique responsable
- <cite><a href="https://theshiftproject.org/article/deployer-la-sobriete-numerique-rapport-shift/">Déployer la sobriété numérique</a></cite>, rapport du Shift Project <br><small><time datetime="2020-09-14">14 octobre 2020</time></small>
- <cite><a href="https://theshiftproject.org/article/impact-environnemental-du-numerique-5g-nouvelle-etude-du-shift/">Impact environnemental du numérique : tendances à 5 ans et gouvernance de la 5G</a></cite>, rapport du Shift Project <br><small><time datetime="2021-03-26">26 mars 2021</time></small>
- <cite><a href="https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/">Référentiel général d'écoconception de services numériques (RGESN)</a></cite>, Direction interministérielle du numérique (DINUM) <small>version beta, <time datetime="2021-10-19">19 octobre 2021</time></small>
- <cite><a href="https://gauthierroussilhe.com/post/paradoxes-enjeux.html">Paradoxes et enjeux environnementaux de la numérisation</a></cite>, Gauthier Roussilhe <small><time datetime="2021-07-23">23 juillet 2021</time></small>
