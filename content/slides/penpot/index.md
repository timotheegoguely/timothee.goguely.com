---
title: 'À la découverte de Penpot, l’alternative libre et open-source à Figma'
slug: penpot
redirect: 'https://design.penpot.app/#/view/aba078d3-8498-80bd-8003-76dc3bb1ab4c?page-id=52961d58-0a92-80c2-8003-4935ba6d5909&index=0&share-id=c29b5282-da70-8012-8003-78f1d67f529e&zoom=fit'
description: 'Pixels & Bretzels Apéro #97 – LISAA Strasbourg'
date: 2023-11-22
cover: https://timothee.goguely.com/slides/penpot/cover.png
tags:
  - design
  - outils
  - logiciels libres
  - open source
  - web
  - css
---