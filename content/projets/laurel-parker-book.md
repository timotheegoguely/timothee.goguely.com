---
title: Laurel Parker Book
date: 2020-01-01
description: 'Identité visuelle, design et développement web d’un atelier de design et de fabrication artisanale de livres / maison d’édition / galerie d’art.'
categories:
  - design
  - code
# tags:
#   - Hugo
#   - Forestry
#   - Netlify
link: https://laurelparkerbook.com
sitemapExclude: true

---