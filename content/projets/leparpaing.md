---
title: le parpaing
date: 2023-08-01
description: 'Développement web du site du comptoir de matériaux de réemploi « le parpaing » basé à Roubaix et porté par le collectif d’architecture Zerm.'
categories:
  - code
# tags:
#   - Hugo
#   - Netlify CMS
#   - TailwindCSS
link: https://leparpaing.fr
sitemapExclude: true

---