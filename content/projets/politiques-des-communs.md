---
title: Politiques des communs
date: 2019-01-01
description: 'Design et développement web du projet Politiques des communs autour du Cahier de propositions en contexte municipal.'
categories:
  - design
  - code
# tags:
#   - Kirby
link: https://politiquesdescommuns.cc
sitemapExclude: true

---