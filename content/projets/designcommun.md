---
title: design↔commun
date: 2020-01-01
description: 'Identité visuelle, design et développement de l’association Design Commun co-fondée avec Gauthier Roussilhe.'
categories:
  - design
  - code
# tags:
#   - Kirby
link: https://designcommun.fr
sitemapExclude: true

---