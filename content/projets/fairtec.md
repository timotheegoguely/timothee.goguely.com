---
title: FairTEC
date: 2021-05-26
description: 'Design et intégration web HTML / CSS de FairTEC, un collectif d’acteurs européens engagés au service de la sobriété numérique.'
categories:
  - design
  - code
# tags:
#   - Hugo
#   - TailwindCSS
#   - Netlify CMS
link: https://fairtec.io
sitemapExclude: true

---