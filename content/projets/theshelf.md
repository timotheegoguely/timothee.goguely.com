---
title: The Shelf
date: 2019-01-01
description: 'Développement web du site portfolio du studio de design graphique The Shelf (The Shelf Journal + The Shelf Company).'
categories:
  - code
# tags:
#   - Kirby
link: http://theshelf.fr/
sitemapExclude: true

---